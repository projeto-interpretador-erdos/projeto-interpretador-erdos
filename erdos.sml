(* erdos.sml *)

(* This file provides glue code for building interpreter using the
 * parser and lexer specified in erdos.lex and erdos.grm.
*)


structure Erdos : sig
    val parse : unit -> unit
    val from_file : string -> unit
end = 
struct

(* 
 * We apply the functors generated from erdos.lex and erdos.grm to produce
 * the ErdosParser structure.
 *)

  structure ErdosLrVals =
    ErdosLrValsFun(structure Token = LrParser.Token)

  structure ErdosLex =
    ErdosLexFun(structure Tokens = ErdosLrVals.Tokens)

  structure ErdosParser =
    Join(structure LrParser = LrParser
	 structure ParserData = ErdosLrVals.ParserData
	 structure Lex = ErdosLex)

(* 
 * We need a function which given a lexer invokes the parser. The
 * function invoke does this.
 *)

  fun invoke lexstream =
      let fun print_error (s,i:int,_) =
	      TextIO.output(TextIO.stdOut,
			    "Error, line " ^ (Int.toString i) ^ ", " ^ s ^ "\n")
       in ErdosParser.parse(0,lexstream,print_error,())
      end

(* 
 * Finally, we need a driver function that reads one or more expressions
 * from the standard input. The function parse, shown below, does
 * this. It runs the calculator on the standard input and terminates when
 * an end-of-file is encountered.
 *)

open interpretador
  fun parse () = 
      let val lexer = ErdosParser.makeLexer (fn _ => (case TextIO.inputLine TextIO.stdIn
						      of SOME s => s
						       | _ => ""))
	  val dummyEOF = ErdosLrVals.Tokens.EOF(0,0)
	  val dummySEMI = ErdosLrVals.Tokens.SEMI(0,0)
	  fun loop lexer =
	      (
	       print "Erdos > ";TextIO.flushOut (TextIO.stdOut); (* este é o prompt *)
	       let val (result,lexer) = invoke lexer
		   val (nextToken,lexer) = ErdosParser.Stream.get lexer
	       in 
		   if ErdosParser.sameToken(nextToken,dummyEOF) then ()
		   else loop lexer
	       end
	      )
      in imprime_banner_interpretador();
	 clear_interpreter_state();
	 loop lexer
      end


(*
 * This used to be called tt s
 *)
fun from_file s =
    (let val dev = TextIO.openIn s
         val lexstream = ErdosParser.makeLexer(fn i => TextIO.inputN(dev,i))

	 val dummyEOF = ErdosLrVals.Tokens.EOF(0,0)
	 val dummySEMI = ErdosLrVals.Tokens.SEMI(0,0)
	 val dummyEND_KW = ErdosLrVals.Tokens.END_KW(0,0)
	 fun loop lexer =
	     (
	      let val (result,lexer) = invoke lexer
		  val (nextToken,lexer) = ErdosParser.Stream.get lexer
	      in 
		  if ErdosParser.sameToken(nextToken,dummyEOF) 
		     orelse ErdosParser.sameToken(nextToken,dummyEND_KW) then ()
		  else loop lexer
	      end
	     )
     in init_interpreter();
	loop lexstream
        before TextIO.closeIn dev
     end;())




end (* structure Erdos *)
