(* 
 imprime 
     os pares de inteiros formados tomando
     um inteiro da primeira lista e outro da segunda 
 *)

fun todos_os_pares(lum:int list, 
		   ldois:string list, 
		   f: (int * string) -> 'a) =
    case (lum,ldois)
     of (nil,_) => nil
      | (a::nil,c) => (map (fn x => f(a,x)) c)
      | (a::b,c) => 
	(todos_os_pares (a::nil,c,f)) @ (todos_os_pares (b,c,f));

val a = [1,2,3];
val b = ["a","b","c"];
fun junto (a:int,b:string) = (Int.toString a) ^ b;

todos_os_pares (a,b,junto);
