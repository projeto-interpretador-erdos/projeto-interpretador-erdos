datatype exp = n of int (* número *)
	     | p of exp * exp (* plus *)
	     | t of exp * exp (* times *);

fun avalia(e:exp) = case e
		     of n x => x
		      | p (a,b) => avalia(a) + avalia (b)
		      | t (a,b) => avalia(a) * avalia (b);

val a:exp = p (n 17, n 3);
val b:exp = t (n 2,n 2);
val c:exp = t (p (n 2,n  3), t (n 3, p (n 3, n 5) ) );
val d:exp = p (a,b);

avalia (a);
avalia (b);
avalia (c);
avalia (d);

avalia (t (a,c));
