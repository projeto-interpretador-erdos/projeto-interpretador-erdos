
datatype expression = EA of expressao_aritmetica 
		    | EB of expressao_booleana
		    | EL of expressao_do_tipo_lista
     and
     expressao_aritmetica = atomo_numerico
			  | UNARY_MINUS of expressao_aritmetica
			  | opPLUS of expressao_aritmetica * expressao_aritmetica
			  | opMINUS of expressao_aritmetica * expressao_aritmetica
			  | opTIMES of expressao_aritmetica * expressao_aritmetica
			  | opDIV of expressao_aritmetica * expressao_aritmetica
			  | opMOD of expressao_aritmetica * expressao_aritmetica
     and
     expressao_booleana =  atomo_booleano
			| opNOT of expressao_booleana
			| ER of expressao_relacional
			| opLAND of expressao_booleana * expressao_booleana 
			| opLOR of expressao_booleana * expressao_booleana 
     and
     expressao_relacional = opLESS of expressao_aritmetica * expressao_aritmetica
     and 
     expressao_do_tipo_lista = ATOM_L of atomo_lista
			     | opTAIL of expressao_do_tipo_lista
			     | opCONCAT of expressao_do_tipo_lista * expressao_do_tipo_lista
     and
     atomo_numerico = ATOM_A of tipo_basico * valor_basico
		    | opLENGTH of expressao_do_tipo_lista
		    | ATOM_AG of atomo_geral
     and
     atomo_booleano = ATOM_B of tipo_basico * valor_basico
		    | ATOM_BG of atomo_geral
     and 
     atomo_lista = lista_explicita of tipo_basico * valor_basico list
		 | opVIZINHOS of atomo_geral
     and
     atomo_geral = nome_de_variavel of string
		 | acesso_a_atributo of expression * string
		 | chamada_de_funcao of string * (expression list)
		 | opHEAD of expressao_do_tipo_lista
     and
     comando = chamada of string * (expression list)
	     | atribuicao of string * expression
	     | retorno of expression
	     | condicao of expression * expression * (expression option)
	     | add_vertice of string * expression (* grafo, lista de vértices *)
	     | add_aresta of string * string (* grafo, função *)
	     | rem_vertice of string * expression (* grafo, lista de vértices *)
	     | rem_aresta of string * string (* grafo, função *)
	     | get_aresta of string * expression * expression (* grafo, v1, v2 *)

     and
     valor_basico = tI of int | tB of bool | tIF of string * ((string * tipo_basico) list) * (comando list) 
     and 
     tipo_basico = tINT | tBOOL | tINT_FUN;
type tipo_exp = tipo_basico * valor_basico;
type tipo_valor_do_programa = tipo_basico * valor_basico;
