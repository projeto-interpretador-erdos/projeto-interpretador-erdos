structure interpretador = 
struct

datatype tipo_basico = tINT | tBOOL | tSTRING | tTYPE of string
		     | tVERTEX
		     | tINT_FUN | tBOOL_FUN 
		     | tINT_LIST | tBOOL_LIST | tSTRING_LIST | tNULO;

datatype expressao = UNARY_MINUS of expressao
		   | lista_de_expressoes of expressao list
		   | opPLUS of expressao * expressao
		   | opMINUS of expressao * expressao
		   | opTIMES of expressao * expressao
		   | opDIV of expressao * expressao
		   | opMOD of expressao * expressao
		   | opNOT of expressao
		   | opLAND of expressao * expressao 
		   | opLOR of expressao * expressao 
		   | opLESS of expressao * expressao
		   | opTAIL of expressao
		   | opCONCAT of expressao * expressao
		   | opLENGTH of expressao
		   | lista_explicita of tipo_basico * expressao
		   | opVIZINHOS of string * expressao
		   | nome_de_variavel of string
		   | acesso_a_atributo of expressao * string
		   | chamada_de_funcao of string * expressao
		   | opHEAD of expressao
		   | chamada_de_procedimento of string * expressao
		   | atribuicao of string * expressao
		   | atribuicao_a_atributo of expressao * expressao
		   | opRETURN of expressao
		   | condicao of expressao * expressao * (expressao option)
		   | add_vertice of string * expressao (* grafo, lista de vértices *)
		   | add_aresta of string * string (* grafo, função *)
		   | rem_vertice of string * expressao (* grafo, lista de vértices *)
		   | rem_aresta of string * string (* grafo, função *)
		   | get_aresta of string * expressao * expressao (* grafo, v1, v2 *)
		   | tI of int | tB of bool | tS of string 
		   | tIDV of int (* id do vertice *)
		   | tV of (string * (string,expressao) HashTable.hash_table)
		   | tT of ((string,tipo_basico) HashTable.hash_table)
		   | tF of ((string * tipo_basico) list) * (expressao) 
		   | tIL of (int list) | tBL of (bool list) | tSL of (string list)
		   | tN; (* valor para representar o desconhecido *)
(* ~~~~~ Acabaram as definições de tipos ~~~~~~~~~~~ *)

(* ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ *)
(* TABELA DE VARIÁVEIS                            *)
(* ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ *)
(* Sobre as tabelas                               *)
(* Como há uma pilha de tabelas (escopo local das *)
(* chamadas de função, é importante que se opere  *)
(* consultas e inserções por meio das funções     *)
(* insere_na_tabela_certa e busca_na_tabela_certa *)
(* ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ *)
(* TABELA DE VÉRTICES                             *)
(* ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ *)
(* Como cada vértice possui um id único, temos um *)
(* contador de vértices (com o próximo id livre), *)
(* assim como uma tabela com os valores desses    *)
(* vértices.                                      *)
(* ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ *)
exception nao_acho_na_tabela_de_simbolos;
val initial_size:int = 101;
val hash_fn: string->word = HashString.hashString;
val cmp_fn: string*string->bool = (op =);
val tabela_variaveis:(string,expressao) HashTable.hash_table=
    HashTable.mkTable(hash_fn,cmp_fn)
		     (initial_size,
		      nao_acho_na_tabela_de_simbolos);
val tabela_vertices = DynamicArray.array (initial_size,tN);
val contador_de_vertices = ref 0;
val pilha_de_tabelas:((string,expressao) HashTable.hash_table) list ref = ref [];
(* ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ *)

fun busca_na_tabela_certa(nome:string):expressao= 
    (case !pilha_de_tabelas
      of nil => (* ninguém no topo da pilha_de_tabelas *)
	 HashTable.lookup tabela_variaveis (nome)
       | topo::_ =>
	 let 
	     val valor = HashTable.find topo (nome)
	 in
	     case valor of NONE =>
			   HashTable.lookup tabela_variaveis (nome)
			 | SOME a => a
	 end);

fun insere_na_tabela_certa(nome,valor) =
    (case !pilha_de_tabelas
      of nil => (* ninguém no topo da pilha_de_tabelas *)
	 HashTable.insert tabela_variaveis (nome,valor)
       | topo::_ =>
	 HashTable.insert topo (nome,valor)
    );

fun string_de_tipo_basico (t:tipo_basico):string =
    (case t
      of tINT => "int" | tBOOL => "bool" | tSTRING => "string"
       | tINT_FUN => "funcao inteira" | tBOOL_FUN => "funcao booleana"
       | tINT_LIST => "lista de inteiros" | tBOOL_LIST => "lista de booleanos"
       | tNULO => "nada" | tTYPE t => t | _ => "xtxixpxoxdxexsxcxoxnxhxexcxixdxo"
    )
and string_de_expressao(e:expressao):string = 
    (case e
      of UNARY_MINUS a => "-" ^ string_de_expressao(a) 
       | lista_de_expressoes lista => 
	 ("lista de expressoes: "
	  ^ (case lista 
	      of h::nil => string_de_expressao (h)
	       | h::t => string_de_expressao (h) 
			 ^ ", " 
			 ^ string_de_expressao (lista_de_expressoes t)
	       | _ => ""))
       | opPLUS (a,b) => string_de_expressao(a) ^ "+" ^ string_de_expressao(b)
       | opMINUS (a,b) => string_de_expressao(a) ^ "-" ^ string_de_expressao(b)
       | opTIMES (a,b) => string_de_expressao(a) ^ "*" ^ string_de_expressao(b)
       | opDIV (a,b) => string_de_expressao(a) ^ "/" ^ string_de_expressao(b)
       | opMOD (a,b) => string_de_expressao(a) ^ " mod " ^ string_de_expressao(b)
       | opNOT a => "!" ^ string_de_expressao(a)
       | opLAND (a,b) => string_de_expressao(a) ^ " && " ^ string_de_expressao(b) 
       | opLOR (a,b) => string_de_expressao(a) ^ " || " ^ string_de_expressao(b) 
       | opLESS (a,b) => string_de_expressao(a) ^ " < " ^ string_de_expressao(b)
       | opTAIL a => "tail (" ^string_de_expressao(a) ^ ")"
       | opCONCAT (a,b) => "concat ("  ^ string_de_expressao(a)  ^ ", " 
			   ^ string_de_expressao(b) ^ ")"
       | opLENGTH a => "length (" ^string_de_expressao(a) ^ ")"
       | opVIZINHOS (a,b) => a ^ ".vizinhos (" 
			     ^ string_de_expressao(b) ^")"
       | nome_de_variavel (a) => a
       | acesso_a_atributo (a,b) => string_de_expressao(a) ^ "." ^ b
       | chamada_de_funcao (a,b) => a ^ "(" ^ string_de_expressao (b)^")"
       | opHEAD (a) => "hd (" ^ string_de_expressao(a) ^ ")"
       | chamada_de_procedimento (a,b) => a^ "(" ^ string_de_expressao (b) ^ ")"
       | atribuicao (a,b) => a^ ":=" ^string_de_expressao(b)
       | atribuicao_a_atributo (a,b) => string_de_expressao(a) 
					^ ":=" 
					^ string_de_expressao(b)
       | opRETURN (a) => "return " ^string_de_expressao(a)
       | condicao (a,b, SOME c) 
	 => "if ("^string_de_expressao(a) 
	    ^ ") {" ^ string_de_expressao(b) ^ "}"
	    ^ "{" ^string_de_expressao(c) ^ "}"
       | condicao (a,b, NONE) 
	 => "if ("^string_de_expressao(a) 
	    ^ ") {" ^ string_de_expressao(b) ^ "}"
       | add_vertice (a,b) => a^".add_vertices ("^ string_de_expressao(b)
			      ^")" (* grafo, lista de vértices *)
       | add_aresta (a,b) => a^".add_edge (" ^ b ^")" (* grafo, função *)
       | rem_vertice (a,b) => a^".rem_vertices (" ^ string_de_expressao(b) 
			      ^")" (* grafo, lista de vértices *)
       | rem_aresta (a,b) => a^".rem_edge ("^ b ^")" (* grafo, função *)
       | get_aresta (a,b,c) => a ^".get_edge (" ^string_de_expressao(b) 
			       ^ ", " ^ string_de_expressao(c)^")" (* grafo, v1, v2 *)
       | tF (assinatura,definicao) => string_de_funcao (assinatura,definicao)
       | tI v => Int.toString v
       | tB v => if v then "true" else "false"
       | tS v => v
       | tT v => string_de_tipo (v)
       | tV v => string_de_vertice (e)
       | tIDV v => string_de_vertice (e)
       | tIL (h::t)  
	 => string_de_expressao (tI h)
	    ^ " " ^  string_de_expressao (tIL t)
       | tBL (h::t) 
	 => string_de_expressao (tB h)
	    ^ " " ^  string_de_expressao (tBL t)
       | tSL (h::t)
	 => h ^ " " ^ string_de_expressao (tSL t)
       | tN => "nada"
       | _ => ""
    )
and string_de_lista_de_atributos(par_val:(string * expressao) list):string =
    (case par_val
     of (nome,valor)::demais_par_vals => 
	(nome ^ ":" ^ string_de_expressao(valor)
	 ^ (case demais_par_vals
	     of nil => "" 
	      | _ => (", " ^ string_de_lista_de_atributos(demais_par_vals))))
      | _ => "")
and string_de_lista_de_parametros(parametros:(string * tipo_basico) list):string =
    (case parametros
     of nil => ""
      | (nome,tipo)::demais_parametros => 
	(nome ^ ":" ^ string_de_tipo_basico(tipo)
	 ^ (case demais_parametros 
	     of nil => "" 
	      | _ => (", " ^ string_de_lista_de_parametros(demais_parametros)))))
and string_de_funcao (lparametros,lexpressoes) =
    ("fun "
    ^ "(" ^ string_de_lista_de_parametros(lparametros) ^ ") -> "
    ^ "{" ^ string_de_expressao(lexpressoes) ^ "}")
and string_de_tipo (tipo) =
    ("tipo { "
    ^ string_de_lista_de_parametros(HashTable.listItemsi tipo)
    ^ " }")
and string_de_vertice (v) =
    (case v
      of tIDV id => "vertice de id " ^ Int.toString(id)
       | tV (tipo,valores) => 
	 tipo 
	 ^ " = {"
	 ^ string_de_lista_de_atributos(HashTable.listItemsi valores)
       | _ => "que vertice e esse?")




fun imprime_expressao(exp:expressao):unit = 
    TextIO.output(TextIO.stdOut, 
		  (string_de_expressao exp) ^ "\n")

fun make_tabela_de_atributos(tabela, atributos):unit =
    (case (atributos)
     of (nil) =>()
      | ((nome,tipo)::demais_atributos) =>
	(HashTable.insert tabela (nome, tipo);
	 make_tabela_de_atributos(tabela,demais_atributos)));
    
fun make_type(lista_de_atributos):expressao =
    let val tamanho_inicial = 10
	val tabela_de_atributos:(string,tipo_basico) HashTable.hash_table = 
	    HashTable.mkTable(hash_fn,cmp_fn)
			     (tamanho_inicial,
			      nao_acho_na_tabela_de_simbolos)
    in
	make_tabela_de_atributos(tabela_de_atributos, lista_de_atributos);
	tT tabela_de_atributos
    end

fun make_vertice (tipo) = 
    let val id:int = !contador_de_vertices 
	val tamanho_inicial = 7 
	val atributos:(string,expressao) HashTable.hash_table = HashTable.mkTable (hash_fn,cmp_fn) (tamanho_inicial, nao_acho_na_tabela_de_simbolos)
    in
	contador_de_vertices := (!contador_de_vertices) + 1;
	DynamicArray.update (tabela_vertices, id, (tV (tipo, atributos)));
	tIDV (id)
    end
fun novo_identificador (nome:string,x:expressao):unit =
    (
     imprime_expressao(x);
     HashTable.insert tabela_variaveis (nome, x)
    )
fun novos_identificadores (tipo, nomes):unit =
    (case nomes
      of nil => ()
       | (h::t) =>
	 (case tipo
	   of tTYPE tp => 
	      novo_identificador (h, make_vertice tp)
	    | _ => novo_identificador(h, tN);
	  novos_identificadores (tipo, t)));
    
fun is_atom (v) =
    (case v
      of tI _ => true (* inteiro *)
       | tB _ => true (* booleano *)
       | tS _ => true (* string *)
       | tIL _ => true (* lista de inteiros *)
       | tBL _ => true (* lista de booleanos *)
       | tSL _ => true (* lista de strings *)
       | tV _ => true (* vértice (valor) *)
       | tIDV _ => true (* vértice (referência) *)
       | tN => true (* nada *)
       | _ => false);

(* Listas de comandos são a execução de funções ou chamadas de procedimento.
   Talvez valha a pena considerar um novo tipo basico "sem valor": que vamos
   devolver no expressao que resulta da avaliação de uma chamada de _procedimento_. *)
fun evaluate(e:expressao):expressao = 
     let 
	  val antes = e
	  val depois = (case e
      of UNARY_MINUS a =>
	 (case evaluate(a) of tI a' => tI (~a')
			    | a' => UNARY_MINUS a')
       | opPLUS (a,b) => 
	 (case (evaluate a,evaluate b) of (tI a',tI b') => tI (a'+b')
					| (a',b') => opPLUS (a', b'))
       | opMINUS (a,b) =>
	 (case (evaluate a,evaluate b) of (tI a',tI b') => tI (a'-b')
					| (a',b') => opMINUS (a', b'))
       | opTIMES (a,b) =>
	 (case (evaluate a,evaluate b) of (tI a',tI b') => tI (a'*b')
					| (a',b') => opTIMES (a', b'))
       | opDIV (a,b) =>
	 (case (evaluate a,evaluate b) of (tI a',tI b') => tI (a' div b')
					| (a',b') => opDIV (a', b'))
       | opMOD (a,b) =>
	 (case (evaluate a,evaluate b) of (tI a',tI b') => tI (a' mod b')
					| (a',b') => opMOD (a', b'))
       | opNOT a => 
	 (case evaluate(a) of tB a' => tB ( not a')
			    | a' => opNOT a')
       | opLAND (a,b) =>
	 (case (evaluate a,evaluate b) of (tB a',tB b') => tB (a' andalso b')
					| (a',b') => opLAND (a', b'))
       | opLOR (a,b) =>
	 (case (evaluate a,evaluate b) of (tB a',tB b') => tB (a' orelse b')
					| (a',b') => opLOR (a', b'))
       | opLESS (a,b) =>
	 (case (evaluate a,evaluate b) of (tI a',tI b') => tB (a' < b')
					| (a',b') => opLESS (a', b'))
       | opTAIL a => e
       | opCONCAT (a,b) => e
       | opLENGTH a => e
       | opVIZINHOS (a,b) => e
       | nome_de_variavel (a) => busca_na_tabela_certa(a)
       | acesso_a_atributo (vertice,atributo) => 
	 (case vertice 
	  of tIDV id => 
	     (* busca pelo id, e então busca o valor do atributo *)
	     (HashTable.lookup
		  (case (DynamicArray.sub (tabela_vertices,id))
		    of tV (tipo, valores) => valores)
		  (atributo))
	   | tV (tipo, attr_tabl) =>
	     (HashTable.lookup attr_tabl (atributo))
	   | e =>
	     let val v = evaluate (e) in
		 evaluate (acesso_a_atributo(v,atributo))
	     end
	 )
       | chamada_de_funcao (funcao,actual_parameter_list) => 
	 let
	     val (parameter_list,command_list) = 
		 case HashTable.lookup tabela_variaveis (funcao)
		  of tF (a,b) => (a,b)
	 in
	     case parameter_list 
	      of (h::t) => (* Inicializa a tabela local se a função têm parametros *)
		 (let
		      val tamanho_inicial = 10
		      val tabela_local:(string,expressao) HashTable.hash_table = 
			  HashTable.mkTable(hash_fn,cmp_fn)
					   (tamanho_inicial,
					    nao_acho_na_tabela_de_simbolos)
		  in
		      inicializa_tabela_local(tabela_local,parameter_list,actual_parameter_list);
		      pilha_de_tabelas := tabela_local::(!pilha_de_tabelas)
		  end)
	       | _ => ();
	     evaluate_function_command_list command_list
	 end
       | chamada_de_procedimento (a,b) => tN (* é importante que seja tN *)
       | opHEAD (a) => e
       | atribuicao (a,b) 
	 => let val b' = evaluate (b) in
		imprime_expressao(b');
		insere_na_tabela_certa(a,b');
		tN (* atribuições não geram valores *)
	    end
       | atribuicao_a_atributo (a,b)
	 => (case a 
	      of acesso_a_atributo (exp,atributo)
		 => let val exp' = evaluate (exp)
			val b' = evaluate (b)
		    in
			if (( is_atom exp') andalso ( is_atom b' ))
			then 
			    (case (exp')
			      of tIDV id => 
				 (* busca pelo id, e então busca o valor do atributo *)
				 (HashTable.insert
				      (case (DynamicArray.sub (tabela_vertices,id))
					of tV (tipo, valores) => valores)
				      (atributo,b'))
			       | tV (tipo, attr_tabl) =>
				 (HashTable.insert attr_tabl (atributo, b'));
			    tN)
			else tN
		    end)
       | opRETURN (a) =>
	 let val valor = evaluate a in
	     TextIO.output(TextIO.stdOut, 
			   "vou desempilhar, e retornar " ^ string_de_expressao (valor) ^ "\n");
	     case !pilha_de_tabelas
	      of h::t => pilha_de_tabelas := t
	       | _ => pilha_de_tabelas := nil;
	     if is_atom valor
	     then valor
	     else opRETURN valor
	 end
       | condicao (a,b, SOME c) => 
	 (case evaluate(a) 
	   of tB true => evaluate b
	    | tB false => evaluate c
	    | a' => condicao (a', b, SOME c))
       | condicao (a,b, NONE) =>
	 (case evaluate(a) 
	   of tB true => evaluate b
	    | tB false => tN
	    | a' => condicao (a', b, NONE))
       | lista_de_expressoes a => 
	 (case evaluate_expression_list (lista_de_expressoes a)
	   of tI v => tI v
	    | tB v => tB v
	    | tS v => tS v
	    | tIL v => tIL v
	    | tBL v => tBL v
	    | tSL v => tSL v
	    | tN => tN
	    | a' => lista_de_expressoes (a'::nil))
       | add_vertice (a,b) => (e;tN)
       | add_aresta (a,b) => (e;tN)
       | rem_vertice (a,b) => (e;tN)
       | rem_aresta (a,b) => (e;tN)
       | get_aresta (a,b,c) => (e;tN)
       (* para os tipos básicos a avaliação não muda nada mesmo *)
       | tF (assinatura,definicao) => e
       | tI v => e 
       | tB v => e
       | tS v => e
       | tIL v => e
       | tBL v => e
       | tSL v => e
       | tN => e
       | _ => e)
     in
	 (TextIO.output(TextIO.stdOut, 
			string_de_expressao(antes)
			^ " -> "
			^ string_de_expressao(depois)
			^ "\n");depois)
     end
and inicializa_tabela_local(tabela, parameter_list, actual_parameter_list):unit =
    (case (parameter_list,actual_parameter_list)
      of (nil,_) =>()
       | ((par_name,_)::resto_par_list, 
	  lista_de_expressoes ((par_value)::resto_actual_par_list)) =>
	 (let val valor = evaluate par_value in
	      TextIO.output(TextIO.stdOut, 
			    "colocando \"" 
			    ^ par_name 
			    ^ "\" (que vale "
			    ^ string_de_expressao(par_value)
			    ^ " = " 
			    ^ string_de_expressao(valor) 
			    ^ ") na tabela a ser empilhada\n");
	      HashTable.insert tabela (par_name, valor);
	      inicializa_tabela_local(tabela,resto_par_list,lista_de_expressoes resto_actual_par_list)
	  end
	 )
)
and evaluate_expression_list (l:expressao):expressao =
    (case l
     of lista_de_expressoes (nil) => tN (* é importante que seja tN *)
      | lista_de_expressoes (h::nil) => evaluate h
      | lista_de_expressoes (h::t) => 
	(evaluate h; evaluate_expression_list (lista_de_expressoes t)))
and evaluate_function_command_list (l:expressao):expressao = 
    (* É parecido, mas pára ao encontrar um return *)
    (imprime_expressao l; (* para ver o que a função faz antes de fazer *)
     case l
      of lista_de_expressoes (nil) => tN
      | lista_de_expressoes (h::t) =>
	let val valor = evaluate h in
	    case valor
	     of tI v => tI v
	      | tB v => tB v
	      | tS v => tS v
	      | tIL v => tIL v
	      | tBL v => tBL v
	      | tSL v => tSL v
	      | b =>
		(
		 case t 
		  of nil => evaluate b
		   | _ => 
		     (evaluate b;
		      evaluate_function_command_list (lista_de_expressoes(t))
		     )
		)
	end
    );



fun limpa_estado_interpretador() =
    (HashTable.clear tabela_variaveis;
     DynamicArray.truncate (tabela_vertices,0);
     contador_de_vertices := 0;
     pilha_de_tabelas := [])

	 
end
