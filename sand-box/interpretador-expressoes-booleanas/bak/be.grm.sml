functor BeLrValsFun(structure Token : TOKEN)
 : sig structure ParserData : PARSER_DATA
       structure Tokens : Be_TOKENS
   end
 = 
struct
structure ParserData=
struct
structure Header = 
struct
open interpretador


end
structure LrTable = Token.LrTable
structure Token = Token
local open LrTable in 
val table=let val actionRows =
"\
\\001\000\001\000\034\000\002\000\033\000\004\000\032\000\005\000\031\000\
\\011\000\030\000\021\000\029\000\000\000\
\\001\000\001\000\034\000\004\000\032\000\011\000\036\000\000\000\
\\001\000\003\000\021\000\000\000\
\\001\000\003\000\023\000\000\000\
\\001\000\003\000\025\000\000\000\
\\001\000\003\000\081\000\004\000\080\000\005\000\079\000\006\000\078\000\000\000\
\\001\000\008\000\000\000\025\000\000\000\000\000\
\\001\000\009\000\049\000\010\000\048\000\012\000\067\000\000\000\
\\001\000\011\000\068\000\000\000\
\\001\000\011\000\069\000\000\000\
\\001\000\012\000\066\000\015\000\047\000\016\000\046\000\017\000\045\000\
\\018\000\044\000\019\000\043\000\000\000\
\\001\000\012\000\066\000\015\000\047\000\016\000\046\000\017\000\045\000\
\\018\000\044\000\019\000\043\000\020\000\042\000\000\000\
\\001\000\012\000\076\000\026\000\075\000\027\000\074\000\000\000\
\\001\000\012\000\083\000\000\000\
\\001\000\015\000\047\000\016\000\046\000\017\000\045\000\018\000\044\000\
\\019\000\043\000\020\000\042\000\000\000\
\\001\000\023\000\019\000\000\000\
\\001\000\023\000\020\000\000\000\
\\001\000\024\000\007\000\000\000\
\\001\000\024\000\008\000\000\000\
\\001\000\024\000\037\000\000\000\
\\001\000\026\000\017\000\027\000\016\000\000\000\
\\001\000\026\000\055\000\027\000\054\000\000\000\
\\001\000\026\000\075\000\027\000\074\000\000\000\
\\001\000\028\000\014\000\000\000\
\\001\000\029\000\006\000\000\000\
\\001\000\030\000\005\000\031\000\004\000\000\000\
\\086\000\000\000\
\\087\000\000\000\
\\088\000\000\000\
\\089\000\000\000\
\\090\000\000\000\
\\091\000\000\000\
\\092\000\000\000\
\\093\000\000\000\
\\094\000\004\000\012\000\005\000\011\000\000\000\
\\095\000\022\000\041\000\000\000\
\\096\000\000\000\
\\097\000\022\000\039\000\000\000\
\\098\000\000\000\
\\099\000\000\000\
\\100\000\000\000\
\\101\000\022\000\082\000\000\000\
\\102\000\000\000\
\\103\000\000\000\
\\104\000\000\000\
\\105\000\000\000\
\\106\000\000\000\
\\107\000\000\000\
\\108\000\000\000\
\\109\000\000\000\
\\110\000\000\000\
\\111\000\000\000\
\\112\000\000\000\
\\113\000\000\000\
\\114\000\000\000\
\\115\000\000\000\
\\116\000\000\000\
\\117\000\000\000\
\\118\000\017\000\045\000\018\000\044\000\019\000\043\000\000\000\
\\119\000\017\000\045\000\018\000\044\000\019\000\043\000\000\000\
\\120\000\019\000\043\000\000\000\
\\121\000\019\000\043\000\000\000\
\\122\000\000\000\
\\123\000\000\000\
\\124\000\015\000\047\000\016\000\046\000\017\000\045\000\018\000\044\000\
\\019\000\043\000\000\000\
\\125\000\015\000\047\000\016\000\046\000\017\000\045\000\018\000\044\000\
\\019\000\043\000\000\000\
\\126\000\009\000\049\000\010\000\048\000\000\000\
\"
val actionRowNumbers =
"\025\000\024\000\017\000\018\000\
\\034\000\023\000\020\000\026\000\
\\034\000\015\000\016\000\028\000\
\\002\000\027\000\003\000\004\000\
\\033\000\000\000\001\000\019\000\
\\020\000\037\000\020\000\035\000\
\\055\000\014\000\066\000\000\000\
\\000\000\050\000\057\000\049\000\
\\056\000\065\000\001\000\021\000\
\\030\000\003\000\029\000\004\000\
\\001\000\001\000\001\000\001\000\
\\001\000\001\000\000\000\000\000\
\\051\000\011\000\007\000\010\000\
\\008\000\009\000\038\000\036\000\
\\064\000\062\000\060\000\061\000\
\\059\000\058\000\053\000\052\000\
\\063\000\054\000\012\000\012\000\
\\005\000\041\000\013\000\032\000\
\\048\000\047\000\039\000\031\000\
\\046\000\045\000\044\000\043\000\
\\022\000\040\000\042\000\006\000"
val gotoT =
"\
\\002\000\083\000\018\000\001\000\000\000\
\\000\000\
\\000\000\
\\000\000\
\\008\000\008\000\019\000\007\000\000\000\
\\021\000\011\000\000\000\
\\020\000\013\000\000\000\
\\000\000\
\\008\000\008\000\019\000\016\000\000\000\
\\000\000\
\\000\000\
\\000\000\
\\000\000\
\\000\000\
\\007\000\020\000\000\000\
\\006\000\022\000\000\000\
\\000\000\
\\003\000\026\000\004\000\025\000\005\000\024\000\000\000\
\\004\000\033\000\000\000\
\\000\000\
\\020\000\036\000\000\000\
\\000\000\
\\020\000\038\000\000\000\
\\000\000\
\\000\000\
\\000\000\
\\000\000\
\\003\000\048\000\004\000\025\000\005\000\024\000\000\000\
\\003\000\050\000\004\000\049\000\005\000\024\000\000\000\
\\000\000\
\\000\000\
\\000\000\
\\000\000\
\\000\000\
\\004\000\051\000\000\000\
\\000\000\
\\000\000\
\\007\000\054\000\000\000\
\\000\000\
\\006\000\055\000\000\000\
\\004\000\056\000\000\000\
\\004\000\057\000\000\000\
\\004\000\058\000\000\000\
\\004\000\059\000\000\000\
\\004\000\060\000\000\000\
\\004\000\061\000\000\000\
\\003\000\062\000\004\000\025\000\005\000\024\000\000\000\
\\003\000\063\000\004\000\025\000\005\000\024\000\000\000\
\\000\000\
\\000\000\
\\000\000\
\\000\000\
\\000\000\
\\000\000\
\\000\000\
\\000\000\
\\000\000\
\\000\000\
\\000\000\
\\000\000\
\\000\000\
\\000\000\
\\000\000\
\\000\000\
\\000\000\
\\000\000\
\\014\000\071\000\015\000\070\000\016\000\069\000\017\000\068\000\000\000\
\\014\000\075\000\015\000\070\000\016\000\069\000\017\000\068\000\000\000\
\\000\000\
\\000\000\
\\000\000\
\\000\000\
\\000\000\
\\000\000\
\\000\000\
\\000\000\
\\000\000\
\\000\000\
\\000\000\
\\000\000\
\\015\000\082\000\016\000\069\000\017\000\068\000\000\000\
\\000\000\
\\000\000\
\\000\000\
\"
val numstates = 84
val numrules = 41
val s = ref "" and index = ref 0
val string_to_int = fn () => 
let val i = !index
in index := i+2; Char.ord(String.sub(!s,i)) + Char.ord(String.sub(!s,i+1)) * 256
end
val string_to_list = fn s' =>
    let val len = String.size s'
        fun f () =
           if !index < len then string_to_int() :: f()
           else nil
   in index := 0; s := s'; f ()
   end
val string_to_pairlist = fn (conv_key,conv_entry) =>
     let fun f () =
         case string_to_int()
         of 0 => EMPTY
          | n => PAIR(conv_key (n-1),conv_entry (string_to_int()),f())
     in f
     end
val string_to_pairlist_default = fn (conv_key,conv_entry) =>
    let val conv_row = string_to_pairlist(conv_key,conv_entry)
    in fn () =>
       let val default = conv_entry(string_to_int())
           val row = conv_row()
       in (row,default)
       end
   end
val string_to_table = fn (convert_row,s') =>
    let val len = String.size s'
        fun f ()=
           if !index < len then convert_row() :: f()
           else nil
     in (s := s'; index := 0; f ())
     end
local
  val memo = Array.array(numstates+numrules,ERROR)
  val _ =let fun g i=(Array.update(memo,i,REDUCE(i-numstates)); g(i+1))
       fun f i =
            if i=numstates then g i
            else (Array.update(memo,i,SHIFT (STATE i)); f (i+1))
          in f 0 handle General.Subscript => ()
          end
in
val entry_to_action = fn 0 => ACCEPT | 1 => ERROR | j => Array.sub(memo,(j-2))
end
val gotoT=Array.fromList(string_to_table(string_to_pairlist(NT,STATE),gotoT))
val actionRows=string_to_table(string_to_pairlist_default(T,entry_to_action),actionRows)
val actionRowNumbers = string_to_list actionRowNumbers
val actionT = let val actionRowLookUp=
let val a=Array.fromList(actionRows) in fn i=>Array.sub(a,i) end
in Array.fromList(List.map actionRowLookUp actionRowNumbers)
end
in LrTable.mkLrTable {actions=actionT,gotos=gotoT,numRules=numrules,
numStates=numstates,initialState=STATE 0}
end
end
local open Header in
type pos = int
type arg = unit
structure MlyValue = 
struct
datatype svalue = VOID | ntVOID of unit ->  unit
 | FIDENT of unit ->  (string) | BIDENT of unit ->  (string)
 | AIDENT of unit ->  (string) | IDENT of unit ->  (string)
 | BOOL of unit ->  (bool) | NUM of unit ->  (int)
 | TIPO of unit ->  (tipo_basico)
 | PARAMETRO of unit ->  (string*tipo_basico)
 | LISTA_PARAMETROS of unit ->  ( ( string * tipo_basico )  list)
 | ASSINATURA_FUNCAO of unit ->  ( ( string * tipo_basico )  list)
 | REXP of unit ->  (tipo_exp) | AEXP of unit ->  (tipo_exp)
 | BEXP of unit ->  (tipo_exp) | EXP of unit ->  (tipo_exp)
end
type svalue = MlyValue.svalue
type result = unit
end
structure EC=
struct
open LrTable
infix 5 $$
fun x $$ y = y::x
val is_keyword =
fn _ => false
val preferred_change : (term list * term list) list = 
nil
val noShift = 
fn (T 7) => true | _ => false
val showTerminal =
fn (T 0) => "NUM"
  | (T 1) => "BOOL"
  | (T 2) => "IDENT"
  | (T 3) => "AIDENT"
  | (T 4) => "BIDENT"
  | (T 5) => "FIDENT"
  | (T 6) => "SEMI"
  | (T 7) => "EOF"
  | (T 8) => "LAND"
  | (T 9) => "LOR"
  | (T 10) => "LPAR"
  | (T 11) => "RPAR"
  | (T 12) => "LCOL"
  | (T 13) => "RCOL"
  | (T 14) => "PLUS"
  | (T 15) => "SUB"
  | (T 16) => "DIV"
  | (T 17) => "TIMES"
  | (T 18) => "YMOD"
  | (T 19) => "LESS"
  | (T 20) => "LNOT"
  | (T 21) => "COMMA"
  | (T 22) => "RECEBE"
  | (T 23) => "COLON"
  | (T 24) => "DEATH"
  | (T 25) => "INT_KW"
  | (T 26) => "BOOL_KW"
  | (T 27) => "FUN_KW"
  | (T 28) => "PROGRAM_KW"
  | (T 29) => "VARIABLES_KW"
  | (T 30) => "PAF_KW"
  | _ => "bogus-term"
local open Header in
val errtermvalue=
fn _ => MlyValue.VOID
end
val terms : term list = nil
 $$ (T 30) $$ (T 29) $$ (T 28) $$ (T 27) $$ (T 26) $$ (T 25) $$ (T 24)
 $$ (T 23) $$ (T 22) $$ (T 21) $$ (T 20) $$ (T 19) $$ (T 18) $$ (T 17)
 $$ (T 16) $$ (T 15) $$ (T 14) $$ (T 13) $$ (T 12) $$ (T 11) $$ (T 10)
 $$ (T 9) $$ (T 8) $$ (T 7) $$ (T 6)end
structure Actions =
struct 
exception mlyAction of int
local open Header in
val actions = 
fn (i392,defaultPos,stack,
    (()):arg) =>
case (i392,stack)
of  ( 0, ( ( _, ( MlyValue.ntVOID SEQUENCIA_DE_STATEMENTS1, _, 
SEQUENCIA_DE_STATEMENTS1right)) :: _ :: ( _, ( MlyValue.ntVOID 
DEFINICOES_E_DECLARACOES1, DEFINICOES_E_DECLARACOES1left, _)) :: 
rest671)) => let val  result = MlyValue.ntVOID (fn _ => ( let val  
DEFINICOES_E_DECLARACOES1 = DEFINICOES_E_DECLARACOES1 ()
 val  SEQUENCIA_DE_STATEMENTS1 = SEQUENCIA_DE_STATEMENTS1 ()
 in ()
end; ()))
 in ( LrTable.NT 1, ( result, DEFINICOES_E_DECLARACOES1left, 
SEQUENCIA_DE_STATEMENTS1right), rest671)
end
|  ( 1, ( ( _, ( MlyValue.ntVOID VARIABLE_DECL_SECTION1, _, 
VARIABLE_DECL_SECTION1right)) :: _ :: ( _, ( _, VARIABLES_KW1left, _))
 :: rest671)) => let val  result = MlyValue.ntVOID (fn _ => ( let val 
 VARIABLE_DECL_SECTION1 = VARIABLE_DECL_SECTION1 ()
 in ()
end; ()))
 in ( LrTable.NT 17, ( result, VARIABLES_KW1left, 
VARIABLE_DECL_SECTION1right), rest671)
end
|  ( 2, ( ( _, ( MlyValue.ntVOID PROCEDURES_AND_FUNCTIONS1, _, 
PROCEDURES_AND_FUNCTIONS1right)) :: _ :: ( _, ( _, PAF_KW1left, _)) ::
 rest671)) => let val  result = MlyValue.ntVOID (fn _ => ( let val  
PROCEDURES_AND_FUNCTIONS1 = PROCEDURES_AND_FUNCTIONS1 ()
 in ()
end; ()))
 in ( LrTable.NT 17, ( result, PAF_KW1left, 
PROCEDURES_AND_FUNCTIONS1right), rest671)
end
|  ( 3, ( ( _, ( MlyValue.ntVOID VARIABLE_DECL_SECTION1, _, 
VARIABLE_DECL_SECTION1right)) :: ( _, ( MlyValue.ntVOID INT_LIST1, _,
 _)) :: ( _, ( _, INT_KW1left, _)) :: rest671)) => let val  result = 
MlyValue.ntVOID (fn _ => ( let val  INT_LIST1 = INT_LIST1 ()
 val  VARIABLE_DECL_SECTION1 = VARIABLE_DECL_SECTION1 ()
 in ()
end; ()))
 in ( LrTable.NT 19, ( result, INT_KW1left, 
VARIABLE_DECL_SECTION1right), rest671)
end
|  ( 4, ( ( _, ( MlyValue.ntVOID VARIABLE_DECL_SECTION1, _, 
VARIABLE_DECL_SECTION1right)) :: ( _, ( MlyValue.ntVOID BOOL_LIST1, _,
 _)) :: ( _, ( _, BOOL_KW1left, _)) :: rest671)) => let val  result = 
MlyValue.ntVOID (fn _ => ( let val  BOOL_LIST1 = BOOL_LIST1 ()
 val  VARIABLE_DECL_SECTION1 = VARIABLE_DECL_SECTION1 ()
 in ()
end; ()))
 in ( LrTable.NT 19, ( result, BOOL_KW1left, 
VARIABLE_DECL_SECTION1right), rest671)
end
|  ( 5, ( ( _, ( MlyValue.ASSINATURA_FUNCAO ASSINATURA_FUNCAO1, _, 
ASSINATURA_FUNCAO1right)) :: _ :: _ :: _ :: ( _, ( MlyValue.IDENT 
IDENT1, _, _)) :: ( _, ( _, FUN_KW1left, _)) :: rest671)) => let val  
result = MlyValue.ntVOID (fn _ => ( let val  (IDENT as IDENT1) = 
IDENT1 ()
 val  (ASSINATURA_FUNCAO as ASSINATURA_FUNCAO1) = ASSINATURA_FUNCAO1
 ()
 in (
interpretaATRIBUICAO (IDENT, (tINT_FUN, tIF (ASSINATURA_FUNCAO,nil)));()
)
end; ()))
 in ( LrTable.NT 20, ( result, FUN_KW1left, ASSINATURA_FUNCAO1right), 
rest671)
end
|  ( 6, ( ( _, ( MlyValue.ASSINATURA_FUNCAO ASSINATURA_FUNCAO1, _, 
ASSINATURA_FUNCAO1right)) :: _ :: _ :: _ :: ( _, ( MlyValue.IDENT 
IDENT1, _, _)) :: ( _, ( _, FUN_KW1left, _)) :: rest671)) => let val  
result = MlyValue.ntVOID (fn _ => ( let val  (IDENT as IDENT1) = 
IDENT1 ()
 val  (ASSINATURA_FUNCAO as ASSINATURA_FUNCAO1) = ASSINATURA_FUNCAO1
 ()
 in (
interpretaATRIBUICAO (IDENT, (tBOOL_FUN, tBF (ASSINATURA_FUNCAO,nil)));()
)
end; ()))
 in ( LrTable.NT 20, ( result, FUN_KW1left, ASSINATURA_FUNCAO1right), 
rest671)
end
|  ( 7, ( ( _, ( MlyValue.ntVOID SEQUENCIA_DE_STATEMENTS1, _, 
SEQUENCIA_DE_STATEMENTS1right)) :: ( _, ( MlyValue.ntVOID ATRIBUICAO1,
 ATRIBUICAO1left, _)) :: rest671)) => let val  result = 
MlyValue.ntVOID (fn _ => ( let val  ATRIBUICAO1 = ATRIBUICAO1 ()
 val  SEQUENCIA_DE_STATEMENTS1 = SEQUENCIA_DE_STATEMENTS1 ()
 in ()
end; ()))
 in ( LrTable.NT 18, ( result, ATRIBUICAO1left, 
SEQUENCIA_DE_STATEMENTS1right), rest671)
end
|  ( 8, ( rest671)) => let val  result = MlyValue.ntVOID (fn _ => ())
 in ( LrTable.NT 18, ( result, defaultPos, defaultPos), rest671)
end
|  ( 9, ( ( _, ( MlyValue.IDENT IDENT1, IDENT1left, IDENT1right)) :: 
rest671)) => let val  result = MlyValue.ntVOID (fn _ => ( let val  (
IDENT as IDENT1) = IDENT1 ()
 in (interpretaATRIBUICAO(IDENT,(tINT, tI 0)))
end; ()))
 in ( LrTable.NT 5, ( result, IDENT1left, IDENT1right), rest671)
end
|  ( 10, ( ( _, ( MlyValue.ntVOID INT_LIST1, _, INT_LIST1right)) :: _
 :: ( _, ( MlyValue.IDENT IDENT1, IDENT1left, _)) :: rest671)) => let
 val  result = MlyValue.ntVOID (fn _ => ( let val  (IDENT as IDENT1) =
 IDENT1 ()
 val  INT_LIST1 = INT_LIST1 ()
 in (interpretaATRIBUICAO(IDENT,(tINT, tI 0)))
end; ()))
 in ( LrTable.NT 5, ( result, IDENT1left, INT_LIST1right), rest671)

end
|  ( 11, ( ( _, ( MlyValue.IDENT IDENT1, IDENT1left, IDENT1right)) :: 
rest671)) => let val  result = MlyValue.ntVOID (fn _ => ( let val  (
IDENT as IDENT1) = IDENT1 ()
 in (interpretaATRIBUICAO(IDENT,(tBOOL, tB true)))
end; ()))
 in ( LrTable.NT 6, ( result, IDENT1left, IDENT1right), rest671)
end
|  ( 12, ( ( _, ( MlyValue.ntVOID BOOL_LIST1, _, BOOL_LIST1right)) ::
 _ :: ( _, ( MlyValue.IDENT IDENT1, IDENT1left, _)) :: rest671)) =>
 let val  result = MlyValue.ntVOID (fn _ => ( let val  (IDENT as 
IDENT1) = IDENT1 ()
 val  BOOL_LIST1 = BOOL_LIST1 ()
 in (interpretaATRIBUICAO(IDENT,(tBOOL, tB true)))
end; ()))
 in ( LrTable.NT 6, ( result, IDENT1left, BOOL_LIST1right), rest671)

end
|  ( 13, ( ( _, ( _, RPAR1left, RPAR1right)) :: rest671)) => let val  
result = MlyValue.ASSINATURA_FUNCAO (fn _ => (nil))
 in ( LrTable.NT 13, ( result, RPAR1left, RPAR1right), rest671)
end
|  ( 14, ( ( _, ( _, _, RPAR1right)) :: ( _, ( 
MlyValue.LISTA_PARAMETROS LISTA_PARAMETROS1, LISTA_PARAMETROS1left, _)
) :: rest671)) => let val  result = MlyValue.ASSINATURA_FUNCAO (fn _
 => let val  (LISTA_PARAMETROS as LISTA_PARAMETROS1) = 
LISTA_PARAMETROS1 ()
 in (LISTA_PARAMETROS)
end)
 in ( LrTable.NT 13, ( result, LISTA_PARAMETROS1left, RPAR1right), 
rest671)
end
|  ( 15, ( ( _, ( MlyValue.PARAMETRO PARAMETRO1, PARAMETRO1left, 
PARAMETRO1right)) :: rest671)) => let val  result = 
MlyValue.LISTA_PARAMETROS (fn _ => let val  (PARAMETRO as PARAMETRO1)
 = PARAMETRO1 ()
 in (PARAMETRO::nil)
end)
 in ( LrTable.NT 14, ( result, PARAMETRO1left, PARAMETRO1right), 
rest671)
end
|  ( 16, ( ( _, ( MlyValue.LISTA_PARAMETROS LISTA_PARAMETROS1, _, 
LISTA_PARAMETROS1right)) :: _ :: ( _, ( MlyValue.PARAMETRO PARAMETRO1,
 PARAMETRO1left, _)) :: rest671)) => let val  result = 
MlyValue.LISTA_PARAMETROS (fn _ => let val  (PARAMETRO as PARAMETRO1)
 = PARAMETRO1 ()
 val  (LISTA_PARAMETROS as LISTA_PARAMETROS1) = LISTA_PARAMETROS1 ()
 in (PARAMETRO::LISTA_PARAMETROS)
end)
 in ( LrTable.NT 14, ( result, PARAMETRO1left, LISTA_PARAMETROS1right)
, rest671)
end
|  ( 17, ( ( _, ( MlyValue.IDENT IDENT1, _, IDENT1right)) :: ( _, ( 
MlyValue.TIPO TIPO1, TIPO1left, _)) :: rest671)) => let val  result = 
MlyValue.PARAMETRO (fn _ => let val  (TIPO as TIPO1) = TIPO1 ()
 val  (IDENT as IDENT1) = IDENT1 ()
 in ((IDENT,TIPO))
end)
 in ( LrTable.NT 15, ( result, TIPO1left, IDENT1right), rest671)
end
|  ( 18, ( ( _, ( MlyValue.AIDENT AIDENT1, _, AIDENT1right)) :: ( _, (
 MlyValue.TIPO TIPO1, TIPO1left, _)) :: rest671)) => let val  result =
 MlyValue.PARAMETRO (fn _ => let val  (TIPO as TIPO1) = TIPO1 ()
 val  (AIDENT as AIDENT1) = AIDENT1 ()
 in ((AIDENT,TIPO))
end)
 in ( LrTable.NT 15, ( result, TIPO1left, AIDENT1right), rest671)
end
|  ( 19, ( ( _, ( MlyValue.BIDENT BIDENT1, _, BIDENT1right)) :: ( _, (
 MlyValue.TIPO TIPO1, TIPO1left, _)) :: rest671)) => let val  result =
 MlyValue.PARAMETRO (fn _ => let val  (TIPO as TIPO1) = TIPO1 ()
 val  (BIDENT as BIDENT1) = BIDENT1 ()
 in ((BIDENT,TIPO))
end)
 in ( LrTable.NT 15, ( result, TIPO1left, BIDENT1right), rest671)
end
|  ( 20, ( ( _, ( MlyValue.FIDENT FIDENT1, _, FIDENT1right)) :: ( _, (
 MlyValue.TIPO TIPO1, TIPO1left, _)) :: rest671)) => let val  result =
 MlyValue.PARAMETRO (fn _ => let val  (TIPO as TIPO1) = TIPO1 ()
 val  (FIDENT as FIDENT1) = FIDENT1 ()
 in ((FIDENT,TIPO))
end)
 in ( LrTable.NT 15, ( result, TIPO1left, FIDENT1right), rest671)
end
|  ( 21, ( ( _, ( _, INT_KW1left, INT_KW1right)) :: rest671)) => let
 val  result = MlyValue.TIPO (fn _ => (tINT))
 in ( LrTable.NT 16, ( result, INT_KW1left, INT_KW1right), rest671)

end
|  ( 22, ( ( _, ( _, BOOL_KW1left, BOOL_KW1right)) :: rest671)) => let
 val  result = MlyValue.TIPO (fn _ => (tBOOL))
 in ( LrTable.NT 16, ( result, BOOL_KW1left, BOOL_KW1right), rest671)

end
|  ( 23, ( ( _, ( MlyValue.BOOL BOOL1, BOOL1left, BOOL1right)) :: 
rest671)) => let val  result = MlyValue.BEXP (fn _ => let val  (BOOL
 as BOOL1) = BOOL1 ()
 in (interpretaBOOL(BOOL))
end)
 in ( LrTable.NT 2, ( result, BOOL1left, BOOL1right), rest671)
end
|  ( 24, ( ( _, ( MlyValue.BIDENT BIDENT1, BIDENT1left, BIDENT1right))
 :: rest671)) => let val  result = MlyValue.BEXP (fn _ => let val  (
BIDENT as BIDENT1) = BIDENT1 ()
 in (busca_na_tabela_certa(BIDENT))
end)
 in ( LrTable.NT 2, ( result, BIDENT1left, BIDENT1right), rest671)
end
|  ( 25, ( ( _, ( MlyValue.BEXP BEXP1, _, BEXP1right)) :: ( _, ( _, 
LNOT1left, _)) :: rest671)) => let val  result = MlyValue.BEXP (fn _
 => let val  (BEXP as BEXP1) = BEXP1 ()
 in (interpretaLNOT(BEXP))
end)
 in ( LrTable.NT 2, ( result, LNOT1left, BEXP1right), rest671)
end
|  ( 26, ( ( _, ( MlyValue.BEXP BEXP2, _, BEXP2right)) :: _ :: ( _, ( 
MlyValue.BEXP BEXP1, BEXP1left, _)) :: rest671)) => let val  result = 
MlyValue.BEXP (fn _ => let val  BEXP1 = BEXP1 ()
 val  BEXP2 = BEXP2 ()
 in (interpretaAND(BEXP1,BEXP2))
end)
 in ( LrTable.NT 2, ( result, BEXP1left, BEXP2right), rest671)
end
|  ( 27, ( ( _, ( MlyValue.BEXP BEXP2, _, BEXP2right)) :: _ :: ( _, ( 
MlyValue.BEXP BEXP1, BEXP1left, _)) :: rest671)) => let val  result = 
MlyValue.BEXP (fn _ => let val  BEXP1 = BEXP1 ()
 val  BEXP2 = BEXP2 ()
 in (interpretaOR(BEXP1,BEXP2))
end)
 in ( LrTable.NT 2, ( result, BEXP1left, BEXP2right), rest671)
end
|  ( 28, ( ( _, ( _, _, RPAR1right)) :: ( _, ( MlyValue.BEXP BEXP1, _,
 _)) :: ( _, ( _, LPAR1left, _)) :: rest671)) => let val  result = 
MlyValue.BEXP (fn _ => let val  (BEXP as BEXP1) = BEXP1 ()
 in (BEXP)
end)
 in ( LrTable.NT 2, ( result, LPAR1left, RPAR1right), rest671)
end
|  ( 29, ( ( _, ( MlyValue.REXP REXP1, REXP1left, REXP1right)) :: 
rest671)) => let val  result = MlyValue.BEXP (fn _ => let val  (REXP
 as REXP1) = REXP1 ()
 in (REXP)
end)
 in ( LrTable.NT 2, ( result, REXP1left, REXP1right), rest671)
end
|  ( 30, ( ( _, ( MlyValue.NUM NUM1, NUM1left, NUM1right)) :: rest671)
) => let val  result = MlyValue.AEXP (fn _ => let val  (NUM as NUM1) =
 NUM1 ()
 in (interpretaNUM(NUM))
end)
 in ( LrTable.NT 3, ( result, NUM1left, NUM1right), rest671)
end
|  ( 31, ( ( _, ( MlyValue.AIDENT AIDENT1, AIDENT1left, AIDENT1right))
 :: rest671)) => let val  result = MlyValue.AEXP (fn _ => let val  (
AIDENT as AIDENT1) = AIDENT1 ()
 in (busca_na_tabela_certa(AIDENT))
end)
 in ( LrTable.NT 3, ( result, AIDENT1left, AIDENT1right), rest671)
end
|  ( 32, ( ( _, ( MlyValue.AEXP AEXP2, _, AEXP2right)) :: _ :: ( _, ( 
MlyValue.AEXP AEXP1, AEXP1left, _)) :: rest671)) => let val  result = 
MlyValue.AEXP (fn _ => let val  AEXP1 = AEXP1 ()
 val  AEXP2 = AEXP2 ()
 in (interpretaPLUS(AEXP1,AEXP2))
end)
 in ( LrTable.NT 3, ( result, AEXP1left, AEXP2right), rest671)
end
|  ( 33, ( ( _, ( MlyValue.AEXP AEXP2, _, AEXP2right)) :: _ :: ( _, ( 
MlyValue.AEXP AEXP1, AEXP1left, _)) :: rest671)) => let val  result = 
MlyValue.AEXP (fn _ => let val  AEXP1 = AEXP1 ()
 val  AEXP2 = AEXP2 ()
 in (interpretaSUB(AEXP1,AEXP2))
end)
 in ( LrTable.NT 3, ( result, AEXP1left, AEXP2right), rest671)
end
|  ( 34, ( ( _, ( MlyValue.AEXP AEXP2, _, AEXP2right)) :: _ :: ( _, ( 
MlyValue.AEXP AEXP1, AEXP1left, _)) :: rest671)) => let val  result = 
MlyValue.AEXP (fn _ => let val  AEXP1 = AEXP1 ()
 val  AEXP2 = AEXP2 ()
 in (interpretaTIMES(AEXP1,AEXP2))
end)
 in ( LrTable.NT 3, ( result, AEXP1left, AEXP2right), rest671)
end
|  ( 35, ( ( _, ( MlyValue.AEXP AEXP2, _, AEXP2right)) :: _ :: ( _, ( 
MlyValue.AEXP AEXP1, AEXP1left, _)) :: rest671)) => let val  result = 
MlyValue.AEXP (fn _ => let val  AEXP1 = AEXP1 ()
 val  AEXP2 = AEXP2 ()
 in (interpretaDIV(AEXP1,AEXP2))
end)
 in ( LrTable.NT 3, ( result, AEXP1left, AEXP2right), rest671)
end
|  ( 36, ( ( _, ( MlyValue.AEXP AEXP2, _, AEXP2right)) :: _ :: ( _, ( 
MlyValue.AEXP AEXP1, AEXP1left, _)) :: rest671)) => let val  result = 
MlyValue.AEXP (fn _ => let val  AEXP1 = AEXP1 ()
 val  AEXP2 = AEXP2 ()
 in (interpretaYMOD(AEXP1,AEXP1))
end)
 in ( LrTable.NT 3, ( result, AEXP1left, AEXP2right), rest671)
end
|  ( 37, ( ( _, ( _, _, RPAR1right)) :: ( _, ( MlyValue.AEXP AEXP1, _,
 _)) :: ( _, ( _, LPAR1left, _)) :: rest671)) => let val  result = 
MlyValue.AEXP (fn _ => let val  (AEXP as AEXP1) = AEXP1 ()
 in (AEXP)
end)
 in ( LrTable.NT 3, ( result, LPAR1left, RPAR1right), rest671)
end
|  ( 38, ( ( _, ( MlyValue.AEXP AEXP2, _, AEXP2right)) :: _ :: ( _, ( 
MlyValue.AEXP AEXP1, AEXP1left, _)) :: rest671)) => let val  result = 
MlyValue.REXP (fn _ => let val  AEXP1 = AEXP1 ()
 val  AEXP2 = AEXP2 ()
 in (interpretaLESS(AEXP1,AEXP2))
end)
 in ( LrTable.NT 4, ( result, AEXP1left, AEXP2right), rest671)
end
|  ( 39, ( ( _, ( MlyValue.AEXP AEXP1, _, AEXP1right)) :: _ :: ( _, ( 
MlyValue.AIDENT AIDENT1, AIDENT1left, _)) :: rest671)) => let val  
result = MlyValue.ntVOID (fn _ => ( let val  (AIDENT as AIDENT1) = 
AIDENT1 ()
 val  (AEXP as AEXP1) = AEXP1 ()
 in (interpretaATRIBUICAO(AIDENT,AEXP))
end; ()))
 in ( LrTable.NT 7, ( result, AIDENT1left, AEXP1right), rest671)
end
|  ( 40, ( ( _, ( MlyValue.BEXP BEXP1, _, BEXP1right)) :: _ :: ( _, ( 
MlyValue.BIDENT BIDENT1, BIDENT1left, _)) :: rest671)) => let val  
result = MlyValue.ntVOID (fn _ => ( let val  (BIDENT as BIDENT1) = 
BIDENT1 ()
 val  (BEXP as BEXP1) = BEXP1 ()
 in (interpretaATRIBUICAO(BIDENT,BEXP))
end; ()))
 in ( LrTable.NT 7, ( result, BIDENT1left, BEXP1right), rest671)
end
| _ => raise (mlyAction i392)
end
val void = MlyValue.VOID
val extract = fn a => (fn MlyValue.ntVOID x => x
| _ => let exception ParseInternal
	in raise ParseInternal end) a ()
end
end
structure Tokens : Be_TOKENS =
struct
type svalue = ParserData.svalue
type ('a,'b) token = ('a,'b) Token.token
fun NUM (i,p1,p2) = Token.TOKEN (ParserData.LrTable.T 0,(
ParserData.MlyValue.NUM (fn () => i),p1,p2))
fun BOOL (i,p1,p2) = Token.TOKEN (ParserData.LrTable.T 1,(
ParserData.MlyValue.BOOL (fn () => i),p1,p2))
fun IDENT (i,p1,p2) = Token.TOKEN (ParserData.LrTable.T 2,(
ParserData.MlyValue.IDENT (fn () => i),p1,p2))
fun AIDENT (i,p1,p2) = Token.TOKEN (ParserData.LrTable.T 3,(
ParserData.MlyValue.AIDENT (fn () => i),p1,p2))
fun BIDENT (i,p1,p2) = Token.TOKEN (ParserData.LrTable.T 4,(
ParserData.MlyValue.BIDENT (fn () => i),p1,p2))
fun FIDENT (i,p1,p2) = Token.TOKEN (ParserData.LrTable.T 5,(
ParserData.MlyValue.FIDENT (fn () => i),p1,p2))
fun SEMI (p1,p2) = Token.TOKEN (ParserData.LrTable.T 6,(
ParserData.MlyValue.VOID,p1,p2))
fun EOF (p1,p2) = Token.TOKEN (ParserData.LrTable.T 7,(
ParserData.MlyValue.VOID,p1,p2))
fun LAND (p1,p2) = Token.TOKEN (ParserData.LrTable.T 8,(
ParserData.MlyValue.VOID,p1,p2))
fun LOR (p1,p2) = Token.TOKEN (ParserData.LrTable.T 9,(
ParserData.MlyValue.VOID,p1,p2))
fun LPAR (p1,p2) = Token.TOKEN (ParserData.LrTable.T 10,(
ParserData.MlyValue.VOID,p1,p2))
fun RPAR (p1,p2) = Token.TOKEN (ParserData.LrTable.T 11,(
ParserData.MlyValue.VOID,p1,p2))
fun LCOL (p1,p2) = Token.TOKEN (ParserData.LrTable.T 12,(
ParserData.MlyValue.VOID,p1,p2))
fun RCOL (p1,p2) = Token.TOKEN (ParserData.LrTable.T 13,(
ParserData.MlyValue.VOID,p1,p2))
fun PLUS (p1,p2) = Token.TOKEN (ParserData.LrTable.T 14,(
ParserData.MlyValue.VOID,p1,p2))
fun SUB (p1,p2) = Token.TOKEN (ParserData.LrTable.T 15,(
ParserData.MlyValue.VOID,p1,p2))
fun DIV (p1,p2) = Token.TOKEN (ParserData.LrTable.T 16,(
ParserData.MlyValue.VOID,p1,p2))
fun TIMES (p1,p2) = Token.TOKEN (ParserData.LrTable.T 17,(
ParserData.MlyValue.VOID,p1,p2))
fun YMOD (p1,p2) = Token.TOKEN (ParserData.LrTable.T 18,(
ParserData.MlyValue.VOID,p1,p2))
fun LESS (p1,p2) = Token.TOKEN (ParserData.LrTable.T 19,(
ParserData.MlyValue.VOID,p1,p2))
fun LNOT (p1,p2) = Token.TOKEN (ParserData.LrTable.T 20,(
ParserData.MlyValue.VOID,p1,p2))
fun COMMA (p1,p2) = Token.TOKEN (ParserData.LrTable.T 21,(
ParserData.MlyValue.VOID,p1,p2))
fun RECEBE (p1,p2) = Token.TOKEN (ParserData.LrTable.T 22,(
ParserData.MlyValue.VOID,p1,p2))
fun COLON (p1,p2) = Token.TOKEN (ParserData.LrTable.T 23,(
ParserData.MlyValue.VOID,p1,p2))
fun DEATH (p1,p2) = Token.TOKEN (ParserData.LrTable.T 24,(
ParserData.MlyValue.VOID,p1,p2))
fun INT_KW (p1,p2) = Token.TOKEN (ParserData.LrTable.T 25,(
ParserData.MlyValue.VOID,p1,p2))
fun BOOL_KW (p1,p2) = Token.TOKEN (ParserData.LrTable.T 26,(
ParserData.MlyValue.VOID,p1,p2))
fun FUN_KW (p1,p2) = Token.TOKEN (ParserData.LrTable.T 27,(
ParserData.MlyValue.VOID,p1,p2))
fun PROGRAM_KW (p1,p2) = Token.TOKEN (ParserData.LrTable.T 28,(
ParserData.MlyValue.VOID,p1,p2))
fun VARIABLES_KW (p1,p2) = Token.TOKEN (ParserData.LrTable.T 29,(
ParserData.MlyValue.VOID,p1,p2))
fun PAF_KW (p1,p2) = Token.TOKEN (ParserData.LrTable.T 30,(
ParserData.MlyValue.VOID,p1,p2))
end
end
