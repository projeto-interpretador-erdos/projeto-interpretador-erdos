(* be.sml *)

(* This file provides glue code for building the calculator using the
 * parser and lexer specified in calc.lex and calc.grm.
*)


structure Be : sig
    val parse : unit -> unit
end = 
struct

(* 
 * We apply the functors generated from calc.lex and calc.grm to produce
 * the CalcParser structure.
 *)

  structure BeLrVals =
    BeLrValsFun(structure Token = LrParser.Token)

  structure BeLex =
    BeLexFun(structure Tokens = BeLrVals.Tokens)

  structure BeParser =
    Join(structure LrParser = LrParser
	 structure ParserData = BeLrVals.ParserData
	 structure Lex = BeLex)

(* 
 * We need a function which given a lexer invokes the parser. The
 * function invoke does this.
 *)

  fun invoke lexstream =
      let fun print_error (s,i:int,_) =
	      TextIO.output(TextIO.stdOut,
			    "Error, line " ^ (Int.toString i) ^ ", " ^ s ^ "\n")
       in BeParser.parse(0,lexstream,print_error,())
      end

(* 
 * Finally, we need a driver function that reads one or more expressions
 * from the standard input. The function parse, shown below, does
 * this. It runs the calculator on the standard input and terminates when
 * an end-of-file is encountered.
 *)

open interpretador
  fun parse () = 
      let val lexer = BeParser.makeLexer (fn _ => (case TextIO.inputLine TextIO.stdIn
						      of SOME s => s
						       | _ => ""))
	  val dummyEOF = BeLrVals.Tokens.EOF(0,0)
	  val dummyDEATH = BeLrVals.Tokens.DEATH(0,0)
	  fun loop lexer =
	      let val (result,lexer) = invoke lexer
		  val (nextToken,lexer) = BeParser.Stream.get lexer
	      in if BeParser.sameToken(nextToken,dummyEOF) then ()
		 else loop lexer
	      end
      in HashTable.clear tabela_variaveis;
	 pilha_de_tabelas := [];
	 loop lexer
      end
      
end (* structure Be *)
