datatype nomes = tipoB | tipoI;
datatype valores = I of int | B of bool;
type par = nomes * valores;

val par1 = (tipoB, B true):par;
val par2 = (tipoI, I 2):par;

fun oqeh (p:par):string = 
    case p 
     of (tipoI, I inteiro) => Int.toString(inteiro) 
      | (tipoB, B booleano) => if booleano then "trueeee"else "falseee";

oqeh par1;
oqeh par2;
