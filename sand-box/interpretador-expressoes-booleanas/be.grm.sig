signature Be_TOKENS =
sig
type ('a,'b) token
type svalue
val ELSE_KW:  'a * 'a -> (svalue,'a) token
val THEN_KW:  'a * 'a -> (svalue,'a) token
val IF_KW:  'a * 'a -> (svalue,'a) token
val RETURN_KW:  'a * 'a -> (svalue,'a) token
val FUN_BD_KW:  'a * 'a -> (svalue,'a) token
val FUN_KW:  'a * 'a -> (svalue,'a) token
val BOOL_KW:  'a * 'a -> (svalue,'a) token
val INT_KW:  'a * 'a -> (svalue,'a) token
val DOT:  'a * 'a -> (svalue,'a) token
val COLON:  'a * 'a -> (svalue,'a) token
val RECEBE:  'a * 'a -> (svalue,'a) token
val COMMA:  'a * 'a -> (svalue,'a) token
val LNOT:  'a * 'a -> (svalue,'a) token
val LESS:  'a * 'a -> (svalue,'a) token
val YMOD:  'a * 'a -> (svalue,'a) token
val TIMES:  'a * 'a -> (svalue,'a) token
val DIV:  'a * 'a -> (svalue,'a) token
val SUB:  'a * 'a -> (svalue,'a) token
val PLUS:  'a * 'a -> (svalue,'a) token
val RCHAV:  'a * 'a -> (svalue,'a) token
val LCHAV:  'a * 'a -> (svalue,'a) token
val RCOL:  'a * 'a -> (svalue,'a) token
val LCOL:  'a * 'a -> (svalue,'a) token
val RPAR:  'a * 'a -> (svalue,'a) token
val LPAR:  'a * 'a -> (svalue,'a) token
val LOR:  'a * 'a -> (svalue,'a) token
val LAND:  'a * 'a -> (svalue,'a) token
val EOF:  'a * 'a -> (svalue,'a) token
val SEMI:  'a * 'a -> (svalue,'a) token
val IDENT: (string) *  'a * 'a -> (svalue,'a) token
val BOOL: (bool) *  'a * 'a -> (svalue,'a) token
val NUM: (int) *  'a * 'a -> (svalue,'a) token
end
signature Be_LRVALS=
sig
structure Tokens : Be_TOKENS
structure ParserData:PARSER_DATA
sharing type ParserData.Token.token = Tokens.token
sharing type ParserData.svalue = Tokens.svalue
end
