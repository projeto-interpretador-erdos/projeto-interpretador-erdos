
structure Tokens = Tokens
open interpretador

type pos = int
type svalue = Tokens.svalue
type ('a,'b) token = ('a,'b) Tokens.token
type lexresult= (svalue,pos) token

val pos = ref 0
fun eof () = Tokens.EOF(!pos,!pos)
fun error (e,l : int,_) = TextIO.output (TextIO.stdOut, String.concat["line ", (Int.toString l), ": ", e, "\n" 
		 ])

%%
%header (functor BeLexFun(structure Tokens: Be_TOKENS));
alpha=[A-Za-z];
digit=[0-9];
ws = [\ \t];
%%
\n         => (pos := (!pos) + 1; lex());
{ws}+      => (lex());
{digit}+   => (Tokens.NUM (valOf (Int.fromString yytext), !pos, !pos));
"true"     => (Tokens.BOOL    (true, !pos,!pos));
"false"    => (Tokens.BOOL   (false, !pos,!pos));
";"        => (Tokens.SEMI          (!pos,!pos));
"&&"       => (Tokens.LAND          (!pos,!pos));
"||"       => (Tokens.LOR           (!pos,!pos));
"("        => (Tokens.LPAR          (!pos,!pos));
")"        => (Tokens.RPAR          (!pos,!pos));
"["        => (Tokens.LCOL          (!pos,!pos));
"]"        => (Tokens.RCOL          (!pos,!pos));
"{"        => (Tokens.LCHAV         (!pos,!pos));
"}"        => (Tokens.RCHAV         (!pos,!pos));
"+"        => (Tokens.PLUS          (!pos,!pos));
"-"        => (Tokens.SUB           (!pos,!pos));
"/"        => (Tokens.DIV           (!pos,!pos));
"*"        => (Tokens.TIMES         (!pos,!pos));
"mod"      => (Tokens.YMOD          (!pos,!pos));
"<"        => (Tokens.LESS          (!pos,!pos));
"!"        => (Tokens.LNOT          (!pos,!pos));
","        => (Tokens.COMMA         (!pos,!pos));
"int"      => (Tokens.INT_KW        (!pos,!pos));
"bool"     => (Tokens.BOOL_KW       (!pos,!pos));
"function" => (Tokens.FUN_KW        (!pos,!pos));
"return"   => (Tokens.RETURN_KW     (!pos,!pos));
"body"     => (Tokens.FUN_BD_KW     (!pos,!pos));
"if"       => (Tokens.IF_KW         (!pos,!pos));
"then"     => (Tokens.THEN_KW       (!pos,!pos));
"else"     => (Tokens.ELSE_KW       (!pos,!pos));
":"        => (Tokens.COLON         (!pos,!pos));
":="       => (Tokens.RECEBE        (!pos,!pos)); 
"."        => (Tokens.DOT           (!pos,!pos));
{alpha}+   => (Tokens.IDENT  (yytext,!pos,!pos));


