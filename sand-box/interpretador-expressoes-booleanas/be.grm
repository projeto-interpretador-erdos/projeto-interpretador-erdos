open interpretador

%%
%eop EOF SEMI

%pos int

%left RECEBE
%left LOR LAND
%left LNOT
%left LESS
%left PLUS SUB
%left TIMES DIV
%left YMOD


%term NUM of int | 
      BOOL of bool |
      IDENT of string | 
      SEMI | EOF |
      LAND | LOR |
      LPAR | RPAR | LCOL | RCOL | LCHAV | RCHAV |
      PLUS | SUB | DIV | TIMES| YMOD |
      LESS | LNOT | 
      COMMA | RECEBE | COLON |
      DOT |
      INT_KW | BOOL_KW | FUN_KW | FUN_BD_KW | RETURN_KW | IF_KW | THEN_KW | ELSE_KW


(* tipo_exp eh o tipo de expressoes *)
%nonterm START of expressao option 
       | EXP of expressao 
       | ACTUAL_PARAMETER_LIST of expressao list
       | LIST_OF_VAR_NAMES of (atomo_ou_lista * string) list 
       | ATRIBUICAO of expressao 
       | FUN_DECL | FUN_DEF 
       | PAR_LIST | PARAMETER | SIMPLE_TYPE  | CONDICAO of expressao | OPTIONAL_ELSE of expressao option | CHAMADA_DE_FUNCAO of expressao 
       | ASSINATURA_FUNCAO of (tipo_basico * (atomo_ou_lista * string)) list
       | LISTA_PARAMETROS of (tipo_basico * (atomo_ou_lista * string)) list
       | PARAMETRO of  tipo_basico * (atomo_ou_lista * string) | TIPO of tipo_basico
       | CORPO_FN of expressao | LISTA_COMANDOS of expressao list
       | COMANDO of expressao | ACTUAL_LIST of expressao list
       | TYPE_DEFINITION | ACESSO_A_ATRIBUTO of expressao
       | VAR_IDENT of (atomo_ou_lista * string)

%name Be
%noshift EOF
%verbose

%%

START : TIPO LIST_OF_VAR_NAMES (novos_identificadores (TIPO, LIST_OF_VAR_NAMES); NONE)
      | TYPE_DEFINITION (NONE)
      | FUN_KW IDENT COLON TIPO LPAR ASSINATURA_FUNCAO (
	novo_identificador (IDENT, tF (ASSINATURA_FUNCAO,lista_de_expressoes nil)); NONE)
      | FUN_BD_KW IDENT COLON TIPO LPAR ASSINATURA_FUNCAO LCHAV CORPO_FN (
	novo_identificador(IDENT, tF (ASSINATURA_FUNCAO, CORPO_FN));
	NONE)
      | COMANDO (imprime_expressao (evaluate(COMANDO)); NONE)
      | (NONE)
TYPE_DEFINITION : IDENT LCHAV LISTA_PARAMETROS RCHAV (
		  novos_identificadores(tTYPE IDENT, (make_type LISTA_PARAMETROS)::nil); NONE)
CORPO_FN : RCHAV (lista_de_expressoes nil)
	 | LISTA_COMANDOS RCHAV (lista_de_expressoes LISTA_COMANDOS)
LISTA_COMANDOS : COMANDO (COMANDO::nil)
	       | COMANDO COMMA LISTA_COMANDOS (COMANDO::LISTA_COMANDOS)
COMANDO : RETURN_KW EXP (opRETURN (EXP))
	| CONDICAO (CONDICAO)
	| CHAMADA_DE_FUNCAO (CHAMADA_DE_FUNCAO)
	| EXP (EXP)
	| ATRIBUICAO (ATRIBUICAO)
CONDICAO : IF_KW LPAR EXP RPAR THEN_KW LCHAV LISTA_COMANDOS RCHAV OPTIONAL_ELSE (
	   condicao (EXP,lista_de_expressoes LISTA_COMANDOS, OPTIONAL_ELSE))
OPTIONAL_ELSE : RCHAV (NONE)
	      | ELSE_KW LCHAV LISTA_COMANDOS RCHAV (SOME (lista_de_expressoes LISTA_COMANDOS))
LIST_OF_VAR_NAMES : VAR_IDENT (VAR_IDENT::nil)
		  | VAR_IDENT COMMA LIST_OF_VAR_NAMES (VAR_IDENT::LIST_OF_VAR_NAMES)
VAR_IDENT : IDENT (um_atomo, IDENT)
	  | IDENT LCOL RCOL (uma_lista, IDENT)
ASSINATURA_FUNCAO : RPAR (nil)
		  | LISTA_PARAMETROS RPAR (LISTA_PARAMETROS)
LISTA_PARAMETROS : PARAMETRO (PARAMETRO::nil)
		 | PARAMETRO COMMA LISTA_PARAMETROS (PARAMETRO::LISTA_PARAMETROS)
PARAMETRO : TIPO VAR_IDENT ((TIPO,VAR_IDENT))
CHAMADA_DE_FUNCAO : IDENT LPAR ACTUAL_PARAMETER_LIST 
		(chamada_de_funcao(IDENT,lista_de_expressoes ACTUAL_PARAMETER_LIST))

TIPO : INT_KW (tINT)
     | BOOL_KW (tBOOL)
     | IDENT (tVERTEX IDENT)
EXP  : BOOL (tB BOOL)
     | IDENT (nome_de_variavel IDENT)
     | ACESSO_A_ATRIBUTO (ACESSO_A_ATRIBUTO)
     | CHAMADA_DE_FUNCAO (CHAMADA_DE_FUNCAO)
     | LNOT EXP (opNOT (EXP))
     | EXP LAND EXP (opLAND (EXP1,EXP2))
     | EXP LOR  EXP (opLOR (EXP1,EXP2))
     | LPAR EXP RPAR (EXP)
     | EXP LESS EXP (opLESS (EXP1,EXP2))
     | NUM (tI NUM)
     | EXP PLUS EXP (opPLUS (EXP1,EXP2))
     | EXP SUB EXP (opMINUS (EXP1,EXP2))
     | EXP TIMES EXP (opTIMES (EXP1,EXP2))
     | EXP DIV EXP (opDIV (EXP1,EXP2))
     | EXP YMOD EXP (opMOD (EXP1,EXP1))
ATRIBUICAO : IDENT RECEBE EXP (atribuicao (IDENT,EXP))
	   | ACESSO_A_ATRIBUTO RECEBE EXP (atribuicao_a_atributo (ACESSO_A_ATRIBUTO, EXP))
ACTUAL_PARAMETER_LIST : RPAR (nil)
		      | ACTUAL_LIST RPAR (ACTUAL_LIST)
ACTUAL_LIST : EXP (EXP::nil)
	    | EXP COMMA ACTUAL_LIST (EXP::ACTUAL_LIST)
ACESSO_A_ATRIBUTO : IDENT DOT IDENT (
		    acesso_a_atributo (nome_de_variavel IDENT1, IDENT2))
		  | LPAR EXP RPAR DOT IDENT (
		    acesso_a_atributo (EXP, IDENT))
		  | CHAMADA_DE_FUNCAO DOT IDENT (
		    acesso_a_atributo (CHAMADA_DE_FUNCAO, IDENT))
