exception cant;
val initial_size:int = 101;

structure H = HashTable;
structure Hs = HashString;



val hash_fn: string->word = Hs.hashString;

val cmp_fn: string*string->bool = (op =);

val t:(string,int) H.hash_table=H.mkTable(hash_fn,cmp_fn)(initial_size,cant);

H.insert t ("eric", 30);
H.insert t ("greg", 33);
t;

H.lookup t "greg";
H.lookup t "eric";
H.insert t ("greg", 50);
H.lookup t "greg";
(*H.lookup t "douglas adams";*)

