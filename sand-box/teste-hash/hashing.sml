(* raised when I do a lookup and can't find something *)
exception Can't_Find_It

(* used to convert a string to a hash code -- i.e., word *)
val hash_fn : string->word = HashString.hashString

(* used to compare keys in the hash table *)
fun cmp_fn() : string*string->bool = (op =)

(* initial table size -- need something relatively prime and
* approximately the size of the number of elements you expect
* to be in the hash table.  Note that the implementation resizes
* as needed to avoid major collisions. *)
val initial_size : int = 101

val t : (string,int) table = mkTable (hash_fn, cmp_fn)
(initial_size, Can't_Find_it)

insert t ("Eric",30);
insert t ("Jason",24);
lookup t "Jason" (* returns 30 *)
lookup t "Daniel" (* raises Can't_Find_It exception *)
