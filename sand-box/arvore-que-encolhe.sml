datatype tree = leaf | node of tree * tree;

val t = (node (leaf,node(leaf,leaf)));

fun percorre_mas_nao_encolhe (t:tree):tree = 
    case t 
     of node (a,b) => node (percorre_mas_nao_encolhe(a),
			   percorre_mas_nao_encolhe(b))
      | leaf => leaf;

percorre_mas_nao_encolhe t;

fun percorre_encolhendo (t:tree):tree = 
    case t 
     of node (a,b) =>
	let val a' = percorre_encolhendo(a)
	    val b' = percorre_encolhendo(b)
	in case (a',b') 
	    of (leaf,leaf) => leaf (* combina folhas e pai *)
	     | _ => node (a',b')
	end
      | leaf => leaf;

percorre_encolhendo (t);
