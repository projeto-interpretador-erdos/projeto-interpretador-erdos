(* Função itera pela lista somando os valores *)
fun it (l:int list):int =
    case l of nil => 0
	    | x::tail => x + it tail;

val lista1 = [1,2,3,4,5];

it lista1;


(* Agora uma função que aplica eval aos elementos de uma lista *)

type exp = int;

fun eval(e:exp):int = e;

val a1:exp = 13; (* um valor do tipo exp *)
eval a1;

(* lista de exp a partir de lista de inteiros *)
fun lexp_from_lint (l:int list):exp list =
    case l of nil => nil
	    | i::t => 
	      let 
		  val aux:exp = i 
	      in 
		  i :: lexp_from_lint t 
	      end;

val l1 = lexp_from_lint [1,3,5,7,9];
val l2 = lexp_from_lint [2,4,6,10,8];

(* retorna uma lista com elementos "eval"iados,
   ou seja, lista de inteiros *)
fun itera_eval (l:exp list):int list =
    case l of nil => nil
	    | e::t => eval e :: itera_eval t;

itera_eval l1;
itera_eval l2;

