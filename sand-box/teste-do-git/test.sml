fun factorial 0 = 1
| factorial (x : int) : int = x * factorial(x - 1);

