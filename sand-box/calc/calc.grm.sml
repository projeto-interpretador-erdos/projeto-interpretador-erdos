functor CalcLrValsFun(structure Token : TOKEN)
 : sig structure ParserData : PARSER_DATA
       structure Tokens : Calc_TOKENS
   end
 = 
struct
structure ParserData=
struct
structure Header = 
struct
(* Sample interactive calculator for ML-Yacc *)

(* Esta é uma variável-estado do interpretador. *)
val tabelaMemoria = ref 0

fun lookup "bogus" = 10000
  | lookup s = (tabelaMemoria := !tabelaMemoria + 1; !tabelaMemoria)

datatype int_or_bool = Int of int | Bool of bool

end
structure LrTable = Token.LrTable
structure Token = Token
local open LrTable in 
val table=let val actionRows =
"\
\\001\000\001\000\007\000\002\000\006\000\000\000\
\\001\000\010\000\000\000\011\000\000\000\000\000\
\\020\000\000\000\
\\021\000\001\000\007\000\002\000\006\000\009\000\005\000\000\000\
\\022\000\007\000\012\000\008\000\011\000\012\000\010\000\013\000\009\000\
\\014\000\008\000\000\000\
\\023\000\007\000\012\000\008\000\011\000\012\000\010\000\013\000\009\000\
\\014\000\008\000\000\000\
\\025\000\000\000\
\\026\000\000\000\
\\027\000\008\000\011\000\012\000\010\000\013\000\009\000\000\000\
\\028\000\012\000\010\000\000\000\
\\029\000\012\000\010\000\000\000\
\\030\000\008\000\011\000\012\000\010\000\013\000\009\000\000\000\
\\031\000\012\000\010\000\000\000\
\"
val actionRowNumbers =
"\003\000\002\000\005\000\000\000\
\\006\000\007\000\000\000\000\000\
\\000\000\000\000\000\000\004\000\
\\011\000\010\000\012\000\009\000\
\\008\000\001\000"
val gotoT =
"\
\\001\000\002\000\002\000\017\000\003\000\001\000\000\000\
\\000\000\
\\000\000\
\\001\000\011\000\000\000\
\\000\000\
\\000\000\
\\001\000\012\000\000\000\
\\001\000\013\000\000\000\
\\001\000\014\000\000\000\
\\001\000\015\000\000\000\
\\001\000\016\000\000\000\
\\000\000\
\\000\000\
\\000\000\
\\000\000\
\\000\000\
\\000\000\
\\000\000\
\"
val numstates = 18
val numrules = 16
val s = ref "" and index = ref 0
val string_to_int = fn () => 
let val i = !index
in index := i+2; Char.ord(String.sub(!s,i)) + Char.ord(String.sub(!s,i+1)) * 256
end
val string_to_list = fn s' =>
    let val len = String.size s'
        fun f () =
           if !index < len then string_to_int() :: f()
           else nil
   in index := 0; s := s'; f ()
   end
val string_to_pairlist = fn (conv_key,conv_entry) =>
     let fun f () =
         case string_to_int()
         of 0 => EMPTY
          | n => PAIR(conv_key (n-1),conv_entry (string_to_int()),f())
     in f
     end
val string_to_pairlist_default = fn (conv_key,conv_entry) =>
    let val conv_row = string_to_pairlist(conv_key,conv_entry)
    in fn () =>
       let val default = conv_entry(string_to_int())
           val row = conv_row()
       in (row,default)
       end
   end
val string_to_table = fn (convert_row,s') =>
    let val len = String.size s'
        fun f ()=
           if !index < len then convert_row() :: f()
           else nil
     in (s := s'; index := 0; f ())
     end
local
  val memo = Array.array(numstates+numrules,ERROR)
  val _ =let fun g i=(Array.update(memo,i,REDUCE(i-numstates)); g(i+1))
       fun f i =
            if i=numstates then g i
            else (Array.update(memo,i,SHIFT (STATE i)); f (i+1))
          in f 0 handle General.Subscript => ()
          end
in
val entry_to_action = fn 0 => ACCEPT | 1 => ERROR | j => Array.sub(memo,(j-2))
end
val gotoT=Array.fromList(string_to_table(string_to_pairlist(NT,STATE),gotoT))
val actionRows=string_to_table(string_to_pairlist_default(T,entry_to_action),actionRows)
val actionRowNumbers = string_to_list actionRowNumbers
val actionT = let val actionRowLookUp=
let val a=Array.fromList(actionRows) in fn i=>Array.sub(a,i) end
in Array.fromList(List.map actionRowLookUp actionRowNumbers)
end
in LrTable.mkLrTable {actions=actionT,gotos=gotoT,numRules=numrules,
numStates=numstates,initialState=STATE 0}
end
end
local open Header in
type pos = int
type arg = unit
structure MlyValue = 
struct
datatype svalue = VOID | ntVOID of unit ->  unit
 | BOOL of unit ->  (bool) | NUM of unit ->  (int)
 | ID of unit ->  (string) | BOOL_EXP of unit ->  (bool)
 | START_BOOL of unit ->  (bool) | START_INT of unit ->  (int)
 | START of unit ->  (int_or_bool option) | EXP of unit ->  (int)
end
type svalue = MlyValue.svalue
type result = int_or_bool option
end
structure EC=
struct
open LrTable
infix 5 $$
fun x $$ y = y::x
val is_keyword =
fn (T 8) => true | (T 9) => true | _ => false
val preferred_change : (term list * term list) list = 
(nil
 $$ (T 0),nil
 $$ (T 8))::
(nil
,nil
 $$ (T 6))::
(nil
,nil
 $$ (T 7))::
(nil
,nil
 $$ (T 12))::
(nil
,nil
 $$ (T 13))::
nil
val noShift = 
fn (T 10) => true | _ => false
val showTerminal =
fn (T 0) => "ID"
  | (T 1) => "NUM"
  | (T 2) => "BOOL"
  | (T 3) => "LOGICAL_AND"
  | (T 4) => "LOGICAL_OR"
  | (T 5) => "LOGICAL_NOT"
  | (T 6) => "PLUS"
  | (T 7) => "TIMES"
  | (T 8) => "PRINT"
  | (T 9) => "SEMI"
  | (T 10) => "EOF"
  | (T 11) => "CARAT"
  | (T 12) => "DIV"
  | (T 13) => "SUB"
  | _ => "bogus-term"
local open Header in
val errtermvalue=
fn (T 0) => MlyValue.ID(fn () => ("bogus")) | 
_ => MlyValue.VOID
end
val terms : term list = nil
 $$ (T 13) $$ (T 12) $$ (T 11) $$ (T 10) $$ (T 9) $$ (T 8) $$ (T 7)
 $$ (T 6) $$ (T 5) $$ (T 4) $$ (T 3)end
structure Actions =
struct 
exception mlyAction of int
local open Header in
val actions = 
fn (i392,defaultPos,stack,
    (()):arg) =>
case (i392,stack)
of  ( 0, ( ( _, ( MlyValue.START_INT START_INT1, START_INT1left, 
START_INT1right)) :: rest671)) => let val  result = MlyValue.START (fn
 _ => let val  (START_INT as START_INT1) = START_INT1 ()
 in (SOME START_INT)
end)
 in ( LrTable.NT 1, ( result, START_INT1left, START_INT1right), 
rest671)
end
|  ( 1, ( rest671)) => let val  result = MlyValue.START (fn _ => (NONE
))
 in ( LrTable.NT 1, ( result, defaultPos, defaultPos), rest671)
end
|  ( 2, ( ( _, ( MlyValue.EXP EXP1, _, EXP1right)) :: ( _, ( _, 
PRINT1left, _)) :: rest671)) => let val  result = MlyValue.START_INT
 (fn _ => let val  (EXP as EXP1) = EXP1 ()
 in (print (Int.toString EXP);
			 print "\n";
			 EXP)
end)
 in ( LrTable.NT 2, ( result, PRINT1left, EXP1right), rest671)
end
|  ( 3, ( ( _, ( MlyValue.EXP EXP1, EXP1left, EXP1right)) :: rest671))
 => let val  result = MlyValue.START_INT (fn _ => let val  (EXP as 
EXP1) = EXP1 ()
 in (EXP)
end)
 in ( LrTable.NT 2, ( result, EXP1left, EXP1right), rest671)
end
|  ( 4, ( ( _, ( MlyValue.BOOL_EXP BOOL_EXP1, BOOL_EXP1left, 
BOOL_EXP1right)) :: rest671)) => let val  result = MlyValue.START_BOOL
 (fn _ => let val  (BOOL_EXP as BOOL_EXP1) = BOOL_EXP1 ()
 in (BOOL_EXP)
end)
 in ( LrTable.NT 3, ( result, BOOL_EXP1left, BOOL_EXP1right), rest671)

end
|  ( 5, ( ( _, ( MlyValue.NUM NUM1, NUM1left, NUM1right)) :: rest671))
 => let val  result = MlyValue.EXP (fn _ => let val  (NUM as NUM1) = 
NUM1 ()
 in (NUM)
end)
 in ( LrTable.NT 0, ( result, NUM1left, NUM1right), rest671)
end
|  ( 6, ( ( _, ( MlyValue.ID ID1, ID1left, ID1right)) :: rest671)) =>
 let val  result = MlyValue.EXP (fn _ => let val  (ID as ID1) = ID1 ()
 in (lookup ID)
end)
 in ( LrTable.NT 0, ( result, ID1left, ID1right), rest671)
end
|  ( 7, ( ( _, ( MlyValue.EXP EXP2, _, EXP2right)) :: _ :: ( _, ( 
MlyValue.EXP EXP1, EXP1left, _)) :: rest671)) => let val  result = 
MlyValue.EXP (fn _ => let val  EXP1 = EXP1 ()
 val  EXP2 = EXP2 ()
 in (EXP1+EXP2)
end)
 in ( LrTable.NT 0, ( result, EXP1left, EXP2right), rest671)
end
|  ( 8, ( ( _, ( MlyValue.EXP EXP2, _, EXP2right)) :: _ :: ( _, ( 
MlyValue.EXP EXP1, EXP1left, _)) :: rest671)) => let val  result = 
MlyValue.EXP (fn _ => let val  EXP1 = EXP1 ()
 val  EXP2 = EXP2 ()
 in (EXP1*EXP2)
end)
 in ( LrTable.NT 0, ( result, EXP1left, EXP2right), rest671)
end
|  ( 9, ( ( _, ( MlyValue.EXP EXP2, _, EXP2right)) :: _ :: ( _, ( 
MlyValue.EXP EXP1, EXP1left, _)) :: rest671)) => let val  result = 
MlyValue.EXP (fn _ => let val  EXP1 = EXP1 ()
 val  EXP2 = EXP2 ()
 in (EXP1 div EXP2)
end)
 in ( LrTable.NT 0, ( result, EXP1left, EXP2right), rest671)
end
|  ( 10, ( ( _, ( MlyValue.EXP EXP2, _, EXP2right)) :: _ :: ( _, ( 
MlyValue.EXP EXP1, EXP1left, _)) :: rest671)) => let val  result = 
MlyValue.EXP (fn _ => let val  EXP1 = EXP1 ()
 val  EXP2 = EXP2 ()
 in (EXP1-EXP2)
end)
 in ( LrTable.NT 0, ( result, EXP1left, EXP2right), rest671)
end
|  ( 11, ( ( _, ( MlyValue.EXP EXP2, _, EXP2right)) :: _ :: ( _, ( 
MlyValue.EXP EXP1, EXP1left, _)) :: rest671)) => let val  result = 
MlyValue.EXP (fn _ => let val  EXP1 = EXP1 ()
 val  EXP2 = EXP2 ()
 in (
let fun e (m,0) = 1
                                | e (m,l) = m*e(m,l-1)
                         in e (EXP1,EXP2)
                         end
)
end)
 in ( LrTable.NT 0, ( result, EXP1left, EXP2right), rest671)
end
|  ( 12, ( ( _, ( MlyValue.BOOL BOOL1, BOOL1left, BOOL1right)) :: 
rest671)) => let val  result = MlyValue.BOOL_EXP (fn _ => let val  (
BOOL as BOOL1) = BOOL1 ()
 in (BOOL)
end)
 in ( LrTable.NT 4, ( result, BOOL1left, BOOL1right), rest671)
end
|  ( 13, ( ( _, ( MlyValue.BOOL_EXP BOOL_EXP2, _, BOOL_EXP2right)) ::
 _ :: ( _, ( MlyValue.BOOL_EXP BOOL_EXP1, BOOL_EXP1left, _)) :: 
rest671)) => let val  result = MlyValue.BOOL_EXP (fn _ => let val  
BOOL_EXP1 = BOOL_EXP1 ()
 val  BOOL_EXP2 = BOOL_EXP2 ()
 in (BOOL_EXP1 andalso BOOL_EXP2)
end)
 in ( LrTable.NT 4, ( result, BOOL_EXP1left, BOOL_EXP2right), rest671)

end
|  ( 14, ( ( _, ( MlyValue.BOOL_EXP BOOL_EXP2, _, BOOL_EXP2right)) ::
 _ :: ( _, ( MlyValue.BOOL_EXP BOOL_EXP1, BOOL_EXP1left, _)) :: 
rest671)) => let val  result = MlyValue.BOOL_EXP (fn _ => let val  
BOOL_EXP1 = BOOL_EXP1 ()
 val  BOOL_EXP2 = BOOL_EXP2 ()
 in (BOOL_EXP1 orelse  BOOL_EXP2)
end)
 in ( LrTable.NT 4, ( result, BOOL_EXP1left, BOOL_EXP2right), rest671)

end
|  ( 15, ( ( _, ( MlyValue.BOOL_EXP BOOL_EXP1, _, BOOL_EXP1right)) :: 
( _, ( _, LOGICAL_NOT1left, _)) :: rest671)) => let val  result = 
MlyValue.BOOL_EXP (fn _ => let val  BOOL_EXP1 = BOOL_EXP1 ()
 in ((not BOOL_EXP1))
end)
 in ( LrTable.NT 4, ( result, LOGICAL_NOT1left, BOOL_EXP1right), 
rest671)
end
| _ => raise (mlyAction i392)
end
val void = MlyValue.VOID
val extract = fn a => (fn MlyValue.START x => x
| _ => let exception ParseInternal
	in raise ParseInternal end) a ()
end
end
structure Tokens : Calc_TOKENS =
struct
type svalue = ParserData.svalue
type ('a,'b) token = ('a,'b) Token.token
fun ID (i,p1,p2) = Token.TOKEN (ParserData.LrTable.T 0,(
ParserData.MlyValue.ID (fn () => i),p1,p2))
fun NUM (i,p1,p2) = Token.TOKEN (ParserData.LrTable.T 1,(
ParserData.MlyValue.NUM (fn () => i),p1,p2))
fun BOOL (i,p1,p2) = Token.TOKEN (ParserData.LrTable.T 2,(
ParserData.MlyValue.BOOL (fn () => i),p1,p2))
fun LOGICAL_AND (p1,p2) = Token.TOKEN (ParserData.LrTable.T 3,(
ParserData.MlyValue.VOID,p1,p2))
fun LOGICAL_OR (p1,p2) = Token.TOKEN (ParserData.LrTable.T 4,(
ParserData.MlyValue.VOID,p1,p2))
fun LOGICAL_NOT (p1,p2) = Token.TOKEN (ParserData.LrTable.T 5,(
ParserData.MlyValue.VOID,p1,p2))
fun PLUS (p1,p2) = Token.TOKEN (ParserData.LrTable.T 6,(
ParserData.MlyValue.VOID,p1,p2))
fun TIMES (p1,p2) = Token.TOKEN (ParserData.LrTable.T 7,(
ParserData.MlyValue.VOID,p1,p2))
fun PRINT (p1,p2) = Token.TOKEN (ParserData.LrTable.T 8,(
ParserData.MlyValue.VOID,p1,p2))
fun SEMI (p1,p2) = Token.TOKEN (ParserData.LrTable.T 9,(
ParserData.MlyValue.VOID,p1,p2))
fun EOF (p1,p2) = Token.TOKEN (ParserData.LrTable.T 10,(
ParserData.MlyValue.VOID,p1,p2))
fun CARAT (p1,p2) = Token.TOKEN (ParserData.LrTable.T 11,(
ParserData.MlyValue.VOID,p1,p2))
fun DIV (p1,p2) = Token.TOKEN (ParserData.LrTable.T 12,(
ParserData.MlyValue.VOID,p1,p2))
fun SUB (p1,p2) = Token.TOKEN (ParserData.LrTable.T 13,(
ParserData.MlyValue.VOID,p1,p2))
end
end
