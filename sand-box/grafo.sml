(* 
 * Aqui um vértice é um inteiro.
 * E um gafo é:
 * - um conjunto de vértices
 * - um conjunto de (vértice * vértice * aresta)
 *) 

datatype edge = E of int * int * string;


fun compara_arestas (E (a,b,_),E (c,d,_)) =
    let fun ordered(x,y) = if x < y then (x,y) else (y,x);
	val (a',b') = ordered(a,b);
	val (c',d') = ordered(c,d);
    in
    if ((a' = c') andalso (b' = d')) then EQUAL
    else
	if (a' < c') orelse ((a' = c') andalso (b' < d')) then LESS 
	else 
	    if (a' > c') orelse ((a' = c') andalso (b' > d')) then GREATER
	    else EQUAL
    end;

structure EKey = struct
type ord_key = edge
val compare = compara_arestas
end;

structure ESet : ORD_SET = RedBlackSetFn(EKey);

val A = ref IntBinarySet.empty;
val B = ref ESet.empty;


(* Alguns vértices *)
val vlist = [3,4,5,6,7];

A:= IntBinarySet.addList (!A, vlist);
IntBinarySet.listItems (!A);

B:= ESet.add (!B, E (4, 6, "simples"));
ESet.listItems (!B);


fun nova_aresta (i, j, nome) =
    B:= ESet.add (!B, E (i,j,nome));


val a = E (3, 5, "sortuda");  B:= ESet.add(!B,a ); ESet.listItems (!B);
val b = E (3, 5, "einstein"); B:= ESet.add(!B,b ); ESet.listItems (!B);
val c = E (4, 7, "elevada");  B:= ESet.add(!B,c ); ESet.listItems (!B);
val d = E (5, 4, "parecida"); B:= ESet.add(!B,d ); ESet.listItems (!B);
val e = E (4, 5, "mesma");    B:= ESet.add(!B,e ); ESet.listItems (!B);

EKey.compare(a, b);
EKey.compare(d, e);

(* Estamos prontos para definir o grafo *)

datatype grafo = G of IntBinarySet.set ref * ESet.set ref;

val a = G (A,B);

(* Lista de vértices do grafo *)
fun vertices (G(v,e)) = IntBinarySet.listItems (!v);

(* Agora uma função para imprimir um grafo *)
fun print (what:string) = (TextIO.output(TextIO.stdOut, what));

fun imprime (G (v,e)) =
    print("vértices: {" 
	  ^ string_lista_vertices ((IntBinarySet.listItems (!v)))
	  ^ "}\narestas:\n" 
	  ^ string_arestas (ESet.listItems (!e)))
and string_lista_vertices (lv) =
    (case lv
      of nil => ""
       | (h::nil) => Int.toString (h)
       | (h::t) => Int.toString (h) ^ ", " ^ string_lista_vertices (t))
and string_arestas (le) = 
    (case le
      of nil => ""
       | ((E (v1,v2,e))::t) => ("  "
			    ^ Int.toString (v1) 
			    ^ " -- "
			    ^ e ^ " -- " 
			    ^ Int.toString (v2) 
			    ^ "\n"
			    ^ string_arestas (t)));


imprime (a);

(* Agora que sei imprimir, quero encontrar os vizinhos de v *)
fun has_extremes (a:int,b:int) = 
 fn (E(x,y,_)) => 
    (((x = a) andalso (y = b))
     orelse
     ((x = b) andalso (y = a)));


fun has_extreme (i:int) = 
 fn (E(x,y,_)) => ((x = i) orelse (y = i));

fun limpa (x:int, le:edge list) = 
    case le of ((E(a,b,_))::t) => (if a = x then b else a) :: limpa(x,t)
	| _ => nil;

fun neighbors (x:int, G(a,b)) =
    limpa(x,
	  ESet.listItems ((ESet.filter (has_extreme(x))) (!b)));

neighbors (3,a);
neighbors (4,a);
neighbors (5,a);
neighbors (6,a);

(* E a aresta que liga v1 e v2 *)
fun get_edge (x:int, y:int, G(a,b)) =
    ESet.listItems ((ESet.filter (has_extremes(x,y))) (!b));

string_arestas (get_edge (3,5,a));
string_arestas (get_edge (5,4,a));

(* Adicionando vértices *)
fun add_vertices(lista:int list, G(a,b)) =
    a:= IntBinarySet.addList (!a, lista);

print "Acrescentando vértices\n";
imprime(a);
add_vertices([33,44,55],a);
imprime(a);

(* Remoção de vértices *)
fun remove_single_vertex (x:int, G(a,b)) =
    (a:= IntBinarySet.delete(!a,x);
     b:= (ESet.filter (fn (e) => (not ((has_extreme x) (e))))) (!b))
fun remove_vertices (vList:int list, G(a,b)) =
    (case vList 
      of (h::t) => (remove_single_vertex (h,G(a,b));
		    remove_vertices (t,G(a,b)))
      | _ => ());

print "Removendo vértices\n";
imprime(a);
remove_vertices([44,5], a);
imprime(a);

(* precisamos de uma função que para cada dois vértices 
 * dá uma aresta ou nada (aresta option *)
fun cria_aresta (x:int, y:int) =
    if (x+y) mod 3 <> 0 
    then SOME (E(x,y, Int.toString (x+y)
		      ^ " tem resto " ^
		      Int.toString ((x+y) mod 3)
		      ^ " mod 3"))
    else NONE;

(* Vamos usar também uma variante do map, que omite itens
 * NONE que porventura sejam retornado por uma chamada de
 * cria_aresta *)
fun my_map_partial (f:'a -> ('b option)) (l:'a list):'b list =
    (case (l)
     of nil => nil
      | x::xs => (case (f x)
		   of SOME y => y::(my_map_partial f xs)
		    | NONE => (my_map_partial f xs)));

(* A função itera_em_pares recebe
 *
 * - uma lista de vértices, e 
 * - uma função criadora de arestas,
 *
 * e retorna uma lista com as arestas que essa
 * função produz, quando chamada (uma vez) 
 * para cada par de vértices do grafo *)
fun itera_em_pares (vList:int list, f:(int*int) -> (edge option)) =
    (case vList
      of nil => nil
       | (h::t) => (my_map_partial (fn (x) => f(h,x)) t)
		   @ itera_em_pares(t,f));
			 
itera_em_pares ([~1,2,3,4,5],
		fn (x,y) => SOME (E(x,y,Int.toString(x*y))));


(* Agora vamos acrescentar as arestas que itera_em_pares
 * cria ao grafo. *)

fun add_edges(f:(int*int)->(edge option), g:grafo) =
    case g
     of G(v,e) =>
	let val lista_vertices = vertices (G(v,e)) in
	    e := ESet.addList(!e, itera_em_pares(lista_vertices, f))
	end;

print "Adicionando arestas\n";
imprime(a);
add_edges(cria_aresta, a);
imprime(a);

(* Remoção de arestas é parecida com a de vértices *)
fun remove_edges (f:(int *int) -> bool, G(a,b)) =
    (b:= (ESet.filter (fn (E (x,y,_)) => (not (f (x,y)))) (!b)));

print "Removendo arestas que tem ambos os extremos ímpares\n";
fun ambos_impares(x,y) = ((x mod 2) = 1 andalso (y mod 2) = 1);

imprime(a);
remove_edges( ambos_impares, a);
imprime(a);

