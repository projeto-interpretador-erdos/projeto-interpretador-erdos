(* Este arquivo deTokenve conter
 - especificacao do tipo tipo_exp
 - implementacao das funções do interpretador
 - estado do interpretador (tabelas)
*)



structure interpretador = 
struct


datatype expression = EA of expressao_aritmetica 
		    | EB of expressao_booleana
		    | EL of expressao_do_tipo_lista
     and
     expressao_aritmetica = atomo_numerico
			  | UNARY_MINUS of expressao_aritmetica
			  | opPLUS of expressao_aritmetica * expressao_aritmetica
			  | opMINUS of expressao_aritmetica * expressao_aritmetica
			  | opTIMES of expressao_aritmetica * expressao_aritmetica
			  | opDIV of expressao_aritmetica * expressao_aritmetica
			  | opMOD of expressao_aritmetica * expressao_aritmetica
     and
     expressao_booleana =  atomo_booleano
			| opNOT of expressao_booleana
			| ER of expressao_relacional
			| opLAND of expressao_booleana * expressao_booleana 
			| opLOR of expressao_booleana * expressao_booleana 
     and
     expressao_relacional = opLESS of expressao_aritmetica * expressao_aritmetica
     and 
     expressao_do_tipo_lista = ATOM_L of atomo_lista
			     | opTAIL of expressao_do_tipo_lista
			     | opCONCAT of expressao_do_tipo_lista * expressao_do_tipo_lista
     and
     atomo_numerico = ATOM_A of tipo_basico * valor_basico
		    | opLENGTH of expressao_do_tipo_lista
		    | ATOM_AG of atomo_geral
     and
     atomo_booleano = ATOM_B of tipo_basico * valor_basico
		    | ATOM_BG of atomo_geral
     and 
     atomo_lista = lista_explicita of tipo_basico * valor_basico list
		 | opVIZINHOS of atomo_geral
     and
     atomo_geral = nome_de_variavel of string
		 | acesso_a_atributo of expression * string
		 | chamada_de_funcao of string * (expression list)
		 | opHEAD of expressao_do_tipo_lista
     and
     comando = chamada of string * (expression list)
	     | atribuicao of string * expression
	     | retorno of expression
	     | condicao of expression * expression * (expression option)
	     | add_vertice of string * expression (* grafo, lista de vértices *)
	     | add_aresta of string * string (* grafo, função *)
	     | rem_vertice of string * expression (* grafo, lista de vértices *)
	     | rem_aresta of string * string (* grafo, função *)
	     | get_aresta of string * expression * expression (* grafo, v1, v2 *)

     and
     valor_basico = tI of int | tB of bool 
		  | tIF of ((string * tipo_basico) list) * (comando list) 
		  | tIB of ((string * tipo_basico) list) * (comando list) 
     and 
     tipo_basico = tINT | tBOOL | tINT_FUN | tBOOL_FUN; (* tINT_LIST, ... *)
type tipo_exp = tipo_basico * valor_basico;
type tipo_valor_do_programa = tipo_basico * valor_basico;

(* ~~~~~ Acabaram as definições de tipos ~~~~~~~~~~~ *)

fun string_de_tipo_basico (t:tipo_basico):string =
    case t
     of tINT => "int"
      | tBOOL => "bool"
      | tINT_FUN => "funcao inteira"
      | tBOOL_FUN => "funcao booleana";
fun interpretaNUM (x:int):tipo_exp = (tINT, tI x);    
fun interpretaBOOL (b:bool):tipo_exp = (tBOOL, tB b);

(* Expressões Booleanas *)
fun interpretaAND (b1:tipo_exp,b2:tipo_exp):tipo_exp = 
    (tBOOL, case ((#2 b1),(#2 b2)) of (tB a,tB b) => tB (a andalso b));
    
fun interpretaOR (b1:tipo_exp,b2:tipo_exp):tipo_exp = 
    (tBOOL, case ((#2 b1),(#2 b2)) of (tB a,tB b) => tB (a orelse b));
    
fun interpretaLNOT(b:tipo_exp):tipo_exp = 
    (tBOOL, case (#2 b) of tB a => tB (not a));
    
    
fun interpretaPLUS (n1:tipo_exp,n2:tipo_exp):tipo_exp = 
    (tINT, case ((#2 n1),(#2 n2)) of (tI a,tI b) => tI (a + b));
fun interpretaSUB (n1:tipo_exp,n2:tipo_exp):tipo_exp = 
    (tINT, case ((#2 n1),(#2 n2)) of (tI a,tI b) => tI (a - b));
fun interpretaTIMES (n1:tipo_exp,n2:tipo_exp):tipo_exp = 
    (tINT, case ((#2 n1),(#2 n2)) of (tI a,tI b) => tI (a * b));
fun interpretaDIV (n1:tipo_exp,n2:tipo_exp):tipo_exp = 
    (tINT, case ((#2 n1),(#2 n2)) of (tI a,tI b) => tI (a div b));
fun interpretaYMOD (n1:tipo_exp,n2:tipo_exp):tipo_exp = 
    (tINT, case ((#2 n1),(#2 n2)) of (tI a,tI b) => tI (a mod b));
    
    
fun interpretaLESS (n1:tipo_exp,n2:tipo_exp):tipo_exp = 
    (tBOOL, case ((#2 n1),(#2 n2)) of (tI a,tI b) => tB (a < b));
    

(* Tabela de variáveis *)
exception nao_acho_na_tabela_de_simbolos;
val initial_size:int = 101;
(* ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ *)
val hash_fn: string->word = HashString.hashString;
    
val cmp_fn: string*string->bool = (op =);

val tabela_variaveis:(string,tipo_valor_do_programa) HashTable.hash_table=HashTable.mkTable(hash_fn,cmp_fn)(initial_size,nao_acho_na_tabela_de_simbolos);
(* ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ *)

(* Note que no futuro, se deixarmos de usar tipos
   de mesmo tipo que os do domínio da linguagem Erdos
   o pattern-match da esquerda vai ter tipos diferentes
   do da direita (o par esquerdo e o direito serão 
   diferentes *)
fun valor_variavel_int(nome:string):tipo_exp =
    case (HashTable.lookup tabela_variaveis nome)
     of (tINT, tI a) => (tINT, tI a);

fun valor_variavel_bool(nome:string):tipo_exp =
    case (HashTable.lookup tabela_variaveis nome)
     of (tBOOL,tB a) => (tBOOL, tB a);

fun valor_variavel(nome:string):tipo_exp =
    case (HashTable.lookup tabela_variaveis nome)
     of (tBOOL,tB a) => (tBOOL, tB a)
      | (tINT,tI a) => (tINT, tI a);



fun string_de_lista_de_parametros(parametros:(string * tipo_basico) list):string =
    case parametros
     of nil => ""
      | (nome,tipo)::demais_parametros => 
	(nome ^ ":" ^ string_de_tipo_basico(tipo)
	 ^ (case demais_parametros 
	     of nil => "" 
	      | _ => (", " ^ string_de_lista_de_parametros(demais_parametros))))

fun string_de_lista_de_comandos(comandos:comando list):string = " ... ";

fun imprime_descricao_de_funcao (lparametros,lcomandos) =
    "fun "
    ^ "(" ^ string_de_lista_de_parametros(lparametros) ^ ") -> "
    ^ "{" ^ string_de_lista_de_comandos(lcomandos) ^ "}";

fun stringfy_valor_de_expressao(exp:tipo_exp):string =
    case exp 
     of (tINT,  tI i) => Int.toString i
      | (tBOOL, tB b) => if b then "trueee" else "falseee"
      | (tINT_FUN, tIF funcao) => imprime_descricao_de_funcao(funcao)
      | (tBOOL_FUN, tIB funcao) => imprime_descricao_de_funcao(funcao);

fun imprime_expressao(exp:tipo_exp):unit = 
    TextIO.output(TextIO.stdOut, 
		  (stringfy_valor_de_expressao exp) ^ "\n")

fun interpretaATRIBUICAO (nome:string,x:tipo_exp):unit =
    (
     imprime_expressao(x);
     HashTable.insert tabela_variaveis (nome, x)
    )

end
