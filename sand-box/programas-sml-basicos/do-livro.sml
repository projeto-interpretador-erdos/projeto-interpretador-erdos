(* Não está completo! *)
signature REGEXP = sig
    datatype regexp =
	     Zero | One | Char of char |
	     Plus of regexp * regexp |
	     Times of regexp * regexp |
	     Star of regexp
    exception SyntaxError of string
    val parse : string -> regexp
    val format : regexp -> string
end

signature MATCHER = sig
    structure RegExp = REGEXP
    val accepts : RegExp.regexp -> string -> bool
end

(* The implementation of the signatures are structures *)
(* The notation :> means "ascribing" the signature to the structure *)
structure RegExp :> REGEXP = ...
structure Matcher :> MATCHER = ...


(* Usage example *)

val regexp = 
    Matcher.Regexp.parse "(a+b)*"
val matches =
    Matcher.accepts regexp

val ex1 matches "aabba" (* yields true *)
val ex2 matches "abac" (* yields false *)

