fun f(x) = case x of (h::t) => (h,t) | _ => (123,42::nil);
f [1];
f [];
f[2,3];
f[2,3,4];
