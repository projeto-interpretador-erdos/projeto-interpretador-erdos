structure IntKey = struct
type ord_key = int
val compare = (fn (x,y) => if x < y then LESS else (if x > y then GREATER else EQUAL))
end;

structure IntSet : ORD_SET = RedBlackSetFn(IntKey);

val A = ref IntSet.empty;
A := IntSet.add (!A, 1);
A := IntSet.add (!A, 3);
A := IntSet.addList (!A, [111,222,333]);
IntSet.listItems (!A);
