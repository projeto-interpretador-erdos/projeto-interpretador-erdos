Um arquivo markdown
===================


# Queria saber se o Gitorious formata este


Vértices (variáveis do tipo vertex ou de um tipo
de vértice definido pelo usuário) são referências
para uma estrutura de dados que contém os valores
de cada atributo do vértice. Assim, ao passar um
vértice para uma função, o que é passado é uma
referência, e operações alterando o valor de 
atributos do vértice serão visíveis em toda parte.
isso é particularmente útil para operações que 
varrem o grafo e precisam marcar de alguma forma
vértices visitados.

Para obter um vértice novo com que operar, criamos
um operador new_vertex( type ), que recebe um tipo
de vértice, e gera um vértice desse tipo. 


# Vértices e arestas que são resultado de expressões

Para acessar a um atributo de um vértice ou aresta
que é retornado por uma função, está em uma lista,
ou é obtido por alguma operação, é preciso envolver
a dita operação entre parênteses. Assim

    (head( lista )).id := 17
    
É uma construção válida, assim como

    pessoa { list int numeros_favoritos };
    pessoa Godel, Escher, Bach;
    list pessoa geb;
    geb := [ Godel, Escher, Bach ];
    Godel.numeros_favoritos := 42;
    
    (head( tail( tail( geb )))).id := 5;

E ao fim das operações, o valor de geb será

    geb -> [#0 = {numeros_favoritos:42} : pessoa, 
            #1 = {} : pessoa, 
	    #2 = {id:5} : pessoa]
