datatype tree = leaf of bool (* diz se é encolhível ou não *)
	      | node of tree * tree;

fun imprime_folha (v:bool) = if v then "+" else "-";
fun imprime (t:tree) = 
    case t
     of leaf v => imprime_folha (v)
      | node (a,b) => "{" ^ imprime (a) ^ "," ^ imprime (b) ^ "}"

val t = (node (leaf true,node(node(node(leaf true,leaf true),leaf false),leaf true)));
imprime t;

fun percorre_mas_nao_encolhe (t:tree):tree = 
    case t 
     of node (a,b) => node (percorre_mas_nao_encolhe(a),
			   percorre_mas_nao_encolhe(b))
      | leaf v=> leaf v;

imprime (percorre_mas_nao_encolhe t);

fun percorre_encolhendo (t:tree):tree = 
    case t 
     of node (a,b) =>
	let val a' = percorre_encolhendo(a)
	    val b' = percorre_encolhendo(b)
	in case (a',b') 
	    of (leaf true,leaf true) => leaf false(* combina folhas e pai *)
	     | _ => node (a',b')
	end
      | leaf v => leaf v;

"percorre encolhendo";
imprime (percorre_encolhendo (t));

(* folhas que resultam de encolhimento podem ser encolhidas novamente? *)
val continua_encolhendo = false

(* encolhe só se ambas as folhas forem encolhíveis *)
fun percorre_e_encolhe_se_der (t:tree):tree =
    case t
     of node (a,b) => 
	let val a' = percorre_e_encolhe_se_der (a)
	    val b' = percorre_e_encolhe_se_der (b)
	in case (a',b')
	    of (leaf true,leaf true) => leaf continua_encolhendo
	     | _ => node (a',b')
	end
      | leaf _ => t;

"agora soh se der";
imprime (percorre_e_encolhe_se_der (t));

continua_encolhendo=true;
imprime (percorre_e_encolhe_se_der (t));
imprime (percorre_e_encolhe_se_der  (percorre_e_encolhe_se_der (t)));
