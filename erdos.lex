
structure Tokens = Tokens
open interpretador

type pos = int
type svalue = Tokens.svalue
type ('a,'b) token = ('a,'b) Tokens.token
type lexresult= (svalue,pos) token

val pos = ref 0
fun eof () = Tokens.EOF(!pos,!pos)
fun error (e,l : int,_) = TextIO.output (TextIO.stdOut, String.concat["line ", (Int.toString l), ": ", e, "\n" 
		 ])

%%
%header (functor ErdosLexFun(structure Tokens: Erdos_TOKENS));
alpha=[_A-Za-z];
digit=[0-9];
ws = [\ \t];
stringchar=[-!? \ A-Za-z0-9];
%%
\n         => (pos := (!pos) + 1; lex());
{ws}+      => (lex());
\"{stringchar}*\"	  => (Tokens.STRING (yytext, !pos,!pos));{digit}+   => (Tokens.NUM (valOf (Int.fromString yytext), !pos, !pos));
"true"     => (Tokens.BOOL    (true, !pos,!pos));
"false"    => (Tokens.BOOL   (false, !pos,!pos));
";"        => (Tokens.SEMI          (!pos,!pos));
"&&"       => (Tokens.LAND          (!pos,!pos));
"||"       => (Tokens.LOR           (!pos,!pos));
"("        => (Tokens.LPAR          (!pos,!pos));
")"        => (Tokens.RPAR          (!pos,!pos));
"["        => (Tokens.LCOL          (!pos,!pos));
"]"        => (Tokens.RCOL          (!pos,!pos));
"{"        => (Tokens.LCHAV         (!pos,!pos));
"}"        => (Tokens.RCHAV         (!pos,!pos));
"+"        => (Tokens.PLUS          (!pos,!pos));
"-"        => (Tokens.SUB           (!pos,!pos));
"/"        => (Tokens.DIV           (!pos,!pos));
"*"        => (Tokens.TIMES         (!pos,!pos));
"mod"      => (Tokens.YMOD          (!pos,!pos));
"<="        => (Tokens.LOEQ          (!pos,!pos));
">="        => (Tokens.GOEQ          (!pos,!pos));
"!="        => (Tokens.DIFF         (!pos,!pos));
"!"         => (Tokens.LNOT         (!pos,!pos));
"<"        => (Tokens.LESS          (!pos,!pos));
">"        => (Tokens.GREATER          (!pos,!pos));
"="        => (Tokens.EQUAL         (!pos,!pos));
"="        => (Tokens.EQUAL         (!pos,!pos));
","        => (Tokens.COMMA         (!pos,!pos));
"end"      => (Tokens.END_KW        (!pos,!pos));
"int"      => (Tokens.INT_KW        (!pos,!pos));
"bool"     => (Tokens.BOOL_KW       (!pos,!pos));
"string"   => (Tokens.STRING_KW     (!pos,!pos));
"list"     => (Tokens.LIST_OF_KW    (!pos,!pos));
"vertex"   => (Tokens.VERTEX_KW     (!pos,!pos));
"edge"     => (Tokens.EDGE_KW       (!pos,!pos));
"graph"    => (Tokens.GRAPH_KW      (!pos,!pos));
"function" => (Tokens.FUN_KW        (!pos,!pos));
"return"   => (Tokens.RETURN_KW     (!pos,!pos));
"body"     => (Tokens.FUN_BD_KW     (!pos,!pos));
"if"       => (Tokens.IF_KW         (!pos,!pos));
"then"     => (Tokens.THEN_KW       (!pos,!pos));
"else"     => (Tokens.ELSE_KW       (!pos,!pos));
"head"     => (Tokens.HEAD_KW       (!pos,!pos));
"tail"     => (Tokens.TAIL_KW       (!pos,!pos));
"concat"   => (Tokens.CONCAT_KW     (!pos,!pos));
"length"   => (Tokens.LENGTH_KW     (!pos,!pos));
"new_vertex"      => (Tokens.NEW_VERTEX_KW  (!pos,!pos));
"new_edge"        => (Tokens.NEW_EDGE_KW    (!pos,!pos));
"add_vertices"    => (Tokens.ADD_VERTICES_KW   (!pos,!pos));
"add_edges"       => (Tokens.ADD_EDGES_KW      (!pos,!pos)); 
"remove_vertices" => (Tokens.REMOVE_VERTICES_KW   (!pos,!pos));
"remove_edges"    => (Tokens.REMOVE_EDGES_KW      (!pos,!pos));
"nil"             => (Tokens.NIL                  (!pos,!pos));
"neighbors"       => (Tokens.NEIGHBORS_KW         (!pos,!pos));
"get_edge"       => (Tokens.GET_EDGE_KW           (!pos,!pos));
":"        => (Tokens.COLON         (!pos,!pos));
":="       => (Tokens.GETS          (!pos,!pos)); 
"."        => (Tokens.DOT           (!pos,!pos));
{alpha}+   => (Tokens.IDENT  (yytext,!pos,!pos));


