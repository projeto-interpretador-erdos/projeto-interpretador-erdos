functor ErdosLexFun(structure Tokens: Erdos_TOKENS)  = struct

    structure yyInput : sig

        type stream
	val mkStream : (int -> string) -> stream
	val fromStream : TextIO.StreamIO.instream -> stream
	val getc : stream -> (Char.char * stream) option
	val getpos : stream -> int
	val getlineNo : stream -> int
	val subtract : stream * stream -> string
	val eof : stream -> bool
	val lastWasNL : stream -> bool

      end = struct

        structure TIO = TextIO
        structure TSIO = TIO.StreamIO
	structure TPIO = TextPrimIO

        datatype stream = Stream of {
            strm : TSIO.instream,
	    id : int,  (* track which streams originated 
			* from the same stream *)
	    pos : int,
	    lineNo : int,
	    lastWasNL : bool
          }

	local
	  val next = ref 0
	in
	fun nextId() = !next before (next := !next + 1)
	end

	val initPos = 2 (* ml-lex bug compatibility *)

	fun mkStream inputN = let
              val strm = TSIO.mkInstream 
			   (TPIO.RD {
			        name = "lexgen",
				chunkSize = 4096,
				readVec = SOME inputN,
				readArr = NONE,
				readVecNB = NONE,
				readArrNB = NONE,
				block = NONE,
				canInput = NONE,
				avail = (fn () => NONE),
				getPos = NONE,
				setPos = NONE,
				endPos = NONE,
				verifyPos = NONE,
				close = (fn () => ()),
				ioDesc = NONE
			      }, "")
	      in 
		Stream {strm = strm, id = nextId(), pos = initPos, lineNo = 1,
			lastWasNL = true}
	      end

	fun fromStream strm = Stream {
		strm = strm, id = nextId(), pos = initPos, lineNo = 1, lastWasNL = true
	      }

	fun getc (Stream {strm, pos, id, lineNo, ...}) = (case TSIO.input1 strm
              of NONE => NONE
	       | SOME (c, strm') => 
		   SOME (c, Stream {
			        strm = strm', 
				pos = pos+1, 
				id = id,
				lineNo = lineNo + 
					 (if c = #"\n" then 1 else 0),
				lastWasNL = (c = #"\n")
			      })
	     (* end case*))

	fun getpos (Stream {pos, ...}) = pos

	fun getlineNo (Stream {lineNo, ...}) = lineNo

	fun subtract (new, old) = let
	      val Stream {strm = strm, pos = oldPos, id = oldId, ...} = old
	      val Stream {pos = newPos, id = newId, ...} = new
              val (diff, _) = if newId = oldId andalso newPos >= oldPos
			      then TSIO.inputN (strm, newPos - oldPos)
			      else raise Fail 
				"BUG: yyInput: attempted to subtract incompatible streams"
	      in 
		diff 
	      end

	fun eof s = not (isSome (getc s))

	fun lastWasNL (Stream {lastWasNL, ...}) = lastWasNL

      end

    datatype yystart_state = 
INITIAL
    structure UserDeclarations = 
      struct


structure Tokens = Tokens
open interpretador

type pos = int
type svalue = Tokens.svalue
type ('a,'b) token = ('a,'b) Tokens.token
type lexresult= (svalue,pos) token

val pos = ref 0
fun eof () = Tokens.EOF(!pos,!pos)
fun error (e,l : int,_) = TextIO.output (TextIO.stdOut, String.concat["line ", (Int.toString l), ": ", e, "\n" 
		 ])



      end

    datatype yymatch 
      = yyNO_MATCH
      | yyMATCH of yyInput.stream * action * yymatch
    withtype action = yyInput.stream * yymatch -> UserDeclarations.lexresult

    local

    val yytable = 
#[([(#"\t",#"\t",1),
(#" ",#" ",1),
(#"\n",#"\n",2),
(#"!",#"!",3),
(#"\"",#"\"",4),
(#"&",#"&",5),
(#"(",#"(",6),
(#")",#")",7),
(#"*",#"*",8),
(#"+",#"+",9),
(#",",#",",10),
(#"-",#"-",11),
(#".",#".",12),
(#"/",#"/",13),
(#"0",#"9",14),
(#":",#":",15),
(#";",#";",16),
(#"<",#"<",17),
(#"=",#"=",18),
(#">",#">",19),
(#"A",#"Z",20),
(#"_",#"_",20),
(#"d",#"d",20),
(#"j",#"k",20),
(#"o",#"q",20),
(#"u",#"u",20),
(#"w",#"z",20),
(#"[",#"[",21),
(#"]",#"]",22),
(#"a",#"a",23),
(#"b",#"b",24),
(#"c",#"c",25),
(#"e",#"e",26),
(#"f",#"f",27),
(#"g",#"g",28),
(#"h",#"h",29),
(#"i",#"i",30),
(#"l",#"l",31),
(#"m",#"m",32),
(#"n",#"n",33),
(#"r",#"r",34),
(#"s",#"s",35),
(#"t",#"t",36),
(#"v",#"v",37),
(#"{",#"{",38),
(#"|",#"|",39),
(#"}",#"}",40)], []), ([(#"\t",#"\t",1),
(#" ",#" ",1)], [1]), ([], [0]), ([(#"=",#"=",183)], [23]), ([(#" ",#"!",4),
(#"-",#"-",4),
(#"0",#"9",4),
(#"?",#"?",4),
(#"A",#"Z",4),
(#"a",#"z",4),
(#"\"",#"\"",182)], []), ([(#"&",#"&",181)], []), ([], [9]), ([], [10]), ([], [18]), ([], [15]), ([], [28]), ([], [16]), ([], [58]), ([], [17]), ([(#"0",#"9",14)], [3]), ([(#"=",#"=",180)], [56]), ([], [6]), ([(#"=",#"=",179)], [24]), ([], [26, 27]), ([(#"=",#"=",178)], [25]), ([(#"A",#"Z",20),
(#"_",#"_",20),
(#"a",#"z",20)], [59]), ([], [11]), ([], [12]), ([(#"A",#"Z",20),
(#"_",#"_",20),
(#"a",#"c",20),
(#"e",#"z",20),
(#"d",#"d",162)], [59]), ([(#"A",#"Z",20),
(#"_",#"_",20),
(#"a",#"n",20),
(#"p",#"z",20),
(#"o",#"o",157)], [59]), ([(#"A",#"Z",20),
(#"_",#"_",20),
(#"a",#"n",20),
(#"p",#"z",20),
(#"o",#"o",152)], [59]), ([(#"A",#"Z",20),
(#"_",#"_",20),
(#"a",#"c",20),
(#"e",#"k",20),
(#"m",#"m",20),
(#"o",#"z",20),
(#"d",#"d",144),
(#"l",#"l",145),
(#"n",#"n",146)], [59]), ([(#"A",#"Z",20),
(#"_",#"_",20),
(#"b",#"t",20),
(#"v",#"z",20),
(#"a",#"a",133),
(#"u",#"u",134)], [59]), ([(#"A",#"Z",20),
(#"_",#"_",20),
(#"a",#"d",20),
(#"f",#"q",20),
(#"s",#"z",20),
(#"e",#"e",122),
(#"r",#"r",123)], [59]), ([(#"A",#"Z",20),
(#"_",#"_",20),
(#"a",#"d",20),
(#"f",#"z",20),
(#"e",#"e",119)], [59]), ([(#"A",#"Z",20),
(#"_",#"_",20),
(#"a",#"e",20),
(#"g",#"m",20),
(#"o",#"z",20),
(#"f",#"f",116),
(#"n",#"n",117)], [59]), ([(#"A",#"Z",20),
(#"_",#"_",20),
(#"a",#"d",20),
(#"f",#"h",20),
(#"j",#"z",20),
(#"e",#"e",108),
(#"i",#"i",109)], [59]), ([(#"A",#"Z",20),
(#"_",#"_",20),
(#"a",#"n",20),
(#"p",#"z",20),
(#"o",#"o",106)], [59]), ([(#"A",#"Z",20),
(#"_",#"_",20),
(#"a",#"d",20),
(#"f",#"h",20),
(#"j",#"z",20),
(#"e",#"e",84),
(#"i",#"i",85)], [59]), ([(#"A",#"Z",20),
(#"_",#"_",20),
(#"a",#"d",20),
(#"f",#"z",20),
(#"e",#"e",61)], [59]), ([(#"A",#"Z",20),
(#"_",#"_",20),
(#"a",#"s",20),
(#"u",#"z",20),
(#"t",#"t",56)], [59]), ([(#"A",#"Z",20),
(#"_",#"_",20),
(#"b",#"g",20),
(#"i",#"q",20),
(#"s",#"z",20),
(#"a",#"a",47),
(#"h",#"h",48),
(#"r",#"r",49)], [59]), ([(#"A",#"Z",20),
(#"_",#"_",20),
(#"a",#"d",20),
(#"f",#"z",20),
(#"e",#"e",42)], [59]), ([], [13]), ([(#"|",#"|",41)], []), ([], [14]), ([], [8]), ([(#"A",#"Z",20),
(#"_",#"_",20),
(#"a",#"q",20),
(#"s",#"z",20),
(#"r",#"r",43)], [59]), ([(#"A",#"Z",20),
(#"_",#"_",20),
(#"a",#"s",20),
(#"u",#"z",20),
(#"t",#"t",44)], [59]), ([(#"A",#"Z",20),
(#"_",#"_",20),
(#"a",#"d",20),
(#"f",#"z",20),
(#"e",#"e",45)], [59]), ([(#"A",#"Z",20),
(#"_",#"_",20),
(#"a",#"w",20),
(#"y",#"z",20),
(#"x",#"x",46)], [59]), ([(#"A",#"Z",20),
(#"_",#"_",20),
(#"a",#"z",20)], [34, 59]), ([(#"A",#"Z",20),
(#"_",#"_",20),
(#"a",#"h",20),
(#"j",#"z",20),
(#"i",#"i",54)], [59]), ([(#"A",#"Z",20),
(#"_",#"_",20),
(#"a",#"d",20),
(#"f",#"z",20),
(#"e",#"e",52)], [59]), ([(#"A",#"Z",20),
(#"_",#"_",20),
(#"a",#"t",20),
(#"v",#"z",20),
(#"u",#"u",50)], [59]), ([(#"A",#"Z",20),
(#"_",#"_",20),
(#"a",#"d",20),
(#"f",#"z",20),
(#"e",#"e",51)], [59]), ([(#"A",#"Z",20),
(#"_",#"_",20),
(#"a",#"z",20)], [4, 59]), ([(#"A",#"Z",20),
(#"_",#"_",20),
(#"a",#"m",20),
(#"o",#"z",20),
(#"n",#"n",53)], [59]), ([(#"A",#"Z",20),
(#"_",#"_",20),
(#"a",#"z",20)], [41, 59]), ([(#"A",#"Z",20),
(#"_",#"_",20),
(#"a",#"k",20),
(#"m",#"z",20),
(#"l",#"l",55)], [59]), ([(#"A",#"Z",20),
(#"_",#"_",20),
(#"a",#"z",20)], [44, 59]), ([(#"A",#"Z",20),
(#"_",#"_",20),
(#"a",#"q",20),
(#"s",#"z",20),
(#"r",#"r",57)], [59]), ([(#"A",#"Z",20),
(#"_",#"_",20),
(#"a",#"h",20),
(#"j",#"z",20),
(#"i",#"i",58)], [59]), ([(#"A",#"Z",20),
(#"_",#"_",20),
(#"a",#"m",20),
(#"o",#"z",20),
(#"n",#"n",59)], [59]), ([(#"A",#"Z",20),
(#"_",#"_",20),
(#"a",#"f",20),
(#"h",#"z",20),
(#"g",#"g",60)], [59]), ([(#"A",#"Z",20),
(#"_",#"_",20),
(#"a",#"z",20)], [32, 59]), ([(#"A",#"Z",20),
(#"_",#"_",20),
(#"a",#"l",20),
(#"n",#"s",20),
(#"u",#"z",20),
(#"m",#"m",62),
(#"t",#"t",63)], [59]), ([(#"A",#"Z",20),
(#"_",#"_",20),
(#"a",#"n",20),
(#"p",#"z",20),
(#"o",#"o",67)], [59]), ([(#"A",#"Z",20),
(#"_",#"_",20),
(#"a",#"t",20),
(#"v",#"z",20),
(#"u",#"u",64)], [59]), ([(#"A",#"Z",20),
(#"_",#"_",20),
(#"a",#"q",20),
(#"s",#"z",20),
(#"r",#"r",65)], [59]), ([(#"A",#"Z",20),
(#"_",#"_",20),
(#"a",#"m",20),
(#"o",#"z",20),
(#"n",#"n",66)], [59]), ([(#"A",#"Z",20),
(#"_",#"_",20),
(#"a",#"z",20)], [38, 59]), ([(#"A",#"Z",20),
(#"_",#"_",20),
(#"a",#"u",20),
(#"w",#"z",20),
(#"v",#"v",68)], [59]), ([(#"A",#"Z",20),
(#"_",#"_",20),
(#"a",#"d",20),
(#"f",#"z",20),
(#"e",#"e",69)], [59]), ([(#"A",#"Z",20),
(#"a",#"z",20),
(#"_",#"_",70)], [59]), ([(#"A",#"Z",20),
(#"_",#"_",20),
(#"a",#"d",20),
(#"f",#"u",20),
(#"w",#"z",20),
(#"e",#"e",71),
(#"v",#"v",72)], [59]), ([(#"A",#"Z",20),
(#"_",#"_",20),
(#"a",#"c",20),
(#"e",#"z",20),
(#"d",#"d",80)], [59]), ([(#"A",#"Z",20),
(#"_",#"_",20),
(#"a",#"d",20),
(#"f",#"z",20),
(#"e",#"e",73)], [59]), ([(#"A",#"Z",20),
(#"_",#"_",20),
(#"a",#"q",20),
(#"s",#"z",20),
(#"r",#"r",74)], [59]), ([(#"A",#"Z",20),
(#"_",#"_",20),
(#"a",#"s",20),
(#"u",#"z",20),
(#"t",#"t",75)], [59]), ([(#"A",#"Z",20),
(#"_",#"_",20),
(#"a",#"h",20),
(#"j",#"z",20),
(#"i",#"i",76)], [59]), ([(#"A",#"Z",20),
(#"_",#"_",20),
(#"a",#"b",20),
(#"d",#"z",20),
(#"c",#"c",77)], [59]), ([(#"A",#"Z",20),
(#"_",#"_",20),
(#"a",#"d",20),
(#"f",#"z",20),
(#"e",#"e",78)], [59]), ([(#"A",#"Z",20),
(#"_",#"_",20),
(#"a",#"r",20),
(#"t",#"z",20),
(#"s",#"s",79)], [59]), ([(#"A",#"Z",20),
(#"_",#"_",20),
(#"a",#"z",20)], [51, 59]), ([(#"A",#"Z",20),
(#"_",#"_",20),
(#"a",#"f",20),
(#"h",#"z",20),
(#"g",#"g",81)], [59]), ([(#"A",#"Z",20),
(#"_",#"_",20),
(#"a",#"d",20),
(#"f",#"z",20),
(#"e",#"e",82)], [59]), ([(#"A",#"Z",20),
(#"_",#"_",20),
(#"a",#"r",20),
(#"t",#"z",20),
(#"s",#"s",83)], [59]), ([(#"A",#"Z",20),
(#"_",#"_",20),
(#"a",#"z",20)], [52, 59]), ([(#"A",#"Z",20),
(#"_",#"_",20),
(#"a",#"h",20),
(#"j",#"v",20),
(#"x",#"z",20),
(#"i",#"i",87),
(#"w",#"w",88)], [59]), ([(#"A",#"Z",20),
(#"_",#"_",20),
(#"a",#"k",20),
(#"m",#"z",20),
(#"l",#"l",86)], [59]), ([(#"A",#"Z",20),
(#"_",#"_",20),
(#"a",#"z",20)], [53, 59]), ([(#"A",#"Z",20),
(#"_",#"_",20),
(#"a",#"f",20),
(#"h",#"z",20),
(#"g",#"g",100)], [59]), ([(#"A",#"Z",20),
(#"a",#"z",20),
(#"_",#"_",89)], [59]), ([(#"A",#"Z",20),
(#"_",#"_",20),
(#"a",#"d",20),
(#"f",#"u",20),
(#"w",#"z",20),
(#"e",#"e",90),
(#"v",#"v",91)], [59]), ([(#"A",#"Z",20),
(#"_",#"_",20),
(#"a",#"c",20),
(#"e",#"z",20),
(#"d",#"d",97)], [59]), ([(#"A",#"Z",20),
(#"_",#"_",20),
(#"a",#"d",20),
(#"f",#"z",20),
(#"e",#"e",92)], [59]), ([(#"A",#"Z",20),
(#"_",#"_",20),
(#"a",#"q",20),
(#"s",#"z",20),
(#"r",#"r",93)], [59]), ([(#"A",#"Z",20),
(#"_",#"_",20),
(#"a",#"s",20),
(#"u",#"z",20),
(#"t",#"t",94)], [59]), ([(#"A",#"Z",20),
(#"_",#"_",20),
(#"a",#"d",20),
(#"f",#"z",20),
(#"e",#"e",95)], [59]), ([(#"A",#"Z",20),
(#"_",#"_",20),
(#"a",#"w",20),
(#"y",#"z",20),
(#"x",#"x",96)], [59]), ([(#"A",#"Z",20),
(#"_",#"_",20),
(#"a",#"z",20)], [47, 59]), ([(#"A",#"Z",20),
(#"_",#"_",20),
(#"a",#"f",20),
(#"h",#"z",20),
(#"g",#"g",98)], [59]), ([(#"A",#"Z",20),
(#"_",#"_",20),
(#"a",#"d",20),
(#"f",#"z",20),
(#"e",#"e",99)], [59]), ([(#"A",#"Z",20),
(#"_",#"_",20),
(#"a",#"z",20)], [48, 59]), ([(#"A",#"Z",20),
(#"_",#"_",20),
(#"a",#"g",20),
(#"i",#"z",20),
(#"h",#"h",101)], [59]), ([(#"A",#"Z",20),
(#"_",#"_",20),
(#"a",#"a",20),
(#"c",#"z",20),
(#"b",#"b",102)], [59]), ([(#"A",#"Z",20),
(#"_",#"_",20),
(#"a",#"n",20),
(#"p",#"z",20),
(#"o",#"o",103)], [59]), ([(#"A",#"Z",20),
(#"_",#"_",20),
(#"a",#"q",20),
(#"s",#"z",20),
(#"r",#"r",104)], [59]), ([(#"A",#"Z",20),
(#"_",#"_",20),
(#"a",#"r",20),
(#"t",#"z",20),
(#"s",#"s",105)], [59]), ([(#"A",#"Z",20),
(#"_",#"_",20),
(#"a",#"z",20)], [54, 59]), ([(#"A",#"Z",20),
(#"_",#"_",20),
(#"a",#"c",20),
(#"e",#"z",20),
(#"d",#"d",107)], [59]), ([(#"A",#"Z",20),
(#"_",#"_",20),
(#"a",#"z",20)], [19, 59]), ([(#"A",#"Z",20),
(#"_",#"_",20),
(#"a",#"m",20),
(#"o",#"z",20),
(#"n",#"n",112)], [59]), ([(#"A",#"Z",20),
(#"_",#"_",20),
(#"a",#"r",20),
(#"t",#"z",20),
(#"s",#"s",110)], [59]), ([(#"A",#"Z",20),
(#"_",#"_",20),
(#"a",#"s",20),
(#"u",#"z",20),
(#"t",#"t",111)], [59]), ([(#"A",#"Z",20),
(#"_",#"_",20),
(#"a",#"z",20)], [33, 59]), ([(#"A",#"Z",20),
(#"_",#"_",20),
(#"a",#"f",20),
(#"h",#"z",20),
(#"g",#"g",113)], [59]), ([(#"A",#"Z",20),
(#"_",#"_",20),
(#"a",#"s",20),
(#"u",#"z",20),
(#"t",#"t",114)], [59]), ([(#"A",#"Z",20),
(#"_",#"_",20),
(#"a",#"g",20),
(#"i",#"z",20),
(#"h",#"h",115)], [59]), ([(#"A",#"Z",20),
(#"_",#"_",20),
(#"a",#"z",20)], [46, 59]), ([(#"A",#"Z",20),
(#"_",#"_",20),
(#"a",#"z",20)], [40, 59]), ([(#"A",#"Z",20),
(#"_",#"_",20),
(#"a",#"s",20),
(#"u",#"z",20),
(#"t",#"t",118)], [59]), ([(#"A",#"Z",20),
(#"_",#"_",20),
(#"a",#"z",20)], [30, 59]), ([(#"A",#"Z",20),
(#"_",#"_",20),
(#"b",#"z",20),
(#"a",#"a",120)], [59]), ([(#"A",#"Z",20),
(#"_",#"_",20),
(#"a",#"c",20),
(#"e",#"z",20),
(#"d",#"d",121)], [59]), ([(#"A",#"Z",20),
(#"_",#"_",20),
(#"a",#"z",20)], [43, 59]), ([(#"A",#"Z",20),
(#"_",#"_",20),
(#"a",#"s",20),
(#"u",#"z",20),
(#"t",#"t",127)], [59]), ([(#"A",#"Z",20),
(#"_",#"_",20),
(#"b",#"z",20),
(#"a",#"a",124)], [59]), ([(#"A",#"Z",20),
(#"_",#"_",20),
(#"a",#"o",20),
(#"q",#"z",20),
(#"p",#"p",125)], [59]), ([(#"A",#"Z",20),
(#"_",#"_",20),
(#"a",#"g",20),
(#"i",#"z",20),
(#"h",#"h",126)], [59]), ([(#"A",#"Z",20),
(#"_",#"_",20),
(#"a",#"z",20)], [36, 59]), ([(#"A",#"Z",20),
(#"a",#"z",20),
(#"_",#"_",128)], [59]), ([(#"A",#"Z",20),
(#"_",#"_",20),
(#"a",#"d",20),
(#"f",#"z",20),
(#"e",#"e",129)], [59]), ([(#"A",#"Z",20),
(#"_",#"_",20),
(#"a",#"c",20),
(#"e",#"z",20),
(#"d",#"d",130)], [59]), ([(#"A",#"Z",20),
(#"_",#"_",20),
(#"a",#"f",20),
(#"h",#"z",20),
(#"g",#"g",131)], [59]), ([(#"A",#"Z",20),
(#"_",#"_",20),
(#"a",#"d",20),
(#"f",#"z",20),
(#"e",#"e",132)], [59]), ([(#"A",#"Z",20),
(#"_",#"_",20),
(#"a",#"z",20)], [55, 59]), ([(#"A",#"Z",20),
(#"_",#"_",20),
(#"a",#"k",20),
(#"m",#"z",20),
(#"l",#"l",141)], [59]), ([(#"A",#"Z",20),
(#"_",#"_",20),
(#"a",#"m",20),
(#"o",#"z",20),
(#"n",#"n",135)], [59]), ([(#"A",#"Z",20),
(#"_",#"_",20),
(#"a",#"b",20),
(#"d",#"z",20),
(#"c",#"c",136)], [59]), ([(#"A",#"Z",20),
(#"_",#"_",20),
(#"a",#"s",20),
(#"u",#"z",20),
(#"t",#"t",137)], [59]), ([(#"A",#"Z",20),
(#"_",#"_",20),
(#"a",#"h",20),
(#"j",#"z",20),
(#"i",#"i",138)], [59]), ([(#"A",#"Z",20),
(#"_",#"_",20),
(#"a",#"n",20),
(#"p",#"z",20),
(#"o",#"o",139)], [59]), ([(#"A",#"Z",20),
(#"_",#"_",20),
(#"a",#"m",20),
(#"o",#"z",20),
(#"n",#"n",140)], [59]), ([(#"A",#"Z",20),
(#"_",#"_",20),
(#"a",#"z",20)], [37, 59]), ([(#"A",#"Z",20),
(#"_",#"_",20),
(#"a",#"r",20),
(#"t",#"z",20),
(#"s",#"s",142)], [59]), ([(#"A",#"Z",20),
(#"_",#"_",20),
(#"a",#"d",20),
(#"f",#"z",20),
(#"e",#"e",143)], [59]), ([(#"A",#"Z",20),
(#"_",#"_",20),
(#"a",#"z",20)], [5, 59]), ([(#"A",#"Z",20),
(#"_",#"_",20),
(#"a",#"f",20),
(#"h",#"z",20),
(#"g",#"g",150)], [59]), ([(#"A",#"Z",20),
(#"_",#"_",20),
(#"a",#"r",20),
(#"t",#"z",20),
(#"s",#"s",148)], [59]), ([(#"A",#"Z",20),
(#"_",#"_",20),
(#"a",#"c",20),
(#"e",#"z",20),
(#"d",#"d",147)], [59]), ([(#"A",#"Z",20),
(#"_",#"_",20),
(#"a",#"z",20)], [29, 59]), ([(#"A",#"Z",20),
(#"_",#"_",20),
(#"a",#"d",20),
(#"f",#"z",20),
(#"e",#"e",149)], [59]), ([(#"A",#"Z",20),
(#"_",#"_",20),
(#"a",#"z",20)], [42, 59]), ([(#"A",#"Z",20),
(#"_",#"_",20),
(#"a",#"d",20),
(#"f",#"z",20),
(#"e",#"e",151)], [59]), ([(#"A",#"Z",20),
(#"_",#"_",20),
(#"a",#"z",20)], [35, 59]), ([(#"A",#"Z",20),
(#"_",#"_",20),
(#"a",#"m",20),
(#"o",#"z",20),
(#"n",#"n",153)], [59]), ([(#"A",#"Z",20),
(#"_",#"_",20),
(#"a",#"b",20),
(#"d",#"z",20),
(#"c",#"c",154)], [59]), ([(#"A",#"Z",20),
(#"_",#"_",20),
(#"b",#"z",20),
(#"a",#"a",155)], [59]), ([(#"A",#"Z",20),
(#"_",#"_",20),
(#"a",#"s",20),
(#"u",#"z",20),
(#"t",#"t",156)], [59]), ([(#"A",#"Z",20),
(#"_",#"_",20),
(#"a",#"z",20)], [45, 59]), ([(#"A",#"Z",20),
(#"_",#"_",20),
(#"a",#"c",20),
(#"e",#"n",20),
(#"p",#"z",20),
(#"d",#"d",158),
(#"o",#"o",159)], [59]), ([(#"A",#"Z",20),
(#"_",#"_",20),
(#"a",#"x",20),
(#"z",#"z",20),
(#"y",#"y",161)], [59]), ([(#"A",#"Z",20),
(#"_",#"_",20),
(#"a",#"k",20),
(#"m",#"z",20),
(#"l",#"l",160)], [59]), ([(#"A",#"Z",20),
(#"_",#"_",20),
(#"a",#"z",20)], [31, 59]), ([(#"A",#"Z",20),
(#"_",#"_",20),
(#"a",#"z",20)], [39, 59]), ([(#"A",#"Z",20),
(#"_",#"_",20),
(#"a",#"c",20),
(#"e",#"z",20),
(#"d",#"d",163)], [59]), ([(#"A",#"Z",20),
(#"a",#"z",20),
(#"_",#"_",164)], [59]), ([(#"A",#"Z",20),
(#"_",#"_",20),
(#"a",#"d",20),
(#"f",#"u",20),
(#"w",#"z",20),
(#"e",#"e",165),
(#"v",#"v",166)], [59]), ([(#"A",#"Z",20),
(#"_",#"_",20),
(#"a",#"c",20),
(#"e",#"z",20),
(#"d",#"d",174)], [59]), ([(#"A",#"Z",20),
(#"_",#"_",20),
(#"a",#"d",20),
(#"f",#"z",20),
(#"e",#"e",167)], [59]), ([(#"A",#"Z",20),
(#"_",#"_",20),
(#"a",#"q",20),
(#"s",#"z",20),
(#"r",#"r",168)], [59]), ([(#"A",#"Z",20),
(#"_",#"_",20),
(#"a",#"s",20),
(#"u",#"z",20),
(#"t",#"t",169)], [59]), ([(#"A",#"Z",20),
(#"_",#"_",20),
(#"a",#"h",20),
(#"j",#"z",20),
(#"i",#"i",170)], [59]), ([(#"A",#"Z",20),
(#"_",#"_",20),
(#"a",#"b",20),
(#"d",#"z",20),
(#"c",#"c",171)], [59]), ([(#"A",#"Z",20),
(#"_",#"_",20),
(#"a",#"d",20),
(#"f",#"z",20),
(#"e",#"e",172)], [59]), ([(#"A",#"Z",20),
(#"_",#"_",20),
(#"a",#"r",20),
(#"t",#"z",20),
(#"s",#"s",173)], [59]), ([(#"A",#"Z",20),
(#"_",#"_",20),
(#"a",#"z",20)], [49, 59]), ([(#"A",#"Z",20),
(#"_",#"_",20),
(#"a",#"f",20),
(#"h",#"z",20),
(#"g",#"g",175)], [59]), ([(#"A",#"Z",20),
(#"_",#"_",20),
(#"a",#"d",20),
(#"f",#"z",20),
(#"e",#"e",176)], [59]), ([(#"A",#"Z",20),
(#"_",#"_",20),
(#"a",#"r",20),
(#"t",#"z",20),
(#"s",#"s",177)], [59]), ([(#"A",#"Z",20),
(#"_",#"_",20),
(#"a",#"z",20)], [50, 59]), ([], [21]), ([], [20]), ([], [57]), ([], [7]), ([], [2]), ([], [22])]
    fun mk yyins = let
        (* current start state *)
        val yyss = ref INITIAL
	fun YYBEGIN ss = (yyss := ss)
	(* current input stream *)
        val yystrm = ref yyins
	(* get one char of input *)
	val yygetc = yyInput.getc
	(* create yytext *)
	fun yymktext(strm) = yyInput.subtract (strm, !yystrm)
        open UserDeclarations
        fun lex 
(yyarg as ()) = let 
     fun continue() = let
            val yylastwasn = yyInput.lastWasNL (!yystrm)
            fun yystuck (yyNO_MATCH) = raise Fail "stuck state"
	      | yystuck (yyMATCH (strm, action, old)) = 
		  action (strm, old)
	    val yypos = yyInput.getpos (!yystrm)
	    val yygetlineNo = yyInput.getlineNo
	    fun yyactsToMatches (strm, [],	  oldMatches) = oldMatches
	      | yyactsToMatches (strm, act::acts, oldMatches) = 
		  yyMATCH (strm, act, yyactsToMatches (strm, acts, oldMatches))
	    fun yygo actTable = 
		(fn (~1, _, oldMatches) => yystuck oldMatches
		  | (curState, strm, oldMatches) => let
		      val (transitions, finals') = Vector.sub (yytable, curState)
		      val finals = map (fn i => Vector.sub (actTable, i)) finals'
		      fun tryfinal() = 
		            yystuck (yyactsToMatches (strm, finals, oldMatches))
		      fun find (c, []) = NONE
			| find (c, (c1, c2, s)::ts) = 
		            if c1 <= c andalso c <= c2 then SOME s
			    else find (c, ts)
		      in case yygetc strm
			  of SOME(c, strm') => 
			       (case find (c, transitions)
				 of NONE => tryfinal()
				  | SOME n => 
				      yygo actTable
					(n, strm', 
					 yyactsToMatches (strm, finals, oldMatches)))
			   | NONE => tryfinal()
		      end)
	    in 
let
fun yyAction0 (strm, lastMatch : yymatch) = (yystrm := strm;
      (pos := (!pos) + 1; lex()))
fun yyAction1 (strm, lastMatch : yymatch) = (yystrm := strm; (lex()))
fun yyAction2 (strm, lastMatch : yymatch) = let
      val yytext = yymktext(strm)
      in
        yystrm := strm; (Tokens.STRING (yytext, !pos,!pos))
      end
fun yyAction3 (strm, lastMatch : yymatch) = let
      val yytext = yymktext(strm)
      in
        yystrm := strm;
        (Tokens.NUM (valOf (Int.fromString yytext), !pos, !pos))
      end
fun yyAction4 (strm, lastMatch : yymatch) = (yystrm := strm;
      (Tokens.BOOL    (true, !pos,!pos)))
fun yyAction5 (strm, lastMatch : yymatch) = (yystrm := strm;
      (Tokens.BOOL   (false, !pos,!pos)))
fun yyAction6 (strm, lastMatch : yymatch) = (yystrm := strm;
      (Tokens.SEMI          (!pos,!pos)))
fun yyAction7 (strm, lastMatch : yymatch) = (yystrm := strm;
      (Tokens.LAND          (!pos,!pos)))
fun yyAction8 (strm, lastMatch : yymatch) = (yystrm := strm;
      (Tokens.LOR           (!pos,!pos)))
fun yyAction9 (strm, lastMatch : yymatch) = (yystrm := strm;
      (Tokens.LPAR          (!pos,!pos)))
fun yyAction10 (strm, lastMatch : yymatch) = (yystrm := strm;
      (Tokens.RPAR          (!pos,!pos)))
fun yyAction11 (strm, lastMatch : yymatch) = (yystrm := strm;
      (Tokens.LCOL          (!pos,!pos)))
fun yyAction12 (strm, lastMatch : yymatch) = (yystrm := strm;
      (Tokens.RCOL          (!pos,!pos)))
fun yyAction13 (strm, lastMatch : yymatch) = (yystrm := strm;
      (Tokens.LCHAV         (!pos,!pos)))
fun yyAction14 (strm, lastMatch : yymatch) = (yystrm := strm;
      (Tokens.RCHAV         (!pos,!pos)))
fun yyAction15 (strm, lastMatch : yymatch) = (yystrm := strm;
      (Tokens.PLUS          (!pos,!pos)))
fun yyAction16 (strm, lastMatch : yymatch) = (yystrm := strm;
      (Tokens.SUB           (!pos,!pos)))
fun yyAction17 (strm, lastMatch : yymatch) = (yystrm := strm;
      (Tokens.DIV           (!pos,!pos)))
fun yyAction18 (strm, lastMatch : yymatch) = (yystrm := strm;
      (Tokens.TIMES         (!pos,!pos)))
fun yyAction19 (strm, lastMatch : yymatch) = (yystrm := strm;
      (Tokens.YMOD          (!pos,!pos)))
fun yyAction20 (strm, lastMatch : yymatch) = (yystrm := strm;
      (Tokens.LOEQ          (!pos,!pos)))
fun yyAction21 (strm, lastMatch : yymatch) = (yystrm := strm;
      (Tokens.GOEQ          (!pos,!pos)))
fun yyAction22 (strm, lastMatch : yymatch) = (yystrm := strm;
      (Tokens.DIFF         (!pos,!pos)))
fun yyAction23 (strm, lastMatch : yymatch) = (yystrm := strm;
      (Tokens.LNOT         (!pos,!pos)))
fun yyAction24 (strm, lastMatch : yymatch) = (yystrm := strm;
      (Tokens.LESS          (!pos,!pos)))
fun yyAction25 (strm, lastMatch : yymatch) = (yystrm := strm;
      (Tokens.GREATER          (!pos,!pos)))
fun yyAction26 (strm, lastMatch : yymatch) = (yystrm := strm;
      (Tokens.EQUAL         (!pos,!pos)))
fun yyAction27 (strm, lastMatch : yymatch) = (yystrm := strm;
      (Tokens.EQUAL         (!pos,!pos)))
fun yyAction28 (strm, lastMatch : yymatch) = (yystrm := strm;
      (Tokens.COMMA         (!pos,!pos)))
fun yyAction29 (strm, lastMatch : yymatch) = (yystrm := strm;
      (Tokens.END_KW        (!pos,!pos)))
fun yyAction30 (strm, lastMatch : yymatch) = (yystrm := strm;
      (Tokens.INT_KW        (!pos,!pos)))
fun yyAction31 (strm, lastMatch : yymatch) = (yystrm := strm;
      (Tokens.BOOL_KW       (!pos,!pos)))
fun yyAction32 (strm, lastMatch : yymatch) = (yystrm := strm;
      (Tokens.STRING_KW     (!pos,!pos)))
fun yyAction33 (strm, lastMatch : yymatch) = (yystrm := strm;
      (Tokens.LIST_OF_KW    (!pos,!pos)))
fun yyAction34 (strm, lastMatch : yymatch) = (yystrm := strm;
      (Tokens.VERTEX_KW     (!pos,!pos)))
fun yyAction35 (strm, lastMatch : yymatch) = (yystrm := strm;
      (Tokens.EDGE_KW       (!pos,!pos)))
fun yyAction36 (strm, lastMatch : yymatch) = (yystrm := strm;
      (Tokens.GRAPH_KW      (!pos,!pos)))
fun yyAction37 (strm, lastMatch : yymatch) = (yystrm := strm;
      (Tokens.FUN_KW        (!pos,!pos)))
fun yyAction38 (strm, lastMatch : yymatch) = (yystrm := strm;
      (Tokens.RETURN_KW     (!pos,!pos)))
fun yyAction39 (strm, lastMatch : yymatch) = (yystrm := strm;
      (Tokens.FUN_BD_KW     (!pos,!pos)))
fun yyAction40 (strm, lastMatch : yymatch) = (yystrm := strm;
      (Tokens.IF_KW         (!pos,!pos)))
fun yyAction41 (strm, lastMatch : yymatch) = (yystrm := strm;
      (Tokens.THEN_KW       (!pos,!pos)))
fun yyAction42 (strm, lastMatch : yymatch) = (yystrm := strm;
      (Tokens.ELSE_KW       (!pos,!pos)))
fun yyAction43 (strm, lastMatch : yymatch) = (yystrm := strm;
      (Tokens.HEAD_KW       (!pos,!pos)))
fun yyAction44 (strm, lastMatch : yymatch) = (yystrm := strm;
      (Tokens.TAIL_KW       (!pos,!pos)))
fun yyAction45 (strm, lastMatch : yymatch) = (yystrm := strm;
      (Tokens.CONCAT_KW     (!pos,!pos)))
fun yyAction46 (strm, lastMatch : yymatch) = (yystrm := strm;
      (Tokens.LENGTH_KW     (!pos,!pos)))
fun yyAction47 (strm, lastMatch : yymatch) = (yystrm := strm;
      (Tokens.NEW_VERTEX_KW  (!pos,!pos)))
fun yyAction48 (strm, lastMatch : yymatch) = (yystrm := strm;
      (Tokens.NEW_EDGE_KW    (!pos,!pos)))
fun yyAction49 (strm, lastMatch : yymatch) = (yystrm := strm;
      (Tokens.ADD_VERTICES_KW   (!pos,!pos)))
fun yyAction50 (strm, lastMatch : yymatch) = (yystrm := strm;
      (Tokens.ADD_EDGES_KW      (!pos,!pos)))
fun yyAction51 (strm, lastMatch : yymatch) = (yystrm := strm;
      (Tokens.REMOVE_VERTICES_KW   (!pos,!pos)))
fun yyAction52 (strm, lastMatch : yymatch) = (yystrm := strm;
      (Tokens.REMOVE_EDGES_KW      (!pos,!pos)))
fun yyAction53 (strm, lastMatch : yymatch) = (yystrm := strm;
      (Tokens.NIL                  (!pos,!pos)))
fun yyAction54 (strm, lastMatch : yymatch) = (yystrm := strm;
      (Tokens.NEIGHBORS_KW         (!pos,!pos)))
fun yyAction55 (strm, lastMatch : yymatch) = (yystrm := strm;
      (Tokens.GET_EDGE_KW           (!pos,!pos)))
fun yyAction56 (strm, lastMatch : yymatch) = (yystrm := strm;
      (Tokens.COLON         (!pos,!pos)))
fun yyAction57 (strm, lastMatch : yymatch) = (yystrm := strm;
      (Tokens.GETS          (!pos,!pos)))
fun yyAction58 (strm, lastMatch : yymatch) = (yystrm := strm;
      (Tokens.DOT           (!pos,!pos)))
fun yyAction59 (strm, lastMatch : yymatch) = let
      val yytext = yymktext(strm)
      in
        yystrm := strm; (Tokens.IDENT  (yytext,!pos,!pos))
      end
val yyactTable = Vector.fromList([yyAction0, yyAction1, yyAction2, yyAction3,
  yyAction4, yyAction5, yyAction6, yyAction7, yyAction8, yyAction9, yyAction10,
  yyAction11, yyAction12, yyAction13, yyAction14, yyAction15, yyAction16,
  yyAction17, yyAction18, yyAction19, yyAction20, yyAction21, yyAction22,
  yyAction23, yyAction24, yyAction25, yyAction26, yyAction27, yyAction28,
  yyAction29, yyAction30, yyAction31, yyAction32, yyAction33, yyAction34,
  yyAction35, yyAction36, yyAction37, yyAction38, yyAction39, yyAction40,
  yyAction41, yyAction42, yyAction43, yyAction44, yyAction45, yyAction46,
  yyAction47, yyAction48, yyAction49, yyAction50, yyAction51, yyAction52,
  yyAction53, yyAction54, yyAction55, yyAction56, yyAction57, yyAction58,
  yyAction59])
in
  if yyInput.eof(!(yystrm))
    then UserDeclarations.eof(yyarg)
    else (case (!(yyss))
       of INITIAL => yygo yyactTable (0, !(yystrm), yyNO_MATCH)
      (* end case *))
end
            end
	  in 
            continue() 	  
	    handle IO.Io{cause, ...} => raise cause
          end
        in 
          lex 
        end
    in
    fun makeLexer yyinputN = mk (yyInput.mkStream yyinputN)
    fun makeLexer' ins = mk (yyInput.mkStream ins)
    end

  end
