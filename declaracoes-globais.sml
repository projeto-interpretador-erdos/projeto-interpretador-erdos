structure interpretador = 
struct

datatype agregate_type = vertex_structure | edge_structure;

datatype expression = expression_list of expression list
		    | expression_sequence of expression list
		    | assignment of expression * expression
		    | return of expression
		    | if_then_else of expression * expression * expression option
		    | function_call of string * (expression list) (* função, argumentos *)
		    (*  nome do tipo, tabela (nome-atributo, tipo_atributo) *)
		    | Type_name of string
		    | Vertex_type_name of string
		    | Edge_type_name of string
		    | Type_body of agregate_type * (string, expression) HashTable.hash_table 
		    | Type_list_of_type of expression
		    | Simple_vertex_type
		    | Simple_edge_type
		    | Integer
		    | Boolean
		    | String
		    | B of bool
		    | I of int
		    | S of string
		    | N (* valor para representar o desconhecido *)
		    | V of int (* id do vértice *)
		    | V_body of string * (string,expression) HashTable.hash_table (* valores dos campos do vértice *)
		    | G of string * (expression list) * (expression list)
		    | E of int * int * string * (string,expression) HashTable.hash_table
		    | E_body of string * (string,expression) HashTable.hash_table (* valores dos campos do vértice *)
		    | F of ((expression * string) list) * (expression list) (* Função: parâmetros, lista de statements *)
		    | variable of string
		    | attribute of expression * string
		    | in_parenthesis of expression
		    |
		    (*  ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ LOGICAL OPERATORS  *)
		    opNOT of expression
		    | opLAND of expression * expression
		    | opLOR of expression * expression 
		    |
		    (* ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ RELATIONAL OPERATORS *)
		    opLESS of expression * expression 
		    | opGREATER of expression * expression 
		    | opLOEQ of expression * expression 
		    | opGOEQ of expression * expression 
		    | opDIFF of expression * expression 
		    |
		    (* ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ ARITHMETIC OPERATORS *)
		    opPLUS of expression * expression 
		    | opMINUS of expression * expression 
		    | opUMINUS of expression
		    | opTIMES of expression * expression 
		    | opDIV of expression * expression 
		    | opMOD of expression * expression 
		    |
		    (* ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ LIST  OPERATORS *)
		    opHEAD of expression
		    | opTAIL of expression
		    | opCONCAT of expression * expression (* listas *)
		    | opLENGTH of expression
		    |
		    (* ~~~~~~~~~~~~~~~~~~~~~~~~~~~ VERTEX, EDGE and GRAPH  OPERATORS *)
		    op_neighbors of string * expression (* grafo, vértice *)
		    | op_add_vertices of string * expression (* grafo, lista de vértices *)
		    | op_add_edges of string * string (* grafo, função *)
		    | op_rem_vertices of string * expression (* grafo, lista de vértices *)
		    | op_rem_edges of string * string (* grafo, função *)
		    | op_get_edge of string * expression * expression (**)
		    | new_vertex of expression
		    | new_edge of expression
		    |
		    (* ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ POLIMORPHIC OPERATORS *)
		    opEQ of expression * expression 
fun print (what:string) = (TextIO.output(TextIO.stdOut, what));
fun say_ok() = print "ok\n";
fun imprime_banner_interpretador() = 
    let val banner = "~~~~~~~ Interpretador Erdos, versão 0.1 (jun 2012)\n"
    in
	TextIO.output(TextIO.stdOut, banner)
    end

exception identifier_not_found;
val initial_size:int = 101;
val hash_fn: string->word = HashString.hashString;
val cmp_fn: string*string->bool = (op =);
val var_table:(string,expression) HashTable.hash_table=
    HashTable.mkTable(hash_fn,cmp_fn)
                     (initial_size,
                      identifier_not_found);
val simple_vertex_string = "vertex";
val simple_edge_string = "edge";
val vertex_table = DynamicArray.array (initial_size,N);
val vertex_counter = ref 0;
val table_stack:((string,expression) HashTable.hash_table) list ref = ref [];

fun edge_compare ((a,b,_,_), (c,d,_,_)) =
    let fun ordered(x,y) = if x < y then (x,y) else (y,x);
	val (a',b') = ordered(a,b);
	val (c',d') = ordered(c,d);
    in
    if ((a' = c') andalso (b' = d')) then EQUAL
    else
	if (a' < c') orelse ((a' = c') andalso (b' < d')) then LESS 
	else 
	    if (a' > c') orelse ((a' = c') andalso (b' > d')) then GREATER
	    else EQUAL
    end;

structure EKey = struct
type ord_key = int * int * string * ((string,expression) HashTable.hash_table)
val compare = edge_compare
end;

structure ESet : ORD_SET = RedBlackSetFn(EKey);
datatype graph = gr of IntBinarySet.set ref * ESet.set ref;

val graph_table:(string,graph) HashTable.hash_table=
    HashTable.mkTable(hash_fn,cmp_fn) 
		     (initial_size,
		      identifier_not_found);

fun find_graph (g_name:string) =
    HashTable.lookup graph_table (g_name);


fun vertices_of_graph (g_name:string) =
    let val gr(v,_) = find_graph(g_name) in
	(map (fn (x:int) => (V x)) (IntBinarySet.listItems (!v)))
    end;

fun edges_of_graph (g_name:string) =
    let val gr(_,e) = find_graph(g_name) in
	(map (fn (x,y,z,w) => (E (x,y,z,w)))  (ESet.listItems (!e)))
    end;
(************************)

(* Agora que sei imprimir, quero encontrar os vizinhos de v *)
fun has_extremes (a:int,b:int) = 
 fn ((x,y,_,_)) => 
    (((x = a) andalso (y = b))
     orelse
     ((x = b) andalso (y = a)));


fun has_extreme (i:int) = 
 fn ((x,y,_,_)) => ((x = i) orelse (y = i));

fun clear_list_of_neighbors (x:int, le) = 
    case le of ((E(a,b,_,_))::t) => (if a = x then V b else V a) :: clear_list_of_neighbors(x,t)
	| _ => nil;

fun neighbors (x:int, gr (a,b)) =
    clear_list_of_neighbors(
    x,
    map (fn (x,y,z,w) => E(x,y,z,w)) 
	(ESet.listItems 
	     (ESet.filter (has_extreme(x))
			  (!b))));

(* E a aresta que liga v1 e v2 *)
fun get_edge (x:int, y:int, gr(a,b)) =
    ESet.listItems ((ESet.filter (has_extremes(x,y))) (!b));

(* Adicionando vértices *)
fun add_vertices (lista:int list, gr(a,b)) =
    a:= IntBinarySet.addList (!a, lista);

(* Remoção de vértices *)
fun remove_single_vertex (x:int, gr(a,b)) =
    (a:= IntBinarySet.delete(!a,x);
     b:= (ESet.filter (fn (e) => (not ((has_extreme x) (e))))) (!b))
fun remove_vertices (vList:int list, gr(a,b)) =
    (case vList 
      of (h::t) => (remove_single_vertex (h,gr(a,b));
		    remove_vertices (t,gr(a,b)))
      | _ => ());

(* precisamos de uma função que para cada dois vértices 
 * dá uma aresta ou nada (aresta option *)
(*
fun cria_aresta (x:int, y:int) =
    if (x+y) mod 3 <> 0 
    then SOME (E(x,y, Int.toString (x+y)
		      ^ " tem resto " ^
		      Int.toString ((x+y) mod 3)
		      ^ " mod 3"))
    else NONE;
*)

(* Vamos usar também uma variante do map, que omite itens
 * NONE que porventura sejam retornado por uma chamada de
 * cria_aresta *)
fun my_map_partial (f:'a -> ('b option)) (l:'a list):'b list =
    (case (l)
     of nil => nil
      | x::xs => (case (f x)
		   of SOME y => y::(my_map_partial f xs)
		    | NONE => (my_map_partial f xs)));

(* A função itera_em_pares recebe
 *
 * - uma lista de vértices, e 
 * - uma função criadora de arestas,
 *
 * e retorna uma lista com as arestas que essa
 * função produz, quando chamada (uma vez) 
 * para cada par de vértices do grafo *)
fun call_for_all_pairs (vList:int list, f:(int*int) -> (expression option)) =
    (case vList
      of nil => nil
       | (h::t) => (my_map_partial (fn (x) => f(h,x)) t)
		   @ call_for_all_pairs(t,f));
			 

(* Lista de vértices do grafo *)
fun vertices (gr(v,e)) = IntBinarySet.listItems (!v);

(* Agora vamos acrescentar as arestas que itera_em_pares
 * cria ao grafo. *)

fun add_edges(f:(int*int)->(expression option), g:graph) =
    case g
     of gr(v,e) =>
	let val vertex_list = vertices (gr(v,e)) in
	    e := ESet.addList(!e, 
			      (map (fn (E(x,y,z,w)) 
				       => ((x,y,z,w):ESet.item )) (call_for_all_pairs(vertex_list, f))))
	end;

(* Remoção de arestas é parecida com a de vértices *)
fun remove_edges (f:(int *int) -> bool, gr(a,b)) =
    (b:= (ESet.filter (fn (x,y,_,_) => (not (f (x,y)))) (!b)));


(************************)
fun is_atom (v) =
    (case v
      of I _ => true (* inteiro *)
       | B _ => true (* booleano *)
       | S _ => true (* string *)
       | expression_list v =>
	 (case v 
	   of nil => true
	    | h::t => is_atom (h) andalso is_atom (expression_list t))
       | V _ => true (* vértice (valor) *)
       | E_body _ => true 
       | N => true (* nada *)
       | _ => false);

fun expression_string(e:expression):string =
    (case e
      of opUMINUS a => "-" ^ expression_string(a)
       | expression_list l =>
         ("[" ^ string_of_list (l) ^ "]")
       | expression_sequence l => 
	 (case l
	   of nil => ""
	    | (h::nil) => expression_string (h)
	    | _ => string_of_list (l))
       | opPLUS (a,b) => expression_string(a) ^ "+" ^ expression_string(b)
       | opMINUS (a,b) => expression_string(a) ^ "-" ^ expression_string(b)
       | opTIMES (a,b) => expression_string(a) ^ "*" ^ expression_string(b)
       | opDIV (a,b) => expression_string(a) ^ "/" ^ expression_string(b)
       | opMOD (a,b) => expression_string(a) ^ " mod " ^ expression_string(b)
       | opNOT a => "!" ^ expression_string(a)
       | opLAND (a,b) => expression_string(a) ^ " && " ^ expression_string(b)
       | opLOR (a,b) => expression_string(a) ^ " || " ^ expression_string(b)
       | opLESS (a,b) => expression_string(a) ^ " < " ^ expression_string(b)
       | opGREATER (a,b) => expression_string(a) ^ " > " ^ expression_string(b)
       | opDIFF (a,b) => expression_string(a) ^ " != " ^ expression_string(b)
       | opLOEQ (a,b) => expression_string(a) ^ " <= " ^ expression_string(b)
       | opGOEQ (a,b) => expression_string(a) ^ " >= " ^ expression_string(b)
       | opHEAD a => "head (" ^ expression_string (a) ^ ")"
       | opTAIL a => "tail (" ^ expression_string (a) ^ ")"
       | opCONCAT (a,b) => "concat (" ^ expression_string (a) ^ ", " 
			   ^ expression_string(b) ^ ")"
       | opLENGTH a => "length (" ^ expression_string (a) ^ ")"
       | opEQ (a,b) => expression_string (a) ^ " = " ^ expression_string (b)
       | return a => "return " ^ expression_string (a)
       | Vertex_type_name n => n
       | Edge_type_name n => n
       | Type_list_of_type e => "list of " ^ expression_string( e )
       | Simple_vertex_type => "vertex"
       | Simple_edge_type => "edge"
       | Integer => "int"
       | Boolean => "bool"
       | String => "string"
       | in_parenthesis e => "(" ^ expression_string e ^ ")"
       | if_then_else (a,b,c) => 
	 ("if (" ^ expression_string (a) 
	  ^ ") then {" ^ expression_string (b)
	  ^ (case c
	      of SOME c' => ("} else {" ^ expression_string (c'))
	       | NONE => "")
	  ^ "}")
       | assignment (var, value) => expression_string (var) ^ " := " ^ expression_string (value)
       | variable nome => nome
       | attribute (exp,field) => expression_string (exp) ^ "." ^ field
       | function_call (f,arglist) => f ^ "(" ^ string_of_list (arglist) ^ ")"
       | I v => Int.toString v
       | B v => if v then "true" else "false"
       | S v => v
       | V id => (* V é referência a um vértice *)
	 ("#" ^ Int.toString(id)
          ^ " = "
	  ^ expression_string (DynamicArray.sub (vertex_table, id)))
       | V_body (v_type,attrs) => (* V_body contém os dados do tipo *)
	 ("{" 
	  ^ attribute_value_string (HashTable.listItemsi attrs)
	  ^ "} : vertex " ^ v_type)
       | E_body (e_type,attrs) => (* E_body contém os dados do tipo *)
	 ("{" 
	  ^ attribute_value_string (HashTable.listItemsi attrs)
	  ^ "} : edge " ^ e_type)
       | Type_body (agreg,table) =>
	 ((case agreg
	      of vertex_structure => "vertex" 
	       | _ => "edge") 
	  ^ " type {"
	  ^ signature_string(map (fn (x,y) => (y,x)) (HashTable.listItemsi table) )
	  ^ "}" )
       | N => "nada"
       | F (f_signature,f_body) =>
	   ("fun "
	    ^ "(" ^ signature_string (f_signature) ^ ") -> "
	    ^ "{" ^ string_of_list ( f_body) ^ "}")
       | new_vertex a => "new_vertex (" ^ expression_string (a) ^ ")"
       | new_edge a => "new_edge (" ^ expression_string (a) ^ ")"
       | op_neighbors (the_graph, v) =>
	 ("neighbors ("
	  ^ the_graph
	  ^ ", "
	  ^ expression_string (v)
	  ^ ")" )
       | op_get_edge (the_graph,e1,e2) => 
	 ("get_edge ("
	  ^ the_graph
	  ^ ", "
	  ^ expression_string (e1)
	  ^ ", "
	  ^ expression_string (e2)
	  ^ ")" )
       | op_add_vertices (the_graph,v_list) => 
	 ("add_vertices (" 
	  ^ the_graph 
	  ^ ", " 
	  ^ expression_string(v_list)
	  ^ ")" )
       | op_rem_vertices (the_graph, v_list) => 
	 ("rem_vertices (" 
	  ^ the_graph 
	  ^ ", " 
	  ^ expression_string(v_list)
	  ^ ")" )
       | op_add_edges (the_graph, edge_function) =>
	 ("add_edges (" 
	  ^ the_graph 
	  ^ ", " 
	  ^ edge_function
	  ^ ")"  )       
       | op_rem_edges (the_graph, edge_function) =>
	 ("remove_edges (" 
	  ^ the_graph 
	  ^ ", " 
	  ^ edge_function
	  ^ ")"  )       
       | E (v1,v2,e_type,table) => 
	 (Int.toString (v1) ^ " -- " ^ Int.toString (v2)
	  ^ "{" 
	  ^ attribute_value_string (HashTable.listItemsi table)
	  ^ "} : " ^ e_type)
       | G (name, v_types, e_types) => 
	 (
	  "graph: " ^ name
	  ^ " (" ^ string_of_list( v_types ) ^ ")"
	  ^  "(" ^ string_of_list( e_types ) ^ "):\n"
	  ^ "vertices: {" 
	  ^ string_of_list ( vertices_of_graph( name ) ) (* nil será vertices_of_graph ( nome ) *)
	  ^ "}\nedges: " 
	  ^ string_of_list ( edges_of_graph( name ) ) (* nil será edges_of_graph ( nome ) *)
	 )
       | _ => "")
and string_of_list (l:expression list) = 
    (case l
      of (h::nil) => (expression_string (h))
       | (h::t) => expression_string (h)
                   ^ ", "
                   ^ string_of_list (t)
       | nil => "")
and attribute_value_string (attrs):string =
    (case attrs
      of (name,value)::remaining_values =>
         (name ^ ":" ^ expression_string(value)
          ^ (case remaining_values
              of nil => ""
               | _ => (", " ^ attribute_value_string(remaining_values))))
       | _ => "")
and signature_string (params:(expression*string) list) =
 (case params
     of nil => ""
      | (the_type,name)::remaining_params =>
        (name ^ ":" ^ expression_string(the_type)
         ^ (case remaining_params
             of nil => ""
              | _ => (", " ^ signature_string (remaining_params)))))
fun evaluate(e:expression):expression =
     let
          val antes = e
          val depois = (case e
      of opUMINUS a =>
         (case evaluate(a) of I a' => I (~a')
                            | a' => opUMINUS a')
       | opPLUS (a,b) =>
         (case (evaluate a,evaluate b) of (I a',I b') => I (a'+b')
                                        | (a',b') => opPLUS (a', b'))
       | opMINUS (a,b) =>
         (case (evaluate a,evaluate b) of (I a',I b') => I (a'-b')
                                        | (a',b') => opMINUS (a', b'))
       | opTIMES (a,b) =>
         (case (evaluate a,evaluate b) of (I a',I b') => I (a'*b')
                                        | (a',b') => opTIMES (a', b'))
       | opDIV (a,b) =>
         (case (evaluate a,evaluate b) of (I a',I b') => I (a' div b')
                                        | (a',b') => opDIV (a', b'))
       | opMOD (a,b) =>
         (case (evaluate a,evaluate b) of (I a',I b') => I (a' mod b')
                                        | (a',b') => opMOD (a', b'))
       | opNOT a =>
         (case evaluate(a) of B a' => B ( not a')
                            | a' => opNOT a')
       | opLAND (a,b) =>
         (case (evaluate a,evaluate b) of (B a',B b') => B (a' andalso b')
                                        | (a',b') => opLAND (a', b'))
       | opLOR (a,b) =>
         (case (evaluate a,evaluate b) of (B a',B b') => B (a' orelse b')
                                        | (a',b') => opLOR (a', b'))
       | opLESS (a,b) =>
         (case (evaluate a,evaluate b) of (I a',I b') => B (a' < b')
                                        | (a',b') => opLESS (a', b'))
       | opGREATER (a,b) =>
         (case (evaluate a,evaluate b) of (I a',I b') => B (a' > b')
                                        | (a',b') => opGREATER (a', b'))
       | opDIFF (a,b) =>
         (case (evaluate a,evaluate b) of (I a',I b') => B (a' <> b')
                                        | (a',b') => opDIFF (a', b'))
       | opLOEQ (a,b) =>
         (case (evaluate a,evaluate b) of (I a',I b') => B (a' <= b')
                                        | (a',b') => opLOEQ (a', b'))

       | opGOEQ (a,b) =>
         (case (evaluate a,evaluate b) of (I a',I b') => B (a' >= b')
                                        | (a',b') => opGOEQ (a', b'))
       | opEQ (a,b) => 
	 (case (evaluate a, evaluate b)
	   of (I a', I b') => B (a' = b')
	    | (B a', B b') => B (a' = b')
	    | (S a', S b') => B (a' = b')
	    | (V a', V b') => B (a' = b')
	    | (expression_list a', expression_list b') 
	      => B (list_equality_test(a', b'))
	    | _ => B false)
       | opHEAD a =>
         (case evaluate(a) of expression_list a' => hd a'
                            | a' => opHEAD  a')
       | opTAIL a => 
         (case evaluate(a) of expression_list a' => expression_list (tl a')
                            | a' => opTAIL  a')
       | opCONCAT (a,b) =>
         (case (evaluate a,evaluate b) of (expression_list a',expression_list b') => expression_list (a' @ b')
                                        | (a',b') => opCONCAT (a', b'))
       | opLENGTH a =>
         (case evaluate(a) of expression_list a' => I (length a')
                            | a' => opLENGTH  a')
       | in_parenthesis e => evaluate e
       | variable name => search_table( name )
       | attribute (exp,field) =>
	 (case evaluate (exp)
	   of V id =>
	      let val V_body (_,attr_hash) = DynamicArray.sub (vertex_table, id) in
		  (HashTable.lookup attr_hash (field))
	      end
	    | V_body (_,(attr_hash)) => (HashTable.lookup attr_hash (field))
	    | E_body (_,(attr_hash)) => (HashTable.lookup attr_hash (field))
	    | _ => attribute (exp,field))
       | function_call (f,arglist) => 
	 (print ("function call: " ^ expression_string (e) ^"\n");
         let
             val (parameter_list,command_list) =
                 case HashTable.lookup var_table (f)
                  of F (a,b) => (a,b)
         in
             (case parameter_list
              of (h::t) => (* Inicializa a tabela local se a função têm parametros *)
                 (let
                      val initial_size = 10
                      val parameter_table:(string,expression) HashTable.hash_table =
                          HashTable.mkTable(hash_fn,cmp_fn)
                                           (initial_size,
                                            identifier_not_found)
                  in
                      set_up_parameter_table(parameter_table,parameter_list,arglist);
                      table_stack := parameter_table::(!table_stack);
		      let val value = evaluate_expression_sequence (command_list) in
			  (case !table_stack
			    of (h::t) => table_stack := t
			     | _ => table_stack := nil);
			  value
		      end
                  end)
               | _ => 
		 evaluate_expression_sequence (command_list))
         end)
       | V _ => e
       | E _ => e
       | V_body _ => e
       | E_body _ => e
       | I v => e
       | B v => e
       | S v => e
       | N => e
       | F _ => e
       | assignment (var, value) =>
	 (let val v' = evaluate (value) in
	      if is_atom(v') then 
		  (case var
		   of variable name => (insert_to_table (name, v');N)
 		    | attribute (e, field) =>
		      (let val tb = 
			       (case evaluate(e) 
				 of V id =>
				    let val V_body (_,table) =
					    DynamicArray.sub (vertex_table, id) in
					table
				    end
				  | V_body (_,table) => table
				  | E_body (_,table) => table)
		       in
			   HashTable.insert tb (field, v');
			   N
		       end)
		    | _ => assignment (var, v'))
	      else assignment (var, v')
	  end
	 )
       | return e =>
         let val value = evaluate e in
	     if(is_atom value) then value
	     else return value
	 end
       | expression_sequence l => (evaluate_expression_sequence l)
       | expression_list l =>
         (case l
	   of nil => (expression_list nil)
	    | (h::nil) => (expression_list ((evaluate h)::nil))
	    | (h::t) =>
	      let val h' = evaluate h
		  val t'=  evaluate (expression_list t)
	      in
		  case t' of expression_list t'' => expression_list (h'::t'')
	      end)
       | if_then_else (a,b,c) =>
         (case evaluate(a)
           of B true => evaluate b
            | B false => 
	      (case c
		of SOME c' => evaluate c'
		 | NONE => N)
            | a' => if_then_else (a', b, c))
       | op_add_edges (the_graph, edge_generator_function) =>
	 let fun trigger_call (x:int,y:int) =
		 (case evaluate (
		      function_call (edge_generator_function, 
				     [V x, V y]))
		  of E_body (e_type,attrs) => SOME (E (x,y,e_type,attrs))
		   | _ => NONE)
	 in
	     add_edges(trigger_call, find_graph the_graph);
	     N
	 end 
(**************)
       | op_rem_edges (the_graph, edge_generator_function) =>
	 let fun trigger_call (x:int,y:int) =
		 (case evaluate (
		      function_call (edge_generator_function, 
				     [V x, V y]))
		  of B true => true
		   | _ => false)
	 in
	     remove_edges(trigger_call, find_graph the_graph);
	     N
	 end 
       | op_neighbors (the_graph,a) =>
	 (case evaluate (a)
	   of  V id => 
	       expression_list (neighbors (id, find_graph the_graph))
	     | _ => e)
       | op_get_edge (the_graph,e1,e2) => 
	 (case (evaluate (e1),evaluate (e2))
	   of (V id_1,V id_2) =>
	       let val edg = get_edge(id_1,id_2,find_graph the_graph) in
		   case edg
		    of (h::t) => E h
		     | _ => N
	       end
	    | _ => e)
       | op_add_vertices (the_graph,v_list) =>
	 (let val l =  evaluate (v_list) in
	      if is_atom(l) 
	      then (
		  case l
		   of expression_list l' =>
		      (
		       add_vertices (
		       (map (fn (V x) => x) l'),
		       find_graph (the_graph)
		       );N)
		    | _ => e
		  )
	      else e
	  end)
       | op_rem_vertices (the_graph,v_list) =>
	 (let val l =  evaluate (v_list) in
	      if is_atom(l) 
	      then (
		  case l
		   of expression_list l' =>
		      (
		       remove_vertices (
		       (map (fn (V x) => x) l'),
		       find_graph (the_graph)
		       );N)
		    | _ => e
		  )
	      else e
	  end)
(*
		    op_neighbor of string * expression * expression (* vértice, vértice *)
	
		    | op_add_edges of expression * expression (* grafo, função *)
		    | op_rem_vertices of expression * expression (* grafo, lista de vértices *)
		    | op_rem_edges of expression * expression (* grafo, função *)
		    | op_get_edge of expression * expression * expression (**)


*)
       | new_vertex a => 
	 (case a
	   of Simple_vertex_type => make_simple_vertex()
	    | Vertex_type_name v_type => make_vertex (v_type))
       | new_edge a => 
	 (case a
	   of Simple_edge_type => make_simple_edge()
	    | Edge_type_name v_type => make_edge (v_type))
       | _ => e)
     in
         (print (expression_string(antes)
                 ^ " -> "
                 ^ expression_string(depois)
                 ^ "\n");depois)
     end
and list_equality_test(a, b) = 
    (case (a,b)
      of (nil, nil) => true
       | ((ha::ta),(hb::tb)) =>
	 (case (ha,hb)
	   of (I ha, I hb) => ha = hb
	    | (B ha, B hb) => ha = hb
	    | (S ha, S hb) => ha = hb
	    | (V ha, V hb) => ha = hb
	    | _ => false) andalso list_equality_test (ta,tb)
       | _ => false)
and search_table (name:string):expression=
(case !table_stack
      of nil =>	 
	 HashTable.lookup var_table (name)
       | top::_ =>
         let
             val value = HashTable.find top (name)
         in
             case value of NONE =>
                           HashTable.lookup var_table (name)
                         | SOME a => a
         end)
and insert_to_table (name,value)=
    (case !table_stack
      of nil => (* ninguém no topo da pilha_de_tabelas *)
         HashTable.insert var_table (name,value)
       | top::_ =>
         HashTable.insert top (name,value)
    )
and print_expression (exp:expression):unit =
    TextIO.output(TextIO.stdOut,
                  (expression_string (exp)) ^ "\n")
and add_single_identifier (name:string,e:expression):unit =
    (print_expression(e);
     insert_to_table (name, e))
and add_multiple_identifiers (TYPE,IDENTIFIER_LIST:string list) = 
    (case IDENTIFIER_LIST
      of nil => ()
       | h::t =>
	 (case TYPE
	   of Simple_vertex_type =>
	      add_single_identifier(h,make_simple_vertex())
	    | Simple_edge_type =>
	      add_single_identifier(h,make_simple_edge())
	    | Type_name the_type =>
	      let val Type_body (agreg,_) = search_table( the_type ) in
		  case agreg
		   of vertex_structure =>
		      add_single_identifier(h, make_vertex (the_type))
		    | edge_structure =>
		      add_single_identifier(h, make_edge (the_type))
	      end
	    | Vertex_type_name the_type => 
	      add_single_identifier(h, make_vertex (the_type))
	    | Edge_type_name the_type =>
	      add_single_identifier(h, make_edge (the_type))
	    | _ => add_single_identifier(h, N);
	  add_multiple_identifiers(TYPE, t)))
and make_graph (name, v_type_list, e_type_list) =
    (let val new_graph = gr( ref IntBinarySet.empty,
				ref ESet.empty ) in
	 HashTable.insert graph_table (name, new_graph);
	 (name, v_type_list, e_type_list)
     end)
and make_type (agreg,field_list) =
    let val initial_size = 7
	val body:(string,expression) HashTable.hash_table = 
	    HashTable.mkTable (hash_fn,cmp_fn) (initial_size, identifier_not_found) in
	set_up_type_table(field_list,body);
	(agreg,body)
    end
and make_simple_vertex () = make_vertex (simple_vertex_string)
and make_simple_edge () = make_edge (simple_edge_string)
and make_vertex (vertex_type) =
    let val id:int = !vertex_counter
        val initial_size = 7
        val attributes:(string,expression) HashTable.hash_table = HashTable.mkTable (hash_fn,cmp_fn) (initial_size, identifier_not_found)
    in
        vertex_counter := (!vertex_counter) + 1;
        DynamicArray.update (vertex_table, id, (V_body (vertex_type, attributes)));
        V (id)
    end
and make_edge (edge_type) =
    let val initial_size = 7
        val attributes:(string,expression) HashTable.hash_table = 
	    HashTable.mkTable (hash_fn,cmp_fn) (initial_size, identifier_not_found)
    in
	E_body (edge_type, attributes)
    end
and set_up_parameter_table( table, parameter_list, actual_parameter_list):unit =
    (case (parameter_list,actual_parameter_list)
      of (nil,_) =>()
       | ((_,par_name)::remainings_of_par_list,
          ((par_value)::remainings_of_actual_par_list)) =>
         (let val value = evaluate par_value in
              TextIO.output(TextIO.stdOut,
                            "adding \""
                            ^ par_name
                            ^ "\" (of value "
                            ^ expression_string (par_value)
                            ^ " = "
                            ^ expression_string (value)
                            ^ ") on the table to be stacked\n");
              HashTable.insert table (par_name, value);
              set_up_parameter_table(table,
				     remainings_of_par_list,
				     remainings_of_actual_par_list)
          end
         )
       | _ =>    TextIO.output(TextIO.stdOut, "ooops!\n")
    )
and set_up_type_table(fieldList:(expression * string) list,table) =
    (case fieldList
     of nil => ()
      | (the_type,the_field)::t => 
	(HashTable.insert table (the_field, the_type);set_up_type_table(t, table);()))
and evaluate_expression_sequence (l:expression list):expression =
    (case l
      of (nil) => N
       | (h::t) =>
	 let val v = evaluate h in
	     case (v,is_atom(v))
	      of (N,_) =>
		 evaluate_expression_sequence (t)
	       | (_,true) => v
	       | _ =>
		 evaluate_expression_sequence (t)
	 end
    )




fun clear_interpreter_state() = (
    HashTable.clear var_table;
    HashTable.clear graph_table;
    DynamicArray.truncate (vertex_table,0);
    vertex_counter := 0;
    table_stack := []
);

fun init_interpreter() = (
    clear_interpreter_state();
    add_single_identifier (simple_vertex_string, Type_body (make_type(vertex_structure,nil)));
    add_single_identifier (simple_edge_string, Type_body (make_type(edge_structure,nil)))
)

end
