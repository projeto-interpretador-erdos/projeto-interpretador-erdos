\select@language {portuguese}
\contentsline {chapter}{\numberline {1}Facilidades e Dificuldades}{5}
\contentsline {chapter}{\numberline {2}Considera\IeC {\c c}\IeC {\~o}es Gerais}{7}
\contentsline {section}{\numberline {2.1}Prop\IeC {\'o}sito da Linguagem}{7}
\contentsline {chapter}{\numberline {3}Elementos L\IeC {\'e}xicos}{9}
\contentsline {section}{\numberline {3.1}Identificadores}{9}
\contentsline {subsection}{\numberline {3.1.1}Sintaxe}{9}
\contentsline {subsection}{\numberline {3.1.2}Exemplos}{9}
\contentsline {section}{\numberline {3.2}Literais Num\IeC {\'e}ricos}{9}
\contentsline {subsection}{\numberline {3.2.1}Sintaxe}{9}
\contentsline {subsection}{\numberline {3.2.2}Exemplos}{10}
\contentsline {section}{\numberline {3.3}Literais String}{10}
\contentsline {subsection}{\numberline {3.3.1}Sintaxe}{10}
\contentsline {subsection}{\numberline {3.3.2}Exemplos}{10}
\contentsline {section}{\numberline {3.4}Palavras Reservadas}{10}
\contentsline {chapter}{\numberline {4}Declara\IeC {\c c}\IeC {\~o}es e Tipos}{11}
\contentsline {section}{\numberline {4.1}Declara\IeC {\c c}\IeC {\~o}es}{11}
\contentsline {subsection}{\numberline {4.1.1}Declara\IeC {\c c}\IeC {\~a}o de Tipos}{12}
\contentsline {subsubsection}{Declara\IeC {\c c}\IeC {\~a}o de Tipos de V\IeC {\'e}rtices}{13}
\contentsline {subsubsection}{Declara\IeC {\c c}\IeC {\~a}o de Tipos de Arestas}{13}
\contentsline {subsubsection}{Declara\IeC {\c c}\IeC {\~a}o de Grafos}{14}
\contentsline {subsection}{\numberline {4.1.2}Declara\IeC {\c c}\IeC {\~a}o de Vari\IeC {\'a}veis}{15}
\contentsline {subsection}{\numberline {4.1.3}Declara\IeC {\c c}\IeC {\~a}o de Subprogramas}{16}
\contentsline {section}{\numberline {4.2}Tipos Primitivos}{17}
\contentsline {subsection}{\numberline {4.2.1}Booleanos}{17}
\contentsline {subsubsection}{Opera\IeC {\c c}\IeC {\~o}es sobre Booleanos}{17}
\contentsline {subsection}{\numberline {4.2.2}Inteiros}{18}
\contentsline {subsubsection}{Opera\IeC {\c c}\IeC {\~o}es sobre Inteiros}{18}
\contentsline {subsection}{\numberline {4.2.3}Strings}{20}
\contentsline {section}{\numberline {4.3}Tipos Compostos}{21}
\contentsline {subsection}{\numberline {4.3.1}V\IeC {\'e}rtices}{21}
\contentsline {subsubsection}{Opera\IeC {\c c}\IeC {\~o}es}{22}
\contentsline {subsection}{\numberline {4.3.2}Arestas}{22}
\contentsline {subsubsection}{Opera\IeC {\c c}\IeC {\~o}es}{24}
\contentsline {subsection}{\numberline {4.3.3}Listas}{25}
\contentsline {subsubsection}{Opera\IeC {\c c}\IeC {\~o}es}{26}
\contentsline {section}{\numberline {4.4}Grafos}{28}
\contentsline {subsubsection}{Opera\IeC {\c c}\IeC {\~o}es sobre Grafos}{28}
\contentsline {chapter}{\numberline {5}Express\IeC {\~o}es}{33}
\contentsline {section}{\numberline {5.1}Express\IeC {\~o}es Condicionais}{33}
\contentsline {subsection}{\numberline {5.1.1}Sintaxe}{33}
\contentsline {subsection}{\numberline {5.1.2}Exemplo}{33}
\contentsline {chapter}{\numberline {6}Subprogramas}{35}
\contentsline {section}{\numberline {6.1}Modo de Parametriza\IeC {\c c}\IeC {\~a}o}{35}
\contentsline {section}{\numberline {6.2}Declara\IeC {\c c}\IeC {\~o}es de Fun\IeC {\c c}\IeC {\~o}es}{36}
\contentsline {section}{\numberline {6.3}Express\IeC {\~a}o de Retorno}{36}
\contentsline {section}{\numberline {6.4}Corpo de Fun\IeC {\c c}\IeC {\~o}es}{36}
\contentsline {section}{\numberline {6.5}Chamada de Fun\IeC {\c c}\IeC {\~o}es}{37}
\contentsline {chapter}{\numberline {7}Execu\IeC {\c c}\IeC {\~a}o do Programa}{39}
\contentsline {chapter}{\numberline {8}Exemplo: Grafo Conexo}{43}
\contentsline {section}{\numberline {8.1}Descri\IeC {\c c}\IeC {\~a}o do Problema}{43}
\contentsline {section}{\numberline {8.2}Algoritmo}{43}
\contentsline {subsection}{\numberline {8.2.1}Exemplos}{44}
\contentsline {section}{\numberline {8.3}Literais Num\IeC {\'e}ricos}{45}
\contentsline {subsection}{\numberline {8.3.1}Sintaxe}{45}
\contentsline {subsection}{\numberline {8.3.2}Exemplos}{45}
\contentsline {section}{\numberline {8.4}Literais String}{45}
\contentsline {subsection}{\numberline {8.4.1}Sintaxe}{45}
\contentsline {subsection}{\numberline {8.4.2}Exemplos}{45}
\contentsline {section}{\numberline {8.5}Palavras Reservadas}{45}
\contentsline {chapter}{\numberline {9}Declara\IeC {\c c}\IeC {\~o}es e Tipos}{47}
\contentsline {section}{\numberline {9.1}Declara\IeC {\c c}\IeC {\~o}es}{47}
\contentsline {subsection}{\numberline {9.1.1}Declara\IeC {\c c}\IeC {\~a}o de Tipos}{48}
\contentsline {subsubsection}{Declara\IeC {\c c}\IeC {\~a}o de Tipos de V\IeC {\'e}rtices}{49}
\contentsline {subsubsection}{Declara\IeC {\c c}\IeC {\~a}o de Tipos de Arestas}{49}
\contentsline {subsubsection}{Declara\IeC {\c c}\IeC {\~a}o de Grafos}{50}
\contentsline {subsection}{\numberline {9.1.2}Declara\IeC {\c c}\IeC {\~a}o de Vari\IeC {\'a}veis}{51}
\contentsline {subsection}{\numberline {9.1.3}Declara\IeC {\c c}\IeC {\~a}o de Subprogramas}{52}
\contentsline {section}{\numberline {9.2}Tipos Primitivos}{53}
\contentsline {subsection}{\numberline {9.2.1}Booleanos}{53}
\contentsline {subsubsection}{Opera\IeC {\c c}\IeC {\~o}es sobre Booleanos}{53}
\contentsline {subsection}{\numberline {9.2.2}Inteiros}{54}
\contentsline {subsubsection}{Opera\IeC {\c c}\IeC {\~o}es sobre Inteiros}{54}
\contentsline {subsection}{\numberline {9.2.3}Strings}{56}
\contentsline {section}{\numberline {9.3}Tipos Compostos}{57}
\contentsline {subsection}{\numberline {9.3.1}V\IeC {\'e}rtices}{57}
\contentsline {subsubsection}{Opera\IeC {\c c}\IeC {\~o}es}{58}
\contentsline {subsection}{\numberline {9.3.2}Arestas}{58}
\contentsline {subsubsection}{Opera\IeC {\c c}\IeC {\~o}es}{60}
\contentsline {subsection}{\numberline {9.3.3}Listas}{61}
\contentsline {subsubsection}{Opera\IeC {\c c}\IeC {\~o}es}{62}
\contentsline {section}{\numberline {9.4}Grafos}{64}
\contentsline {subsubsection}{Opera\IeC {\c c}\IeC {\~o}es sobre Grafos}{64}
\contentsline {chapter}{\numberline {10}Express\IeC {\~o}es}{69}
\contentsline {section}{\numberline {10.1}Express\IeC {\~o}es Condicionais}{69}
\contentsline {subsection}{\numberline {10.1.1}Sintaxe}{69}
\contentsline {subsection}{\numberline {10.1.2}Exemplo}{69}
\contentsline {chapter}{\numberline {11}Subprogramas}{71}
\contentsline {section}{\numberline {11.1}Modo de Parametriza\IeC {\c c}\IeC {\~a}o}{71}
\contentsline {section}{\numberline {11.2}Declara\IeC {\c c}\IeC {\~o}es de Fun\IeC {\c c}\IeC {\~o}es}{72}
\contentsline {section}{\numberline {11.3}Express\IeC {\~a}o de Retorno}{72}
\contentsline {section}{\numberline {11.4}Corpo de Fun\IeC {\c c}\IeC {\~o}es}{72}
\contentsline {section}{\numberline {11.5}Chamada de Fun\IeC {\c c}\IeC {\~o}es}{73}
\contentsline {chapter}{\numberline {12}Execu\IeC {\c c}\IeC {\~a}o do Programa}{75}
\contentsline {chapter}{\numberline {13}Exemplo: Grafo Conexo}{79}
\contentsline {section}{\numberline {13.1}Descri\IeC {\c c}\IeC {\~a}o do Problema}{79}
\contentsline {section}{\numberline {13.2}Algoritmo}{79}
\contentsfinish 
